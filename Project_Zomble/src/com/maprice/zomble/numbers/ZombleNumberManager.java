package com.maprice.zomble.numbers;

import org.andengine.entity.IEntity;
import org.andengine.entity.modifier.AlphaModifier;
import org.andengine.entity.modifier.DelayModifier;
import org.andengine.opengl.font.Font;
import org.andengine.util.modifier.ease.EaseExponentialIn;

import com.maprice.zomble.scene.ZombleSceneManager;
import com.maprice.zomble.util.contants.ZombleConstants;


/**
 * Manages the all activity with respect to damage numbers
 * 
 * @author maprice
 */


public class ZombleNumberManager implements ZombleConstants{
	// ===========================================================
	// Constants
	// ===========================================================

	static final String LOG_TAG = PREFIX + ZombleNumberManager.class.getSimpleName();

	// ===========================================================
	// Fields
	// ===========================================================

	final ZombleNumberPool mNumberPool;

	// ===========================================================
	// Constructors
	// ===========================================================

	public ZombleNumberManager(Font pFont){
		mNumberPool = new ZombleNumberPool(pFont);
	}

	// ===========================================================
	// Methods
	// ===========================================================

	/**
	 * Creates a bullet casing, discharging it from the gun and then simulates gravity
	 * 
	 * @param pX
	 *            starting x position
	 *            
	 * @param pY
	 *            starting y position
	 *                    
	 * @param pAngle
	 *            angle that the BULLET is being shot
	 */




	public void dropNumber(float pX, float pY, int pDamage) {

		// Get new casing
		ZombleNumber casing = mNumberPool.obtainPoolItem();

		casing.setText(String.valueOf(pDamage));

		// Fade blood out exponentially
		casing.registerEntityModifier(new AlphaModifier(0.5f, 1, 0, EaseExponentialIn.getInstance()));

		// Delay modifier used to call reycle after a set time
		casing.registerEntityModifier(new DelayModifier(4){

			@Override
			public void onModifierFinished(IEntity pItem) {
				mNumberPool.recyclePoolItem((ZombleNumber) pItem);

			}
		});

		// Attach casing to scene
		if(!casing.hasParent()){
			ZombleSceneManager.sharedInstance().getCurrentScene().attachChild(casing);
		}

		// Discharge casing
		casing.start(pX, pY);
	}


}
