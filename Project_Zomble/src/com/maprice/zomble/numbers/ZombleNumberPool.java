package com.maprice.zomble.numbers;

import org.andengine.opengl.font.Font;
import org.andengine.util.adt.pool.GenericPool;

import com.maprice.zomble.scene.ZombleSceneManager;
import com.maprice.zomble.util.contants.ZombleConstants;

/**
 * Pool system that controls the allocation and deletion of damage numbers numbers
 * 
 * @author maprice
 */


public class ZombleNumberPool extends GenericPool<ZombleNumber> implements ZombleConstants{

	// ===========================================================
	// Fields
	// ===========================================================

	private final Font mFont;

	// ===========================================================
	// Constructors
	// ===========================================================

	public ZombleNumberPool(Font pFont) {
		mFont = pFont;
	}

	// ===========================================================
	// Methods
	// ===========================================================


	@Override
	protected ZombleNumber onAllocatePoolItem() {
		return new ZombleNumber(mFont);
	}

	@Override
	protected void onHandleRecycleItem(final ZombleNumber pNumber) {
		ZombleSceneManager.sharedInstance().getEngine().runOnUpdateThread(new Runnable(){
			@Override
			public void run() {
				pNumber.setIgnoreUpdate(true);
				pNumber.clearUpdateHandlers();
				pNumber.clearEntityModifiers();
				pNumber.setVisible(false);
			}
		});
	}

	@Override
	protected void onHandleObtainItem(final ZombleNumber pNumber) {
		pNumber.setIgnoreUpdate(false);
		pNumber.resetInteractions();
	}
}