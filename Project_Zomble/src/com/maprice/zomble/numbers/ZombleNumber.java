package com.maprice.zomble.numbers;

import java.util.Random;

import org.andengine.engine.handler.physics.PhysicsHandler;
import org.andengine.entity.text.Text;
import org.andengine.opengl.font.Font;

import com.maprice.zomble.scene.ZombleSceneManager;
import com.maprice.zomble.util.contants.ZombleConstants;


/**
 * Single number (discharge from gun)
 * 
 * @author maprice
 */

public class ZombleNumber extends Text implements ZombleConstants{
	// ===========================================================
	// Constants
	// ===========================================================

	static final String LOG_TAG = PREFIX + ZombleNumber.class.getSimpleName();

	// ===========================================================
	// Fields
	// ===========================================================

	final private PhysicsHandler mPhysicsHandler;

	// ===========================================================
	// Constructors
	// ===========================================================

	public ZombleNumber(Font pFont) {
		super(0, 0, pFont, "  ", ZombleSceneManager.sharedInstance().getVBOmanager());

		// Create a random offset for where the number will fall
		this.setZIndex(Layer.HUD.ordinal());

		// Init and set the physics handlers
		mPhysicsHandler = new PhysicsHandler(this);
	}

	// ===========================================================
	// Methods
	// ===========================================================


	public void resetInteractions(){
		this.setAlpha(1.0f);
		this.registerUpdateHandler(mPhysicsHandler);
	}


	/**
	 * Starts the motion of the number
	 * 
	 * @param pAngle 
	 * @param pY 
	 *            
	 */
	public void start(float pX, float pY) {
		Random rand = new Random();

		this.setPosition(pX, pY-40);
		this.setVisible(true);	

		// Create random offsets for both the x and y velocity of the number
		//Random rand = new Random();
		int xVelocity = rand.nextInt((int) 160) - 80;
		int yVelocity = rand.nextInt((int) CASING_RANDOM_VELOCITY_Y_RANGE) + CASING_BASE_VELOCITY_Y+100;

		mPhysicsHandler.setVelocity((float) xVelocity, -yVelocity);

		// Constant downward acceleration
		mPhysicsHandler.setAccelerationY(GRAVITY);
	}
}