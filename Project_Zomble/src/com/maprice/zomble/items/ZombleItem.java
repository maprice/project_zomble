package com.maprice.zomble.items;

import org.andengine.entity.sprite.TiledSprite;
import org.andengine.opengl.texture.region.TiledTextureRegion;

import com.maprice.zomble.items.ZombleItemManager.ItemType;
import com.maprice.zomble.scene.ZombleSceneManager;
import com.maprice.zomble.util.contants.ZombleCallback;
import com.maprice.zomble.util.contants.ZombleConstants;


/**
 * Single number (discharge from gun)
 * 
 * @author maprice
 */

public class ZombleItem extends TiledSprite implements ZombleConstants{
	// ===========================================================
	// Constants
	// ===========================================================

	static final String LOG_TAG = PREFIX + ZombleItem.class.getSimpleName();

	// ===========================================================
	// Fields
	// ===========================================================

	private ItemType mType;
	
	// ===========================================================
	// Constructors
	// ===========================================================

	public ZombleItem(ItemType pType, TiledTextureRegion mOrbTextureRegion) {
		super(0, 0, mOrbTextureRegion, ZombleSceneManager.sharedInstance().getVBOmanager());
		mType = pType;
		
		this.setCullingEnabled(true);
		this.setScale(SCALE/2);
		this.setCurrentTileIndex(pType.ordinal());
		this.setZIndex(Layer.BACK_EFFECTS.ordinal());
	}

	// ===========================================================
	// Methods
	// ===========================================================

	public void resetInteractions(){
		this.setVisible(true);
	}

	public ItemType getItemType(){
		return mType;
	}

}