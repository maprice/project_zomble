package com.maprice.zomble.items;

import org.andengine.opengl.texture.region.TiledTextureRegion;
import org.andengine.util.adt.pool.GenericPool;

import com.maprice.zomble.items.ZombleItemManager.ItemType;
import com.maprice.zomble.scene.ZombleSceneManager;
import com.maprice.zomble.util.contants.ZombleConstants;
import com.maprice.zomble.util.contants.ZombleUtil;

/**
 * Pool system that controls the allocation and deletion of items
 * 
 * @author maprice
 */


public class ZombleItemPool extends GenericPool<ZombleItem> implements ZombleConstants{

	// ===========================================================
	// Fields
	// ===========================================================

	private final TiledTextureRegion mItemTextureRegion;

	// ===========================================================
	// Constructors
	// ===========================================================

	public ZombleItemPool(TiledTextureRegion pItemTextureRegion) {
		mItemTextureRegion = pItemTextureRegion;
	}

	// ===========================================================
	// Methods
	// ===========================================================

	@Override
	protected ZombleItem onAllocatePoolItem() {

		ItemType lNewItem = ItemType.HEALTH;
		
		switch(ZombleUtil.getRandom(0, 3)){
		case 0:
			lNewItem = ItemType.HEALTH;
			break;
		case 1:
			lNewItem = ItemType.AMMO;
			break;
		case 2:
			lNewItem = ItemType.FOOD;
			break;
		case 3:
			lNewItem = ItemType.BATTERY;
			break;
		}
		
		return new ZombleItem(lNewItem, mItemTextureRegion);
	}

	@Override
	protected void onHandleRecycleItem(final ZombleItem pItem) {
		pItem.setIgnoreUpdate(true);
		ZombleSceneManager.sharedInstance().getEngine().runOnUpdateThread(new Runnable(){
			@Override
			public void run() {

				pItem.clearUpdateHandlers();
				pItem.clearEntityModifiers();
				pItem.setVisible(false);
			}
		});
	}

	@Override
	protected void onHandleObtainItem(final ZombleItem pItem) {
		pItem.setIgnoreUpdate(false);
		pItem.resetInteractions();
	}
}