package com.maprice.zomble.items;

import java.util.LinkedList;
import java.util.List;

import org.andengine.entity.Entity;
import org.andengine.opengl.texture.region.TiledTextureRegion;

import android.graphics.Point;
import android.util.Log;

import com.maprice.zomble.enemy.ZombleEnemy;
import com.maprice.zomble.hero.ZombleHero;
import com.maprice.zomble.map.ZombleMapManager;
import com.maprice.zomble.scene.ZombleSceneManager;
import com.maprice.zomble.util.contants.ZombleCallback;
import com.maprice.zomble.util.contants.ZombleConstants;


/**
 * Manages the all activity with respect to items
 * 
 * @author maprice
 */


public class ZombleItemManager implements ZombleConstants{
	// ===========================================================
	// Constants
	// ===========================================================

	static final String LOG_TAG = PREFIX + ZombleItemManager.class.getSimpleName();

	public enum ItemType{
		FOOD,
		AMMO,
		HEALTH,
		BATTERY,
	}
	
	// ===========================================================
	// Fields
	// ===========================================================

	final ZombleItemPool mItemPool;
	
	private List<ZombleItem> mItemList = new LinkedList<ZombleItem>();
	
	// ===========================================================
	// Constructors
	// ===========================================================

	public ZombleItemManager(TiledTextureRegion pItemTextureRegion){
		mItemPool = new ZombleItemPool(pItemTextureRegion);
	}

	// ===========================================================
	// Methods
	// ===========================================================


	public void spawnItem(final float pX, final float pY){
		
		ZombleSceneManager.sharedInstance().getEngine().runOnUpdateThread(new Runnable(){

			@Override
			public void run() {
				ZombleItem zomble = mItemPool.obtainPoolItem();
				zomble.setPosition(pX, pY);
				
				mItemList.add(zomble);
				
				if(!zomble.hasParent()){
				ZombleSceneManager.sharedInstance().getCurrentScene().attachChild(zomble);
				}
				
			}
		});
}
	
	int zomblenum = 10;
	
	public void clearItems(){
		Log.e(LOG_TAG, "Nuked items");
		
		for(ZombleItem lEnemy : mItemList){
			mItemPool.recyclePoolItem(lEnemy);
		}
		
		mItemList.clear();
	}
	
	public void onSwitchMap() {
		Log.e(LOG_TAG, "Added items");
		for(int i = 0; i < zomblenum; i++){
		
			Point lSpawnLocation = ZombleMapManager.sharedInstance().getValidItemSpawnPoint();
			this.spawnItem(lSpawnLocation.x, lSpawnLocation.y);
		}
		ZombleSceneManager.sharedInstance().getCurrentScene().sortChildren();

	}

	public List<ZombleItem> getItems() {
		return mItemList;
	}

	public void removeItem(ZombleItem mZomble) {
		mItemList.remove(mZomble);
		mItemPool.recyclePoolItem(mZomble);		
	}
	
}