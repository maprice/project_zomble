package com.maprice.zomble.casing;

import org.andengine.opengl.texture.region.TiledTextureRegion;
import org.andengine.util.adt.pool.GenericPool;

import com.maprice.zomble.scene.ZombleSceneManager;
import com.maprice.zomble.util.contants.ZombleConstants;

/**
 * Pool system that controls the allocation and deletion of casings
 * 
 * @author maprice
 */


public class ZombleCasingPool extends GenericPool<ZombleCasing> implements ZombleConstants{

	// ===========================================================
	// Fields
	// ===========================================================

	final TiledTextureRegion mCasingTextureRegion;

	// ===========================================================
	// Constructors
	// ===========================================================

	public ZombleCasingPool(TiledTextureRegion pCasingTextureRegion) {
		mCasingTextureRegion = pCasingTextureRegion;
	}

	// ===========================================================
	// Methods
	// ===========================================================


	@Override
	protected ZombleCasing onAllocatePoolItem() {
		ZombleCasing casing = new ZombleCasing(0, 0, mCasingTextureRegion);
		return casing;
	}


	@Override
	protected void onHandleRecycleItem(final ZombleCasing pCasing) {
		ZombleSceneManager.sharedInstance().getEngine().runOnUpdateThread(new Runnable(){
			@Override
			public void run() {
				pCasing.setIgnoreUpdate(true);
				pCasing.clearUpdateHandlers();
				pCasing.clearEntityModifiers();
				pCasing.setVisible(false);
			}
		});
	}


	@Override
	protected void onHandleObtainItem(final ZombleCasing pCasing) {
		pCasing.setIgnoreUpdate(false);
		pCasing.resetInteractions();
	}
}

