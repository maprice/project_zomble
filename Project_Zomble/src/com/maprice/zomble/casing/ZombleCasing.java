package com.maprice.zomble.casing;

import java.util.Random;

import org.andengine.engine.handler.physics.PhysicsHandler;
import org.andengine.entity.IEntity;
import org.andengine.entity.sprite.TiledSprite;
import org.andengine.opengl.texture.region.ITiledTextureRegion;
import org.andengine.util.math.MathUtils;

import com.maprice.zomble.ZombleSoundManager;
import com.maprice.zomble.scene.ZombleSceneManager;
import com.maprice.zomble.util.contants.ZombleConstants;


/**
 * Single casing (discharge from gun)
 * 
 * @author maprice
 */

public class ZombleCasing extends TiledSprite implements ZombleConstants{
	// ===========================================================
	// Constants
	// ===========================================================

	static final String LOG_TAG = PREFIX + ZombleCasing.class.getSimpleName();

	// ===========================================================
	// Fields
	// ===========================================================
	private float mGround;
	
	final private PhysicsHandler mPhysicsHandler = new PhysicsHandler(this){
		@Override
		protected void onUpdate(final float pSecondsElapsed, final IEntity pEntity) {
			super.onUpdate(pSecondsElapsed,pEntity);

			// When the casing reaches the ground
			if(pEntity.getY() > mGround){
				if(sound == false){
				 ZombleSoundManager.sharedInstance().playSound(1);
				 sound = true;
				}
				// If the next bounce wont go higher then 20 pixels
				if(this.getVelocityY()*CASING_ELASTICITY<20){

					// Stop all motion and remove the handler
					mPhysicsHandler.setVelocity(0, 0);
					mPhysicsHandler.setAcceleration(0, 0);
					pEntity.clearUpdateHandlers();
					sound = false;
				}
				else{
					// Cause the casing to bounce and lose CASING_ELASTICITY of its energy
					mPhysicsHandler.setVelocity(this.getVelocityX()*CASING_ELASTICITY, -this.getVelocityY()*CASING_ELASTICITY);

					// Draw the casing BEHIND the character
					ZombleCasing.this.setZIndex(0);
					ZombleCasing.this.getParent().sortChildren();

				}
			}
		}
	};


	// ===========================================================
	// Constructors
	// ===========================================================

	public ZombleCasing(float pX, float pY, ITiledTextureRegion pTiledTextureRegion) {
		super(pX, pY, pTiledTextureRegion,
				ZombleSceneManager.sharedInstance().getVBOmanager());
	}

	// ===========================================================
	// Methods
	// ===========================================================


	public void resetInteractions(){
		this.setAlpha(1.0f);
		this.registerUpdateHandler(mPhysicsHandler);
	}


	/**
	 * Starts the motion of the casing
	 * 
	 * @param pAngle
	 *            angle the bullet will travel at (Radians [0 , 2PI])
	 * @param pAngle 
	 * @param pY 
	 *            
	 */
	public void start(float pX, float pY, double pAngle) {
		Random rand = new Random();

		int groundYOffset = rand.nextInt((int) (CASING_RANDOM_GROUND_RANGE)) + CASING_BASE_GROUND;

		// Set the lowest value for the casing
		mGround = pY + groundYOffset;

		this.setPosition(pX, pY);

		// Set the casing's rotation in the direction the gun is being fired
		this.setRotation(MathUtils.radToDeg((float)-pAngle));

		// Create random offsets for both the x and y velocity of the casing
		//Random rand = new Random();

		int xVelocity = rand.nextInt((int) CASING_RANDOM_VELOCITY_X_RANGE) + CASING_BASE_VELOCITY_X;
		int yVelocity = rand.nextInt((int) CASING_RANDOM_VELOCITY_Y_RANGE) + CASING_BASE_VELOCITY_Y;

		mPhysicsHandler.setVelocity((float) (xVelocity*Math.sin(pAngle)), -yVelocity);

		// Constant downward acceleration
		mPhysicsHandler.setAccelerationY(GRAVITY);
	}

	// ghetto
	boolean sound = false;

}