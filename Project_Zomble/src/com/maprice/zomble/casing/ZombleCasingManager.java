package com.maprice.zomble.casing;

import org.andengine.entity.IEntity;
import org.andengine.entity.modifier.AlphaModifier;
import org.andengine.entity.modifier.DelayModifier;
import org.andengine.opengl.texture.region.TiledTextureRegion;
import org.andengine.util.modifier.ease.EaseExponentialIn;

import com.maprice.zomble.scene.ZombleSceneManager;
import com.maprice.zomble.util.contants.ZombleConstants;



/**
 * Manages the all activity with respect to casings
 * 
 * @author maprice
 */


public class ZombleCasingManager implements ZombleConstants{
	// ===========================================================
	// Constants
	// ===========================================================

	static final String LOG_TAG = PREFIX + ZombleCasingManager.class.getSimpleName();

	// ===========================================================
	// Fields
	// ===========================================================

	private final ZombleCasingPool mCasingPool;

	// ===========================================================
	// Constructors
	// ===========================================================

	public ZombleCasingManager(TiledTextureRegion pCasingTextureRegion){
		mCasingPool = new ZombleCasingPool(pCasingTextureRegion);
	}

	// ===========================================================
	// Methods
	// ===========================================================

	/**
	 * Creates a bullet casing, discharging it from the gun and then simulates gravity
	 * 
	 * @param pX
	 *            starting x position
	 *            
	 * @param pY
	 *            starting y position
	 *                    
	 * @param pAngle
	 *            angle that the BULLET is being shot
	 */
	public void dischargeCasing(float pX, float pY, double pAngle, int index) {

		// Calculate offset of the casing
		float offsetX1 =  pX+(float)(ITEM_X_OFFSET_MULTIPLIER * 3.2 * Math.cos(-pAngle));
		float offsetY1 =  -10+pY+(float)(ITEM_Y_OFFSET_MULTIPLIER * 3.2 * Math.sin(-pAngle));
		
		// Get new casing
		ZombleCasing casing = mCasingPool.obtainPoolItem();

		// Use correct casing image
		casing.setCurrentTileIndex(index);

		// Fade blood out exponentially
		casing.registerEntityModifier(new AlphaModifier(3, 1, 0, EaseExponentialIn.getInstance()));

		// Delay modifier used to call reycle after a set time
		casing.registerEntityModifier(new DelayModifier(4){

			@Override
			public void onModifierFinished(IEntity pItem) {
				mCasingPool.recyclePoolItem((ZombleCasing) pItem);

			}
		});

		// Attach casing to scene
		if(!casing.hasParent()){
			ZombleSceneManager.sharedInstance().getCurrentScene().attachChild(casing);
		}
		
		// Casing should be draw BEHIND of the character
		// TODO: create a global constant to organise layers
		if((pAngle < Math.PI)){

			casing.setZIndex(Layer.BACK_EFFECTS.ordinal());
			casing.getParent().sortChildren();
		}
		// Gun should be draw INFRONT of the character
		else{
			casing.setZIndex(Layer.FRONT_EFFECTS.ordinal());
			casing.getParent().sortChildren();
		}

		// Discharge casing
		casing.start(offsetX1, offsetY1, pAngle);
	}


}
