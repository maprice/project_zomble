package com.maprice.zomble.map;


import java.util.LinkedList;
import java.util.List;

import org.andengine.extension.tmx.TMXLayer;
import org.andengine.extension.tmx.TMXLoader;
import org.andengine.extension.tmx.TMXLoader.ITMXTilePropertiesListener;
import org.andengine.extension.tmx.TMXProperties;
import org.andengine.extension.tmx.TMXTile;
import org.andengine.extension.tmx.TMXTileProperty;
import org.andengine.extension.tmx.TMXTiledMap;
import org.andengine.extension.tmx.util.exception.TMXLoadException;
import org.andengine.opengl.texture.TextureManager;
import org.andengine.opengl.texture.TextureOptions;
import org.andengine.opengl.vbo.VertexBufferObjectManager;
import org.andengine.util.algorithm.path.ICostFunction;
import org.andengine.util.algorithm.path.IPathFinderMap;
import org.andengine.util.algorithm.path.Path;
import org.andengine.util.algorithm.path.astar.AStarPathFinder;
import org.andengine.util.algorithm.path.astar.EuclideanHeuristic;

import android.content.res.AssetManager;
import android.graphics.Point;
import android.util.Log;

import com.maprice.zomble.projectile.ZombleCollisionManager.Direction;
import com.maprice.zomble.scene.ZombleSceneManager;
import com.maprice.zomble.util.contants.ZombleCallback;
import com.maprice.zomble.util.contants.ZombleConstants;
import com.maprice.zomble.util.contants.ZombleUtil;



/**
 * <Class Description>
 * 
 * @author maprice
 */


public class ZombleMapManager implements ZombleConstants{
	// ===========================================================
	// Constants
	// ===========================================================


	static final String LOG_TAG = PREFIX + ZombleMapManager.class.getSimpleName();
	static final String TMX_PATH = "tmx/";
	static final String TMX_EXTENSION = ".tmx";

	private static final ZombleMapManager sZombleMapManager = new ZombleMapManager();

	// ===========================================================
	// Fields
	// ===========================================================

	// TODO: Generate this
	final private String mapArray[][] = {
			{"suburb_se1", "suburb_sw1"},
			{"suburb_ne1", "suburb_nw1"}
	};

	private int currentMapCol;
	private int currentMapRow;

	private TMXLayer mTmxLayer;
	private TMXTiledMap mTMXTiledMap;

	final AStarPathFinder<TMXLayer> mAStar = new AStarPathFinder<TMXLayer>();

	// No heuristic needed
	final EuclideanHeuristic<TMXLayer> mHeuristic = new EuclideanHeuristic<TMXLayer>();

	// Define the block behavior
	final IPathFinderMap<TMXLayer> mPathMap = new IPathFinderMap<TMXLayer>()
	{
		@Override
		public boolean isBlocked(int pX, int pY, TMXLayer pEntity) {
			TMXTile x = mTmxLayer.getTMXTile(pX, pY);
			TMXProperties<TMXTileProperty> properties = x.getTMXTileProperties(mTMXTiledMap);

			if(properties != null && properties.containsTMXProperty("wall", "true")){
				return true;
			}
			else{
				return false;
			}
		}

	};

	// Define how cost is determined
	final ICostFunction<TMXLayer> mCostCallback = new ICostFunction<TMXLayer>()
	{
		@Override
		public float getCost(IPathFinderMap<TMXLayer> pPathFinderMap,
				int pFromX, int pFromY, int pToX, int pToY, TMXLayer pEntity) {

			return 0;
		}
	};


	// ===========================================================
	// Constructors
	// ===========================================================


	/**
	 * Returns shared instance of the map manager
	 * 
	 * @return
	 *            Shared map manager
	 */
	public static ZombleMapManager sharedInstance() {
		return sZombleMapManager;
	}


	public void initMapManager() {
		mEnemySpawnLocations = new LinkedList<Point>();
		mItemSpawnLocations = new LinkedList<Point>();
		
		currentMapRow = 0;
		currentMapCol = 0;

		loadNewMap(mapArray[currentMapRow][currentMapCol], new ZombleCallback<TMXTiledMap>(){

			@Override
			public void onComplete(TMXTiledMap pResult) {
				mTMXTiledMap = pResult;

				mTmxLayer = mTMXTiledMap.getTMXLayers().get(0);
				mTmxLayer.setScale(SCALE);
				mTmxLayer.setScaleCenter(0, 0);
				mTmxLayer.setZIndex(Layer.BACKGROUND.ordinal());

				ZombleSceneManager.sharedInstance().getCurrentScene().attachChild(mTmxLayer);
				ZombleSceneManager.sharedInstance().getCurrentScene().registerTouchArea(mTmxLayer);

			}

			@Override
			public void onError(String pErrorMessage) {
				Log.e(LOG_TAG, "Error loading map for the first time: " + pErrorMessage);
			}
		});			
	}

	// ===========================================================
	// Methods
	// ===========================================================

	public TMXLayer getCurrentLayer(){
		return mTmxLayer;
	}

	public int getMapColumns(){
		return mTMXTiledMap.getTileColumns();
	}

	public int getMapRows(){
		return mTMXTiledMap.getTileRows();
	}

	private List<Point> mEnemySpawnLocations;
	private List<Point> mItemSpawnLocations;
	private Point mStartSpawn;

	public void loadNextMap(Direction pDirection, ZombleCallback<TMXTiledMap> pZombleCallback){
		switch(pDirection){
		case NORTH:
			currentMapRow--;
			break;
		case EAST:
			currentMapCol++;
			break;
		case SOUTH:
			currentMapRow++;
			break;
		case WEST:
			currentMapCol--;
			break;

		default:
			throw new RuntimeException("Invalid map direction");
		}
		loadNewMap(mapArray[currentMapRow][currentMapCol], pZombleCallback);
	}

	public Point getValidSpawnPoint(){
		int lLocation = ZombleUtil.getRandom(0, mEnemySpawnLocations.size());
		Point lPoint = 	mEnemySpawnLocations.get(lLocation);
		mEnemySpawnLocations.remove(lLocation);
		return lPoint;
	}

	public Point getStartingSpawn(){
		return mStartSpawn;
	}
	

	public Point getValidItemSpawnPoint() {
		int lLocation = ZombleUtil.getRandom(0, mItemSpawnLocations.size());
		Point lPoint = 	mItemSpawnLocations.get(lLocation);
		mItemSpawnLocations.remove(lLocation);
		return lPoint;
	}
	
	public void loadNewMap(String pMapName, ZombleCallback<TMXTiledMap> pZombleCallback){

		mEnemySpawnLocations.clear();
		mItemSpawnLocations.clear();
		TMXTiledMap lTMXTiledMap = null;
		AssetManager lAssets = ZombleSceneManager.sharedInstance().getContext().getAssets();
		TextureManager lTextureManager = ZombleSceneManager.sharedInstance().getEngine().getTextureManager();
		VertexBufferObjectManager lVertexBufferObjectManager = ZombleSceneManager.sharedInstance().getVBOmanager();

		try {
			final TMXLoader tmxLoader = new TMXLoader(lAssets, lTextureManager, TextureOptions.NEAREST, lVertexBufferObjectManager, new ITMXTilePropertiesListener() {
				@Override
				public void onTMXTileWithPropertiesCreated(final TMXTiledMap pTMXTiledMap, final TMXLayer pTMXLayer, final TMXTile pTMXTile, final TMXProperties<TMXTileProperty> pTMXTileProperties) {

					if(pTMXTileProperties.containsTMXProperty("spawnable", "true")) {
						mItemSpawnLocations.add(new Point((pTMXTile.getTileX()+pTMXTiledMap.getTileWidth()/2)*(int)SCALE, (pTMXTile.getTileY() + pTMXTiledMap.getTileHeight()/2)*(int)SCALE));
						mEnemySpawnLocations.add(new Point((pTMXTile.getTileX()+pTMXTiledMap.getTileWidth()/2)*(int)SCALE, (pTMXTile.getTileY() + pTMXTiledMap.getTileHeight()/2)*(int)SCALE));
					}
					else if(pTMXTileProperties.containsTMXProperty("startSpawn", "true")) {
						mStartSpawn = new Point((pTMXTile.getTileX()+pTMXTiledMap.getTileWidth()/2)*(int)SCALE, (pTMXTile.getTileY() + pTMXTiledMap.getTileHeight()/2)*(int)SCALE);
					}

				}
			});
			lTMXTiledMap = tmxLoader.loadFromAsset(this.getMapPath(pMapName));
		} catch (final TMXLoadException tmxle) {
			pZombleCallback.onError("Could not load map: " + tmxle);
		}
		pZombleCallback.onComplete(lTMXTiledMap);
	}

	private String getMapPath(String pMapName){
		return TMX_PATH + pMapName + TMX_EXTENSION;
	}

	public void switchMaps(final TMXTiledMap lTMXTiledMap) {

		ZombleSceneManager.sharedInstance().getCurrentScene().unregisterTouchArea(ZombleMapManager.this.getCurrentLayer());
		ZombleSceneManager.sharedInstance().getCurrentScene().detachChild(ZombleMapManager.this.getCurrentLayer());

		mTmxLayer = lTMXTiledMap.getTMXLayers().get(0);
		mTmxLayer.setScale(SCALE);
		mTmxLayer.setScaleCenter(0, 0);
		mTmxLayer.setZIndex(Layer.BACKGROUND.ordinal());

		ZombleSceneManager.sharedInstance().getCurrentScene().attachChild(mTmxLayer);
		ZombleSceneManager.sharedInstance().getCurrentScene().registerTouchArea(mTmxLayer);

		mTMXTiledMap = lTMXTiledMap;
	}


	public Path findPath(float pFromX, float pFromY, float pToX, float pToY){

		Path lAStarPath;

		/* Get the tile the feet of the player are currently waking on. */
		final TMXTile lFromTmxTile = mTmxLayer.getTMXTileAt(pFromX, pFromY);
		final TMXTile lToTmxTile = mTmxLayer.getTMXTileAt(pToX, pToY);

		// Path is in the same tile
		if(lFromTmxTile == lToTmxTile){
			Path lShortPath = new Path(2);
			lShortPath.set(0, (int)pFromX, (int)pFromY);
			lShortPath.set(1, (int)pToX, (int)pToY);
			return lShortPath;
		}

		int lFromCol = lFromTmxTile.getTileColumn();
		int lFromRow = lFromTmxTile.getTileRow();

		int lToCol =  lToTmxTile.getTileColumn();
		int lToRow = lToTmxTile.getTileRow();


		lAStarPath = mAStar.findPath(
				mPathMap, 														// IPathFinderMap defined above
				0, 0,  															// Min X and min Y values
				mTMXTiledMap.getTileColumns()-1, mTMXTiledMap.getTileRows()-1,  // Max X and max Y values
				mTMXTiledMap.getTMXLayers().get(0), 							// The TMXLayer the walls are defined on
				lFromCol, lFromRow, 											// Starting column and row positions
				lToCol, lToRow,  												//  Ending column and row positions
				true, 															// Diagonal movement
				mHeuristic, 													// Heuristics defined above
				mCostCallback, 													// CostFunction defined above
				0); 															// Max cost of a path

		if(lAStarPath == null)
		{
			return null;
		}

		return convertToCoordinates(lAStarPath, pFromX, pFromY, pToX, pToY);
	}


	public float convertToPoints(float pTileCoord) {
		return pTileCoord*mTMXTiledMap.getTileWidth()*SCALE + mTMXTiledMap.getTileWidth()*SCALE/2;
	}


	public Path convertToCoordinates(Path tilePath, float pFromX, float pFromY, float pToX, float pToY) {
		Path lCoordinatePath = new Path(tilePath.getLength());

		// Set first position as current character position
		lCoordinatePath.set(0, (int)pFromX, (int)pFromY);

		for(int i = 1; i < tilePath.getLength(); i++){
			lCoordinatePath.set(i, (int)convertToPoints(tilePath.getX(i)), (int)convertToPoints(tilePath.getY(i)));
		}

		// Set the final position as the player selected coordinate
		lCoordinatePath.set(tilePath.getLength()-1, (int)pToX, (int)pToY);

		return lCoordinatePath;
	}


}