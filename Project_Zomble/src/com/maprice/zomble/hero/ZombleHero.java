package com.maprice.zomble.hero;


import java.util.Timer;
import java.util.TimerTask;

import org.andengine.opengl.texture.region.TiledTextureRegion;
import org.andengine.util.Constants;
import org.andengine.util.algorithm.path.Path;
import org.andengine.util.color.Color;

import android.util.Log;

import com.maprice.zomble.enemy.ZombleEnemy;
import com.maprice.zomble.hud.ZombleHUDManager;
import com.maprice.zomble.hud.ZombleSelectorManager;
import com.maprice.zomble.hud.ZombleWaypoint;
import com.maprice.zomble.items.ZombleItem;
import com.maprice.zomble.map.ZombleMapManager;
import com.maprice.zomble.projectile.ZombleProjectileManager;
import com.maprice.zomble.util.contants.ZombleConstants;
import com.maprice.zomble.util.contants.ZomblePathMapper;
import com.maprice.zomble.util.contants.ZombleUtil;

/**
 * Wraps the statistics of a zomble with its entity
 * 
 * @author maprice
 */


public class ZombleHero implements ZombleConstants {

	// ===========================================================
	// Constants
	// ===========================================================

	static final String LOG_TAG = PREFIX + ZombleHero.class.getSimpleName();

	// ===========================================================
	// Fields
	// ===========================================================

	final public int ID;
	final private ZombleHeroEntity mZombleEnemyCharacterEntity;
	final private ZombleHeroStats mZombleEnemyStats;
	final private ZombleWaypoint mZombleWaypoint;
	final private ZombleSelectorManager mSelectorManager;
	final private ZombleMapManager mMapManager;
	private ZomblePathMapper mPathMapper;

	private Path mCurrentPath;
	private int mCurrentPathIndex = 1;
	
	private boolean mShouldUpdateHunger = true;
	private Timer mHungerCoolDownTimer = new Timer();
	

	// ===========================================================
	// Constructors
	// ===========================================================

	public ZombleHero(int pID, float pX, float pY,
			TiledTextureRegion pCharacterTextureRegion,
			TiledTextureRegion pItemTextureRegion,
			TiledTextureRegion mWaypointTextureRegion,
			TiledTextureRegion mSelectorTextureRegion,
			TiledTextureRegion pCharacterSelectorTextureRegion
	) {
		ID = pID;

		mMapManager = ZombleMapManager.sharedInstance();

		mZombleEnemyCharacterEntity = new ZombleHeroEntity(
				pX, 
				pY,
				pCharacterTextureRegion,
				pItemTextureRegion,
				pCharacterSelectorTextureRegion);


		mZombleWaypoint = new ZombleWaypoint(pX, pY, mWaypointTextureRegion);
		mSelectorManager = new ZombleSelectorManager(mSelectorTextureRegion); 

		mZombleEnemyStats = new ZombleHeroStats(ID, 1.0f, 100, 1);

		if(DEBUG){
			mPathMapper = new ZomblePathMapper(Color.GREEN);
		}
	}


	// ===========================================================
	// Methods
	// ===========================================================


	public ZombleHeroEntity getEntity(){
		return mZombleEnemyCharacterEntity;
	}

	public ZombleHeroStats getStats(){
		return mZombleEnemyStats;
	}


	/**
	 * Called when zomble takes damage
	 * 
	 * @param pBulletDirection
	 *            the angle the bullet is travelling (Radians [0 , 2PI])
	 *            
	 * @param pAmount
	 *            the amount of damage done
	 *          
	 *            
	 */
	public void takeDamage(int pAmount) {
		//Subtract health and check if it is less than or equal to zero
		if(mZombleEnemyStats.subtractHealth(pAmount)){
			//ZombleHUDHealth.sharedHud().updateCounter();
		}

		//Update health bar
		ZombleHUDManager.sharedHud().getHUD(ID).updateHealth(mZombleEnemyStats.getTotalHealth(), mZombleEnemyStats.getCurrentHealth());
	}



	public void setWaypoint(float pX, float pY) {
		// Find new path
		Path lNewPath = mMapManager.findPath(
				this.getEntity().getCoordinates()[Constants.VERTEX_INDEX_X],
				this.getEntity().getCoordinates()[Constants.VERTEX_INDEX_Y], 
				pX,
				pY
		);



		// If new path exists
		if(lNewPath != null){
			mCurrentPath = lNewPath;
			mCurrentPathIndex = 1;
			mZombleWaypoint.setWaypoint(pX, pY);

			if(DEBUG){
				mPathMapper.drawPath(mCurrentPath);
			}
		}
		else
		{
			Log.i(LOG_TAG, "No possible path to that tile");
		}
	}

	public void clearWaypoint() {
		//mZombleWaypoint.removeWaypoint();
		mCurrentPath = null;
		if(DEBUG){
			mPathMapper.clearPath();
		}
	}

	public void setSelected(boolean pSelected){
		this.getEntity().setSelected(pSelected);
		ZombleHUDManager.sharedHud().getHUD(ID).setSelected(pSelected);

	}

	public void setTarget(ZombleEnemy pZomble) {
		mSelectorManager.setTarget(pZomble);
	}
	
	public void clearTarget() {
		mSelectorManager.clearTarget();
	}

	public void onUpdate(ZombleProjectileManager mProjectileManager) {
		onUpdateHunger();
		
		if(mCurrentPath != null ){
			onUpdateFollowPath();
		}
		else{
			onUpdateShoot(mProjectileManager);
		}
	}

	private void onUpdateHunger() {
		if(shouldUpdateHunger()){
			mZombleEnemyStats.loseFood(1);
		}
	}

	public boolean shouldUpdateHunger() {
		if (mShouldUpdateHunger) {
			mShouldUpdateHunger = false;
			mHungerCoolDownTimer.schedule(new HungerCoolDownTask(), 1000);
			return true;
		}
		return false;
	}

	final class HungerCoolDownTask extends TimerTask {
		public void run() {
			mShouldUpdateHunger = true;
		}
	}



	private void onUpdateFollowPath(){
		final float[] lPlayerCordinates = this.getEntity().getCoordinates();

		// The player has reached a waypoint
		if(ZombleUtil.collidesWith(lPlayerCordinates[Constants.VERTEX_INDEX_X], lPlayerCordinates[Constants.VERTEX_INDEX_Y] ,mCurrentPath.getX(mCurrentPathIndex), mCurrentPath.getY(mCurrentPathIndex), 1)) {
			// The player has reached their final destination
			if(mCurrentPathIndex == mCurrentPath.getLength()-1){
				this.clearWaypoint();
				this.getEntity().stop();
			}
			else{
				mCurrentPathIndex++;
			}
			// Move to waypoint
		} else {

			float lDeltaX =  mCurrentPath.getX(mCurrentPathIndex) - lPlayerCordinates[Constants.VERTEX_INDEX_X];
			float lDeltaY =  mCurrentPath.getY(mCurrentPathIndex) - lPlayerCordinates[Constants.VERTEX_INDEX_Y];

			double angleInDegrees = Math.atan2(lDeltaY, lDeltaX);

			// Sets the velocity in the correct direction
			float lX = (float) Math.cos(angleInDegrees);
			float lY = (float) Math.sin(angleInDegrees);

			this.getEntity().walk(lX, lY);
			this.getEntity().look(lX, lY);
		}
	}

	private void onUpdateShoot(ZombleProjectileManager mProjectileManager){
		this.getEntity().stop();

		if(mSelectorManager.hasTarget()){
			float lDeltaX =  mSelectorManager.getTarget().getX() - this.getEntity().getX();
			float lDeltaY =  mSelectorManager.getTarget().getY() - (this.getEntity().getY());

			double angleInDegrees = Math.atan2(lDeltaY, lDeltaX);

			// Sets the velocity in the correct direction
			float lX = (float) Math.cos(angleInDegrees);
			float lY = (float) Math.sin(angleInDegrees);
			this.getEntity().look(lX, lY);

			if(this.getEntity().shoot()){
				// TODO: replace with a callback
				mProjectileManager.shootProjectile(this.getEntity().getX(), this.getEntity().getY(), this.getEntity().getCurrentAngle(),this.getStats().getGunType(), this);
				ZombleHUDManager.sharedHud().getHUD(ID).startGunCooldown(0.3f);
				mZombleEnemyStats.decrementAmmo(1);
			}
		}
	}
	
	public void useAbility(ZombleProjectileManager mProjectileManager){
		this.getEntity().stop();
		this.clearWaypoint();
		Log.e("sdf", "shootin");
		if(mSelectorManager.hasTarget()){
			Log.e("sdf", "igotsatarget");
			float lDeltaX =  mSelectorManager.getTarget().getX() - this.getEntity().getX();
			float lDeltaY =  mSelectorManager.getTarget().getY() - (this.getEntity().getY());

			double angleInDegrees = Math.atan2(lDeltaY, lDeltaX);

			// Sets the velocity in the correct direction
			float lX = (float) Math.cos(angleInDegrees);
			float lY = (float) Math.sin(angleInDegrees);
			this.getEntity().look(lX, lY);


			// TODO: replace with a callback
			mProjectileManager.shootSpecialProjectile(this.getEntity().getX(), this.getEntity().getY(), this.getEntity().getCurrentAngle(),this.getStats().getGunType(), this);
			ZombleHUDManager.sharedHud().getHUD(ID).startAbilityCooldown(4.0f);
			mZombleEnemyStats.decrementAmmo(1);

		}
	}


	public void onOrbCollect(int pXP) {
		this.getStats().gainXP(pXP);
	}


	public void obtainItem(ZombleItem mZomble) {
		switch(mZomble.getItemType()){
		case HEALTH:
			mZombleEnemyStats.gainHealth(10);
			break;
		case AMMO:
			mZombleEnemyStats.gainAmmo(30);
			break;
		case BATTERY:
			
			break;
		case FOOD:
			mZombleEnemyStats.gainFood(50);
			break;
		}
	}
}