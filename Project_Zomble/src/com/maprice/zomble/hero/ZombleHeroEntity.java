package com.maprice.zomble.hero;

import org.andengine.engine.handler.physics.PhysicsHandler;
import org.andengine.entity.primitive.Rectangle;
import org.andengine.opengl.texture.region.TiledTextureRegion;

import com.maprice.zomble.entity.ZombleBaseCharacterEntity;
import com.maprice.zomble.entity.ZombleBaseCharacterSprite;
import com.maprice.zomble.hud.ZombleCharacterSelector;
import com.maprice.zomble.scene.ZombleSceneManager;
import com.maprice.zomble.util.contants.ZombleConstants;
import com.maprice.zomble.util.contants.ZombleUtil;


/**
 * Player controlled entity
 * 
 * @author mprice
 */


public class ZombleHeroEntity extends ZombleBaseCharacterEntity implements ZombleConstants{
	// ===========================================================
	// Constants
	// ===========================================================

	static final String LOG_TAG = PREFIX + ZombleHeroEntity.class.getSimpleName();

	public enum CharacterLayer {
		BEHIND (2),
		CHARACTER (3),
		FRONT (4);

		final int value;   

		CharacterLayer(int value) {
			this.value = value;
		}

		public int index() { 
			return value; 
		}
	}


	// ===========================================================
	// Fields
	// ===========================================================

	final private ZombleBaseCharacterSprite mCharacterSprite;
	final private ZombleHeroArmSprite mGunSprite;
	final private PhysicsHandler mPhysicsHandler;
	final private ZombleCharacterSelector mCharacterSelector;
	// ===========================================================
	// Constructors
	// ===========================================================

	public ZombleHeroEntity(float pX, float pY,
			TiledTextureRegion characterTextureRegion,
			TiledTextureRegion itemTextureRegion, 
			TiledTextureRegion pCharacterSelectorTextureRegion) 
	{
		super(pX,pY);
		mCharacterSelector = new ZombleCharacterSelector(.3f, characterTextureRegion.getHeight()/2.5f, pCharacterSelectorTextureRegion);


		mCharacterSprite = new ZombleBaseCharacterSprite(-characterTextureRegion.getWidth()/2, -characterTextureRegion.getHeight()/2, characterTextureRegion, HERO_FRAME_SPEED);
		mGunSprite = new ZombleHeroArmSprite(-itemTextureRegion.getWidth()/2, -itemTextureRegion.getHeight()/2, itemTextureRegion);
		
		this.setTouchHitbox(new Rectangle(
				-characterTextureRegion.getWidth()*TOUCH_HITBOX_SCALE_X/2,  
				-characterTextureRegion.getHeight()*TOUCH_HITBOX_SCALE_Y/2,
				characterTextureRegion.getWidth()*TOUCH_HITBOX_SCALE_X,  
				characterTextureRegion.getHeight()*TOUCH_HITBOX_SCALE_Y,
				ZombleSceneManager.sharedInstance().getVBOmanager()));
		
		this.setSpriteHitbox(new Rectangle(
				-characterTextureRegion.getWidth()/2,  
				-characterTextureRegion.getHeight()/2,
				characterTextureRegion.getWidth(),  
				characterTextureRegion.getHeight(),
				ZombleSceneManager.sharedInstance().getVBOmanager()));
		
		this.setFeetHitbox(new Rectangle(
				-characterTextureRegion.getWidth()/2,  
				characterTextureRegion.getHeight()/4,
				characterTextureRegion.getWidth(),  
				characterTextureRegion.getHeight()/4,
				ZombleSceneManager.sharedInstance().getVBOmanager()));


		mPhysicsHandler = new PhysicsHandler(this);

		// Set scale
		this.setScale(SCALE);

		// Add sprites to entity
		this.attachChild(mCharacterSprite);
		this.attachChild(mGunSprite);
		this.attachChild(mCharacterSelector);
		this.registerUpdateHandler(mPhysicsHandler);
		
	}

	// ===========================================================
	// Methods
	// ===========================================================


	/**
	 * Moves the character in the appropriate direction and animates the sprite accordingly
	 * 
	 * @param pValueX
	 *            x value of the analog control
	 *            
	 * @param pValueY
	 *            y value of the analog control
	 */
	@Override
	public void walk(float pValueX, float pValueY){
		double angle = ZombleUtil.getAngle(pValueX, pValueY);

		// Animates the character sprite to walk in the correct direction
		mCharacterSprite.walk(angle);

		// Update the current velocity
		mPhysicsHandler.setVelocity(pValueX * HERO_WALK_SPEED_MULTIPLIER, pValueY * HERO_WALK_SPEED_MULTIPLIER);
	}



	/**
	 * Angles the character in the appropriate direction
	 * 
	 * @param pValueX
	 *            x value of the analog control
	 *            
	 * @param pValueY
	 *            y value of the analog control
	 */
	@Override
	public void look(float pValueX, float pValueY){

		// Get the angle in radians from [0, 2PI]
		double angle = ZombleUtil.getAngle(pValueX, pValueY);
		mCurrentAngle = angle;
		// Angles the gun in the correct orientation
		mGunSprite.look(angle);

		// Angles the character in the correct orientation
		mCharacterSprite.look(angle);
	}


	/**
	 * Stops all animation and movement of the character sprite 
	 */
	@Override
	public void stop() {
		// Stop motion of the character
		mPhysicsHandler.setVelocity(0, 0);

		// Stop animation of the character
		mCharacterSprite.stop();
	}



	/**
	 * Simulates recoil on the gun in the current angle
	 * @return 
	 */
	public boolean shoot() {
		return mGunSprite.shoot();
	}


	public void knockBack(double bulletDirection) {

		float gox = (float) Math.cos(bulletDirection);
		float goy = (float) Math.sin(-bulletDirection);

		// Update the current velocity
		mPhysicsHandler.setVelocity(gox * ENEMY_WALK_SPEED_MULTIPLIER * 2, goy * ENEMY_WALK_SPEED_MULTIPLIER * 2);

	}

	public void setSelected(boolean selected) {
		if(selected){
			mCharacterSelector.setSelected();
		}
		else{
			mCharacterSelector.deselect();
		}
	}

	public void onOrbCollected() {
		// TODO Auto-generated method stub
		
	}


}