package com.maprice.zomble.hero;

import android.util.Log;

import com.maprice.zomble.entity.ZombleBaseCharacterStats;
import com.maprice.zomble.hud.ZombleHUDManager;
import com.maprice.zomble.util.contants.ZombleConstants;


/**
 * All statistical information pertaining to the main character entity
 * 
 * @author mprice
 */

public class ZombleHeroStats extends ZombleBaseCharacterStats implements ZombleConstants {


	// ===========================================================
	// Constants
	// ===========================================================


	public enum GunType {
		PISTOL,
		SUBMACHINE,
		SHOTGUN,
		RIFLE,
		SNIPER;
	}

	static final String LOG_TAG = PREFIX + ZombleHeroStats.class.getSimpleName();

	// ===========================================================
	// Fields
	// ===========================================================

	private GunType mGurrentGun = GunType.RIFLE;
	
	private int mTotalXP;
	private int mTotalAmmo;
	private int mTotalHunger;

	private int mXP;
	private int mAmmo;
	private int mHunger;
	
	private int mLevel;
	final private int ID;
	// ===========================================================
	// Constructors
	// ===========================================================

	public ZombleHeroStats(int pID, float pSpeed, int pTotalHealth, int pDamage){
		super(pSpeed, pTotalHealth, pDamage);
		ID = pID;
		
		mTotalXP = 5;
		mTotalAmmo = 100;
		mTotalHunger = 100;
		
		mXP = 0;
		mAmmo = mTotalAmmo;
		mHunger = mTotalHunger;
		
		mLevel = 1;
	}

	// ===========================================================
	// Methods
	// ===========================================================

	public GunType getGunType(){
		return mGurrentGun;
	}

	public void gainXP(int pXP) {
		mXP += pXP;
		
		if(mXP > mTotalXP){
			mLevel++;
			mXP -= mTotalXP;
			mTotalXP = mTotalXP*2;
		}
		
		ZombleHUDManager.sharedHud().getHUD(ID).updateXP(mTotalXP, mXP);
		
	}
	
	public void setXP(int pXP) {
		this.mXP = pXP;
	}

	public int getXP() {
		return mXP;
	}
	
	public void setTotalXP(int pTotalXP) {
		this.mTotalXP = pTotalXP;
	}

	public int getTotalXP() {
		return mTotalXP;
	}
	
	public void gainAmmo(int pAmmo) {
		mAmmo += pAmmo;
		
		if(mAmmo > mTotalAmmo){
			mAmmo = mTotalAmmo;
		}
		
		ZombleHUDManager.sharedHud().getHUD(ID).updateAmmo(mTotalAmmo, mAmmo);
	}


	public void decrementAmmo(int pAmount) {
		mAmmo -= pAmount;

		if(mAmmo < 0){
			mAmmo = 0;
			Log.v(LOG_TAG, "Character "+ID+" is out of Ammo!");
		}

		ZombleHUDManager.sharedHud().getHUD(ID).updateAmmo(mTotalAmmo, mAmmo);
	}

	public void setAmmo(int pAmmo) {
		this.mAmmo = pAmmo;
	}

	public int getAmmo() {
		return mAmmo;
	}

	public void setTotalAmmo(int pTotalAmmo) {
		this.mTotalAmmo = pTotalAmmo;
	}

	public int getTotalAmmo() {
		return mTotalAmmo;
	}
	
	public void setHunger(int pHunger) {
		this.mHunger= pHunger;
	}

	public int getHunger() {
		return mHunger;
	}
	
	public void gainFood(int pHunger) {
		mHunger += pHunger;
		
		if(mHunger > mTotalHunger){
			mHunger = mTotalHunger;
		}
		
		ZombleHUDManager.sharedHud().getHUD(ID).updateHunger(mTotalHunger, mHunger);
	}
	

	public void loseFood(int pAmount) {
		mHunger -= pAmount;
		
		if(mHunger < 0){
			mHunger = 0;
			Log.v(LOG_TAG, "Character "+ID+" is starving!");
		}
		
		ZombleHUDManager.sharedHud().getHUD(ID).updateHunger(mTotalHunger, mHunger);
	}
	
	public void setTotalHunger(int pTotalHunger) {
		this.mTotalHunger = pTotalHunger;
	}

	public int getTotalHunger() {
		return mTotalHunger;
	}

	public void gainHealth(int pHealth) {
		mCurrentHealth += pHealth;
		
		if(mCurrentHealth > mTotalHealth){
			mCurrentHealth = mTotalHealth;
		}
		
		ZombleHUDManager.sharedHud().getHUD(ID).updateHealth(mTotalHealth, mCurrentHealth);
	}



}
