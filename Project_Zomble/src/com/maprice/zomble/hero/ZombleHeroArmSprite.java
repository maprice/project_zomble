package com.maprice.zomble.hero;

import org.andengine.engine.handler.physics.PhysicsHandler;
import org.andengine.entity.IEntity;
import org.andengine.opengl.texture.region.ITiledTextureRegion;

import com.maprice.zomble.entity.ZombleBaseCharacterArmSprite;
import com.maprice.zomble.scene.ZombleSceneManager;
import com.maprice.zomble.util.contants.ZombleConstants;

/**
 * Player controller arm sprite
 * 
 * @author maprice
 */

public class ZombleHeroArmSprite extends ZombleBaseCharacterArmSprite implements ZombleConstants{

	// ===========================================================
	// Constants
	// ===========================================================

	static final String LOG_TAG = PREFIX + ZombleHeroArmSprite.class.getSimpleName();

	// ===========================================================
	// Fields
	// ===========================================================


	final private RecoilHandler mPhysicsHandler;


	// ===========================================================
	// Constructors
	// ===========================================================

	public ZombleHeroArmSprite(float pX, float pY,
			ITiledTextureRegion pTiledTextureRegion) {
		super(pX, pY, pTiledTextureRegion, ZombleSceneManager.sharedInstance().getVBOmanager());

		mPhysicsHandler = new RecoilHandler(this);

		this.registerUpdateHandler(mPhysicsHandler);
	}


	// ===========================================================
	// Methods
	// ===========================================================


	/**
	 * Simulates recoil on the gun in the current angle
	 * @return 
	 */
	public boolean shoot() {
		return mPhysicsHandler.recoil();
	}

	/**
	 * Handles the recoil of a gun
	 */
	private class RecoilHandler extends PhysicsHandler{

		private float mX = 0;
		private float mY = 0;

		private boolean mShot = false;

		private boolean mVelocity = false;
		private boolean mAngle = false;


		public RecoilHandler(IEntity pEntity) {
			super(pEntity);
		}

		public boolean recoil(){

			// Only shoot again once full recoil has finished
			if(mShot == false){
				mX = ZombleHeroArmSprite.this.getX();
				mY = ZombleHeroArmSprite.this.getY();

				double xQuad = Math.cos(mGunAngle);
				double yQuad = Math.sin(mGunAngle);	

				// Angular
				float aV = (float) (-GUN_RECOIL_ANGULAR_VELOCITY * xQuad);
				float aA = (float) (GUN_RECOIL_ANGULAR_ACCEL * xQuad);

				// Linear x
				float xV = (float) (-GUN_RECOIL_VELOCITY * xQuad);
				float xA = (float) (GUN_RECOIL_ACCEL * xQuad);

				// Linear y
				float yV = (float) (GUN_RECOIL_VELOCITY * yQuad);
				float yA = (float) (-GUN_RECOIL_ACCEL * yQuad);


				float xR = ZombleHeroArmSprite.this.getWidth()/2;
				float yR =  ZombleHeroArmSprite.this.getHeight()/2;

				//Set the rotation center to always be the 'butt' of the gun
				ZombleHeroArmSprite.this.setRotationCenter(xR - (float)(xR * xQuad), yR + (float)(yR * yQuad));

				//Set initial angular velocity and acceleration
				this.setAngularVelocity(aV);
				this.setAngularAcceleration(aA);

				//Set initial linear velocity and acceleration
				this.setVelocity(xV, yV);
				this.setAcceleration(xA, yA);

				mShot = true;
				mVelocity = true;
				mAngle = true;
				return true;
			}
			return false;
		}

		@Override
		protected void onUpdate(final float pSecondsElapsed, final IEntity pEntity) {
			super.onUpdate(pSecondsElapsed,pEntity);

			// We only need to update its state if it is in the middle of its recoil
			if(mShot){
				double xQuad = Math.cos(mGunAngle);

				int sign = (xQuad >= 0) ? 1 : -1;

				// If we still need to update the angle
				if(mAngle && sign*ZombleHeroArmSprite.this.getRotation() >= 0){

					this.setAngularVelocity(0);
					this.setAngularAcceleration(0);

					ZombleHeroArmSprite.this.setRotation(0);
					mAngle = false;
				}

				// If we still need to update the position
				if(mVelocity && sign*(ZombleHeroArmSprite.this.getX()-mX) >= 0){

					this.setVelocity(0, 0);
					this.setAcceleration(0, 0);
					ZombleHeroArmSprite.this.setX(mX);
					ZombleHeroArmSprite.this.setY(mY);

					mVelocity = false;

				}

				// Once the angle and posistion have been reset we can end the recoil cycle
				if(!mAngle && !mVelocity){
					mShot = false;
				}
			}
		}
	}

}
