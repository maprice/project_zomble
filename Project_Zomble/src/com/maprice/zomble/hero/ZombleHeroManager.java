package com.maprice.zomble.hero;

import java.util.LinkedList;
import java.util.List;

import org.andengine.entity.Entity;
import org.andengine.opengl.texture.region.TiledTextureRegion;

import android.util.Log;

import com.maprice.zomble.enemy.ZombleEnemy;
import com.maprice.zomble.projectile.ZombleProjectileManager;
import com.maprice.zomble.util.contants.ZombleConstants;



/**
 * <Class Description>
 * 
 * @author maprice
 */


public class ZombleHeroManager implements ZombleConstants{
	// ===========================================================
	// Constants
	// ===========================================================

	static final String LOG_TAG = PREFIX + ZombleHeroManager.class.getSimpleName();

	// ===========================================================
	// Fields
	// ===========================================================

	private List<ZombleHero> mZombleCharacterList;
	private List<ZombleHero> mZombleSelectedCharacterList;

	private Entity mChaseCameraEntity;

	
	private final ZombleProjectileManager mProjectileManager;
	
	// ===========================================================
	// Constructors
	// ===========================================================

	public ZombleHeroManager(float centerX, float centerY,
			TiledTextureRegion mCharacterTextureRegion,
			TiledTextureRegion mCharacter2TextureRegion,
			TiledTextureRegion mItemTextureRegion,
			TiledTextureRegion mWaypointTextureRegion,
			TiledTextureRegion mSelectorTextureRegion,
			TiledTextureRegion mCharacterSelectorTextureRegion, Entity pSpriteLayer, ZombleProjectileManager pProjectileManager) {
		mProjectileManager = pProjectileManager;
		mZombleCharacterList = new LinkedList<ZombleHero>();
		mZombleSelectedCharacterList = new LinkedList<ZombleHero>();

		ZombleHero zombleCharacter = new ZombleHero(0, centerX, 
				centerY, 
				mCharacterTextureRegion, 
				mItemTextureRegion,
				mWaypointTextureRegion, 
				mSelectorTextureRegion,
				mCharacterSelectorTextureRegion);

		ZombleHero zombleCharacter2 = new ZombleHero(1, centerX+20, 
				centerY+20, 
				mCharacter2TextureRegion, 
				mItemTextureRegion,
				mWaypointTextureRegion, 
				mSelectorTextureRegion,
				mCharacterSelectorTextureRegion);


		mZombleCharacterList.add(zombleCharacter2);
		mZombleCharacterList.add(zombleCharacter);

		float sumX = 0;
		float sumY = 0;
		for(ZombleHero character : mZombleCharacterList){
			sumX += character.getEntity().getX();
			sumY += character.getEntity().getY();
		}

		mChaseCameraEntity = new Entity(sumX/mZombleCharacterList.size(), sumY/mZombleCharacterList.size());

	}




	// ===========================================================
	// Methods
	// ===========================================================

	public List<ZombleHero> getCharacters() {
		return mZombleCharacterList;
	}

	public Entity getCameraChaseEntity(){
		return mChaseCameraEntity;
	}

	public void onUpdate(ZombleProjectileManager mProjectileManager) {
		for (ZombleHero character: mZombleCharacterList){
			character.onUpdate(mProjectileManager);
		}

		this.updateChaseEntity();
	}

	public void updateChaseEntity(){
		float sumX = 0;
		float sumY = 0;
		for(ZombleHero character : mZombleCharacterList){
			sumX += character.getEntity().getX();
			sumY += character.getEntity().getY();
		}

		mChaseCameraEntity.setPosition(sumX/mZombleCharacterList.size(), sumY/mZombleCharacterList.size());
	}


	public void setSelectedCharacter(ZombleHero pZombleCharacter) {

		for(ZombleHero zombleCharacter : mZombleSelectedCharacterList){
			zombleCharacter.setSelected(false);
		}

		mZombleSelectedCharacterList.clear();
		mZombleSelectedCharacterList.add(pZombleCharacter);

		pZombleCharacter.setSelected(true);
	}

	public ZombleHero getSelectedCharacter() {

		if(mZombleSelectedCharacterList.size() > 1){
			return null;
		}
		else{
			return mZombleSelectedCharacterList.get(0);
		}
	}

	public void selectAll() {
		mZombleSelectedCharacterList.clear();
		for(ZombleHero lZombleCharacter : mZombleCharacterList){
			mZombleSelectedCharacterList.add(lZombleCharacter);
			lZombleCharacter.setSelected(true);
		}
	}

	//TODO: Make this extendable to N characters in a "flowering" pattern
	public void setWaypoint(float pX, float pY) {
		for(int i = 0; i < mZombleSelectedCharacterList.size(); i++){
			mZombleSelectedCharacterList.get(i).setWaypoint(pX + TOUCH_SPACING * i, pY);
		}	
	}

	public void removeWaypoint() {
		for(ZombleHero lZombleCharacter : mZombleSelectedCharacterList){
			lZombleCharacter.clearWaypoint();
		}
	}

	public void setTarget(ZombleEnemy pZomble) {
		for(ZombleHero lZombleCharacter : mZombleSelectedCharacterList){
			lZombleCharacter.setTarget(pZomble);
		}
	}
	
	

	public void clearAllTargets() {
		for(ZombleHero lZombleCharacter : mZombleCharacterList){
			lZombleCharacter.clearTarget();
		}
	}


	public void onSwitchMap(float pNewX, float pNewY, float pX, float pY) {

		this.selectAll();
		this.removeWaypoint();
		for(ZombleHero lZombleHero : this.getCharacters()){
			lZombleHero.getEntity().setPosition(pNewX, pNewY);
			lZombleHero.clearTarget();
		}

		this.setWaypoint(pX, pY);
		this.updateChaseEntity();
	}




	public void useAbility() {
		for(ZombleHero lZombleCharacter : mZombleSelectedCharacterList){
			Log.e("sdf", "POW");
			lZombleCharacter.useAbility(mProjectileManager);
		}
	}


	public void cycleRight() {
		if(mZombleCharacterList.size() != mZombleSelectedCharacterList.size()){
			ZombleHero lOldHero = mZombleSelectedCharacterList.get(0);
			lOldHero.setSelected(false);

			int lNewHeroIndex = (mZombleCharacterList.indexOf(lOldHero) + 1) % mZombleCharacterList.size();
			ZombleHero lNewHero = mZombleCharacterList.get(lNewHeroIndex);
			lNewHero.setSelected(true);
			mZombleSelectedCharacterList.clear();
			mZombleSelectedCharacterList.add(lNewHero);
		}
		else{
			this.setSelectedCharacter(mZombleCharacterList.get(mZombleCharacterList.size()-1));
		}
	}

	public void cycleLeft() {
		if(mZombleCharacterList.size() != mZombleSelectedCharacterList.size()){
			ZombleHero lOldHero = mZombleSelectedCharacterList.get(0);
			lOldHero.setSelected(false);

			int lNewHeroIndex = (((mZombleCharacterList.indexOf(lOldHero) - 1) % mZombleCharacterList.size()) + mZombleCharacterList.size()) % mZombleCharacterList.size();

			ZombleHero lNewHero = mZombleCharacterList.get(lNewHeroIndex);
			lNewHero.setSelected(true);
			mZombleSelectedCharacterList.clear();
			mZombleSelectedCharacterList.add(lNewHero);
		}
		else{
			this.setSelectedCharacter(mZombleCharacterList.get(0));
		}
	}




	
}