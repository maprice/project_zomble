package com.maprice.zomble;

import org.andengine.audio.music.Music;
import org.andengine.audio.sound.Sound;

import com.maprice.zomble.util.contants.ZombleConstants;



/**
 * Singleton class used to control sound
 * 
 * @author maprice
 */

// TODO: simply POC implementation, will need to be overhauled
public class ZombleSoundManager implements ZombleConstants{
	// ===========================================================
	// Constants
	// ===========================================================

	static final String LOG_TAG = PREFIX + ZombleSoundManager.class.getSimpleName();

	// ===========================================================
	// Fields
	// ===========================================================

	//Need to make scene static

	private static final ZombleSoundManager sZombleHUDHealth = new ZombleSoundManager();

	public Sound mCasing;
	public Sound mGunShot;

	// ===========================================================
	// Constructors
	// ===========================================================

	public static ZombleSoundManager sharedInstance() {
		return sZombleHUDHealth;
	}


	// ===========================================================
	// Methods
	// ===========================================================


	public void loadSounds(Sound a, Sound b, Music mMusic){

		mMusic.play();
		mCasing = a;
		mGunShot = b;
	}



	public void playSound(int i){

		if(i==1){
			mCasing.play();
		}
		else{
			mGunShot.play();
		}
	}



}
