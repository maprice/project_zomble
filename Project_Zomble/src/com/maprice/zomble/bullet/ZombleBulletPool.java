package com.maprice.zomble.bullet;

import org.andengine.util.adt.pool.GenericPool;

import com.maprice.zomble.hero.ZombleHeroStats.GunType;
import com.maprice.zomble.scene.ZombleSceneManager;
import com.maprice.zomble.util.contants.ZombleCallback;
import com.maprice.zomble.util.contants.ZombleConstants;

/**
 * Pool system that controls the allocation and deletion of bullets
 * 
 * @author maprice
 */


public class ZombleBulletPool extends GenericPool<ZombleBullet> implements ZombleConstants{
	// ===========================================================
	// Constants
	// ===========================================================

	static final String LOG_TAG = PREFIX + ZombleBulletPool.class.getSimpleName();
	
	// ===========================================================
	// Fields
	// ===========================================================

	private final GunType mGunType;
	private final ZombleCallback<ZombleBullet> mZombleCallback;

	// ===========================================================
	// Constructors
	// ===========================================================

	public ZombleBulletPool(GunType pGunType, ZombleCallback<ZombleBullet> pZombleCallback) {
		mGunType = pGunType;
		mZombleCallback = pZombleCallback;
	}

	// ===========================================================
	// Methods
	// ===========================================================


	@Override
	protected ZombleBullet onAllocatePoolItem() {
		final ZombleBullet bullet;

		switch(mGunType){
		case RIFLE:
			bullet = new ZombleBullet(MACHINEGUN_BULLET_WIDTH, 
					MACHINEGUN_BULLET_HEIGHT,
					MACHINEGUN_BULLET_COLOR, 
					mZombleCallback);
			break;
		default:
			bullet = null;
			throw new RuntimeException("Invalid gun type");
		}

		return bullet;
	}

	
	@Override
	protected void onHandleRecycleItem(final ZombleBullet pZombleBullet) {
		ZombleSceneManager.sharedInstance().getEngine().runOnUpdateThread(new Runnable(){
			@Override
			public void run() {
				pZombleBullet.setIgnoreUpdate(true);
				pZombleBullet.clearUpdateHandlers();
				pZombleBullet.clearEntityModifiers();
				pZombleBullet.setVisible(false);
			}
		});
	}


	@Override
	protected void onHandleObtainItem(final ZombleBullet pZombleBullet) {
		pZombleBullet.setIgnoreUpdate(false);
		pZombleBullet.resetInteractions();
	}
}

