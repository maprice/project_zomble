package com.maprice.zomble.bullet;

import org.andengine.engine.handler.physics.PhysicsHandler;
import org.andengine.entity.IEntity;
import org.andengine.entity.primitive.Rectangle;
import org.andengine.util.color.Color;
import org.andengine.util.math.MathUtils;

import com.maprice.zomble.ZombleSoundManager;
import com.maprice.zomble.hero.ZombleHero;
import com.maprice.zomble.scene.ZombleSceneManager;
import com.maprice.zomble.util.contants.ZombleCallback;
import com.maprice.zomble.util.contants.ZombleConstants;

/**
 * Single bullet entity
 * 
 * @author mprice
 */

public class ZombleBullet extends Rectangle implements ZombleConstants {

	// ===========================================================
	// Constants
	// ===========================================================

	static final String LOG_TAG = PREFIX + ZombleBullet.class.getSimpleName();

	// ===========================================================
	// Fields
	// ===========================================================

	private ZombleCallback<ZombleBullet> mZombleCallback;
	private double mBulletDirection;
	private ZombleHero mSource;
	
	final private PhysicsHandler mPhysicsHandler = new PhysicsHandler(this){
		@Override
		protected void onUpdate(final float pSecondsElapsed, final IEntity pEntity) {
			super.onUpdate(pSecondsElapsed,pEntity);

			if(pEntity.getX() > ZombleSceneManager.sharedInstance().getCamera().getXMax() || 
					pEntity.getX() < ZombleSceneManager.sharedInstance().getCamera().getXMin() ||
					pEntity.getY() < ZombleSceneManager.sharedInstance().getCamera().getYMin() ||
					pEntity.getY() > ZombleSceneManager.sharedInstance().getCamera().getYMax()){
				
				mZombleCallback.onComplete(ZombleBullet.this);
			}
		}
	};

	// ===========================================================
	// Constructors
	// ===========================================================


	public ZombleBullet(float pWidth, float pHeight, Color pColor, ZombleCallback<ZombleBullet> pZombleCallback) {
		super(0, 0, pWidth, pHeight, ZombleSceneManager.sharedInstance().getVBOmanager());

		mZombleCallback = pZombleCallback;

		// Set the color
		this.setColor(pColor);
		
		this.setZIndex(Layer.FRONT_EFFECTS.ordinal());
	}

	// ===========================================================
	// Methods
	// ===========================================================

	/**
	 * Starts the motion of the bullet in a ceratin direction at a certain speed
	 * 
	 * @param pAngle
	 *            angle the bullet will travel at (Radians [0 , 2PI])
	 *            
	 * @param pVelocity
	 *            velocity the bullet will travel
	 */
	public void start(float pX, float pY, double pAngle, float pVelocity){

		this.setPosition(pX, pY);

		mBulletDirection = pAngle;

		//Rotates the bullet in the direction its being fired
		this.setRotation(MathUtils.radToDeg((float)-pAngle));

		// Sets the velocity in the correct direction
		double x = Math.cos(pAngle);
		double y = Math.sin(-pAngle);

		mPhysicsHandler.setVelocity((float)x*pVelocity,(float)y*pVelocity);

		ZombleSoundManager.sharedInstance().playSound(2);
	}

	/**
	 * Resets the entity to its starting state
	 */
	public void resetInteractions(){
		this.setVisible(true);
		this.registerUpdateHandler(mPhysicsHandler);
	}

	/**
	 * Sets the character who shot the bullet
	 */
	public void setSource(ZombleHero pSource) {
		mSource = pSource;
	}

	/**
	 * Gets the character who shot the bullet
	 */
	public ZombleHero getSource() {
		return mSource;
	}

	/**
	 * Gets the current direction of the bullet
	 */
	public double getBulletDirection(){
		return mBulletDirection;
	}

}