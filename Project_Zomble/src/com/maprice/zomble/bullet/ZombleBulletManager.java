package com.maprice.zomble.bullet;

import java.util.LinkedList;
import java.util.List;
import java.util.Random;

import com.maprice.zomble.hero.ZombleHero;
import com.maprice.zomble.hero.ZombleHeroStats.GunType;
import com.maprice.zomble.scene.ZombleSceneManager;
import com.maprice.zomble.util.contants.ZombleCallback;
import com.maprice.zomble.util.contants.ZombleConstants;


/**
 * Manages the all activity with respect to bullets
 * 
 * @author maprice
 */


public class ZombleBulletManager implements ZombleConstants{
	// ===========================================================
	// Constants
	// ===========================================================

	static final String LOG_TAG = PREFIX + ZombleBulletManager.class.getSimpleName();
	
	// ===========================================================
	// Fields
	// ===========================================================

	final private ZombleBulletPool mBulletPool;
	final private List<ZombleBullet> mActivebulletList;
	final private ZombleCallback<ZombleBullet> mZombleCallback = new ZombleCallback<ZombleBullet>(){

		@Override
		public void onComplete(ZombleBullet pResult) {
			mActivebulletList.remove(pResult);
			mBulletPool.recyclePoolItem(pResult);
		}

		@Override
		public void onError(String pErrorMessage) {
			throw new RuntimeException(pErrorMessage);
		}
	};

	// ===========================================================
	// Constructors
	// ===========================================================

	public ZombleBulletManager(){
		mActivebulletList = new LinkedList<ZombleBullet>(); 
		mBulletPool = new ZombleBulletPool(GunType.RIFLE, mZombleCallback);
	}

	// ===========================================================
	// Methods
	// ===========================================================

	/**
	 * Creates a rectangle starting a specified position and moves it in a 
	 * specified direction according to the type of gun currently in use
	 * 
	 * @param pX
	 *            starting x position
	 *            
	 * @param pY
	 *            starting y position
	 *            
	 * @param pAngle
	 *            angle to shoot the bullet
	 *            
	 * @param pGunType
	 *            which type of gun the bullet is being shot from
	 * @param pSourceEntity 
	 */
	public void shootProjectile(float pX, float pY, double pAngle, GunType pGunType, ZombleHero pSource){

		// Find the x and y offset of the center of rotation for the gun
		float offsetX =  pX+(float)(ITEM_X_OFFSET_MULTIPLIER * 8 * Math.cos(-pAngle));
		float offsetY =  pY+(float)(ITEM_Y_OFFSET_MULTIPLIER * 8 * Math.sin(-pAngle));

		// Shoot the correct bullet depending on which gun is selected
		switch(pGunType){
		case RIFLE:
			shootRifle(offsetX, offsetY, MACHINEGUN_BASE_SPREAD, pAngle, pSource);
			break;

		case SHOTGUN:
			shootShotgun(offsetX, offsetY, SHOTGUN_BASE_SPREAD, pAngle, pSource);
			break;

		default:
		}
	}


	/**
	 * Creates a single rectangle starting a specified position and moves it in a 
	 * specified direction
	 * 
	 * @param pX
	 *            starting x position
	 *            
	 * @param pY
	 *            starting y position
	 *            
	 * @param pSpread
	 *            the random offset min and max the bullet can be fired at (0 == fully accurate)
	 *            
	 * @param pAngle
	 *            angle to shoot the bullet
	 * @param pSourceEntity 
	 */

	private void shootRifle(float pX, float pY, float pSpread, double pAngle, ZombleHero pSource) {
		// Create new bullet
		ZombleBullet bullet = mBulletPool.obtainPoolItem();

		bullet.setSource(pSource);
	
		// Calculate the spread
		Random rand = new Random();
		float spreadOffset = rand.nextFloat() * pSpread;

		// Attach the bullet to the scene
		if(!bullet.hasParent()){
			ZombleSceneManager.sharedInstance().getCurrentScene().attachChild(bullet);
		}
		synchronized(mActivebulletList){
			mActivebulletList.add(bullet);
		}
		// Shoot the bullet
		bullet.start(pX, pY, pAngle+spreadOffset, MACHINEGUN_BASE_VELOCITY);
	}


	/**
	 * Creates a group of rectangles starting a specified position and moves them in a 
	 * specified direction
	 * 
	 * @param pX
	 *            starting x position
	 *            
	 * @param pY
	 *            starting y position
	 *            
	 * @param pSpread
	 *            the random offset min and max the bullet can be fired at (0 == fully accurate)
	 *            
	 * @param pAngle
	 *            angle to shoot the bullet
	 * @param pSourceEntity 
	 */
	// Currently broken
	public void shootShotgun(float pX, float pY, float spread, double pAngle, ZombleHero pSourceEntity){
		// Create new bullets
		final ZombleBullet[] bullets = new ZombleBullet[10];

		for(int i = 0; i < bullets.length; i++){
			// Init new bullets
			bullets[i] = mBulletPool.obtainPoolItem();//(x, y, 3f,3f, new Color(1f,1f,1f,1), mVertexBufferObjectManager);
			// Calculate the spread
			Random rand = new Random();

			float spreadOffset = rand.nextFloat() * spread;
			float velocity = rand.nextFloat() * SHOTGUN_BASE_SPREAD_VELOCITY;

			// Attach the bullet to the scene
			if(!bullets[i].hasParent()){
				ZombleSceneManager.sharedInstance().getCurrentScene().attachChild(bullets[i]);
			}


			// Shoot the bullet
			bullets[i].start(pX, pY, pAngle+spreadOffset, SHOTGUN_BASE_VELOCITY+velocity);
		}
	}

	/**
	 * Creates a group of rectangles starting a specified position and moves them in a 
	 * specified direction
	 * 
	 * @return
	 *           list of all ACTIVE bullets (those that are still in motion)
	 */
	public List<ZombleBullet> getActiveBullets(){
		return mActivebulletList;
	}

	/**
	 * Creates a group of rectangles starting a specified position and moves them in a 
	 * specified direction
	 * 
	 * @param pBullet
	 *           the bullet that has finished
	 */
	public void bulletDidFinish(ZombleBullet pBullet) {
		mActivebulletList.remove(pBullet);
		mBulletPool.recyclePoolItem(pBullet);
	}

}