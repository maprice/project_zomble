package com.maprice.zomble.entity;

import org.andengine.entity.sprite.TiledSprite;

import org.andengine.opengl.texture.region.ITiledTextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;

import com.maprice.zomble.hero.ZombleHeroEntity.CharacterLayer;
import com.maprice.zomble.util.contants.ZombleConstants;


/**
 * Base class for controlling the [arm, gun] of a humanoid sprite
 * 
 * @author mprice
 */


public abstract class ZombleBaseCharacterArmSprite extends TiledSprite implements ZombleConstants{
	// ===========================================================
	// Constants
	// ===========================================================

	static final String LOG_TAG = PREFIX + ZombleBaseCharacterArmSprite.class.getSimpleName();

	// ===========================================================
	// Fields
	// ===========================================================


	final protected float mOriginX;
	final protected float mOriginY;
	private int mFrameCol = 0; 
	protected double mGunAngle = 0; 

	protected int mITEM_X_OFFSET_MULTIPLIER = ITEM_X_OFFSET_MULTIPLIER;
	protected int mITEM_Y_OFFSET_MULTIPLIER = ITEM_Y_OFFSET_MULTIPLIER;

	protected int mITEM_FRAME_BEHIND = ITEM_FRAME_BEHIND;
	protected int mITEM_FRAME_END = ITEM_FRAME_END;

	// ===========================================================
	// Constructors
	// ===========================================================

	public ZombleBaseCharacterArmSprite(float pX, float pY,
			ITiledTextureRegion pTiledTextureRegion,
			VertexBufferObjectManager pVertexBufferObjectManager) {

		super(pX, pY, pTiledTextureRegion, pVertexBufferObjectManager);

		mOriginX = pX;
		mOriginY = pY;

		// Set the Z index to be the in front of the character by default
		this.setZIndex(CharacterLayer.FRONT.index());
	}

	// ===========================================================
	// Methods
	// ===========================================================

	/**
	 * Angles the character in the appropriate direction
	 * 
	 * @param pAngle
	 *            angle the to look in (Radians [0 , 2PI])
	 */
	public void look(double pAngle){

		// Find the x and y offset of the center of rotation for the gun
		double offsetX =  (mITEM_X_OFFSET_MULTIPLIER * Math.cos(-pAngle));
		double offsetY =  (mITEM_Y_OFFSET_MULTIPLIER * Math.sin(-pAngle));

		// Set the x and y offset
		this.setX(mOriginX+(float)offsetX );
		this.setY(mOriginY+(float)offsetY);

		mGunAngle = pAngle;

		// Check to see if gun angle needs updating
		if(mFrameCol != (int)((pAngle*16)/(Math.PI*2))){

			// Update the gun angle
			mFrameCol = (int) ((pAngle*16)/(Math.PI*2));

			// Set the frame of the gun to the new angle
			this.setCurrentTileIndex((int) mFrameCol);

			// Gun should be draw BEHIND of the character
			if(mFrameCol >= mITEM_FRAME_BEHIND && mFrameCol <= mITEM_FRAME_END){
				this.setZIndex(CharacterLayer.BEHIND.index());
				this.getParent().sortChildren();

			}
			// Gun should be draw INFRONT of the character
			else{
				this.setZIndex(CharacterLayer.FRONT.index());
				this.getParent().sortChildren();
			}
		}
	}
}