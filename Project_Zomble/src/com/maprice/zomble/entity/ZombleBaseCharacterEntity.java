package com.maprice.zomble.entity;

import org.andengine.entity.Entity;
import org.andengine.entity.primitive.Rectangle;
import org.andengine.util.color.Color;

import com.maprice.zomble.util.contants.ZombleConstants;


/**
 *  Base class for controlling the body and legs of a humanoid sprite
 * 
 * @author mprice
 */

public abstract class ZombleBaseCharacterEntity extends Entity implements ZombleConstants {
	// ===========================================================
	// Constants
	// ===========================================================

	static final String LOG_TAG = PREFIX + ZombleBaseCharacterEntity.class.getSimpleName();

	public enum CharacterLayer {
		BEHIND (2),
		CHARACTER (3),
		FRONT (4);

		final int value;   

		CharacterLayer(int value) {
			this.value = value;
		}

		public int index() { 
			return value; 
		}
	}

	// ===========================================================
	// Fields
	// ===========================================================

	protected double mCurrentAngle;

	private Rectangle mTouchHitbox;
	private Rectangle mFeetHitbox;
	private Rectangle mSpriteHitbox;
	// ===========================================================
	// Constructors
	// ===========================================================

	public ZombleBaseCharacterEntity(float pX, float pY)
	{
		super(pX,pY);
		this.setZIndex(Layer.SPRITES.ordinal());
	}

	// ===========================================================
	// Methods
	// ===========================================================


	/**
	 * Moves the character in the appropriate direction and animates the sprite accordingly
	 * 
	 * @param pValueX
	 *            x value of the analog control
	 *            
	 * @param pValueY
	 *            y value of the analog control
	 */
	public abstract void walk(float pValueX, float pValueY);



	/**
	 * Angles the character in the appropriate direction
	 * 
	 * @param pValueX
	 *            x value of the analog control
	 *            
	 * @param pValueY
	 *            y value of the analog control
	 */
	public abstract void look(float pValueX, float pValueY);


	/**
	 * Stops all animation and movement of the character sprite 
	 */
	public abstract void stop();

	public double getCurrentAngle() {
		return mCurrentAngle;
	}
	
	public final void setTouchHitbox(Rectangle pRect) {
		mTouchHitbox = pRect;
		if(DEBUG){
			mTouchHitbox.setColor(Color.WHITE);
			mTouchHitbox.setAlpha(0.2f);
			mTouchHitbox.setVisible(true);
		}
		else{
			mTouchHitbox.setVisible(false);	
		}
		this.attachChild(mTouchHitbox);
	}
	public final void setFeetHitbox(Rectangle pRect) {
		mFeetHitbox = pRect;
		if(DEBUG){
			mFeetHitbox.setColor(Color.BLUE);
			mFeetHitbox.setAlpha(0.5f);
			mFeetHitbox.setVisible(true);
		}
		else{
			mFeetHitbox.setVisible(false);	
		}
		this.attachChild(mFeetHitbox);
	}
	public final void setSpriteHitbox(Rectangle pRect) {
		mSpriteHitbox = pRect;
		if(DEBUG){
			mSpriteHitbox.setColor(Color.RED);
			mSpriteHitbox.setAlpha(0.5f);
			mSpriteHitbox.setVisible(true);
		}
		else{
			mSpriteHitbox.setVisible(false);	
		}
		this.attachChild(mSpriteHitbox);
	}
	
	public Rectangle getTouchHitbox(){
		return mTouchHitbox;
	}
	public Rectangle getFeetHitbox() {
		return mFeetHitbox;
	}
	public Rectangle getSpriteHitbox() {
		return mSpriteHitbox;
	}
	
	public float[] getCoordinates(){
		 return this.getFeetHitbox().convertLocalToSceneCoordinates(
				 this.getFeetHitbox().getWidth()/2,
				 this.getFeetHitbox().getHeight()/2);		
	}

}