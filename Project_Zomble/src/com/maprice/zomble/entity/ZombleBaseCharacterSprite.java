package com.maprice.zomble.entity;

import org.andengine.entity.sprite.AnimatedSprite;
import org.andengine.opengl.texture.region.ITiledTextureRegion;

import com.maprice.zomble.hero.ZombleHeroEntity.CharacterLayer;
import com.maprice.zomble.scene.ZombleSceneManager;
import com.maprice.zomble.util.contants.ZombleConstants;

/**
 * @author mprice
 */

public class ZombleBaseCharacterSprite extends AnimatedSprite implements ZombleConstants {

	// ===========================================================
	// Constants
	// ===========================================================

	static final String LOG_TAG = PREFIX + ZombleBaseCharacterSprite.class.getSimpleName();

	// ===========================================================
	// Fields
	// ===========================================================


	final private long[] mDuration = new long[HERO_SPRITE_SHEET_COL-1];
	private int mCharacterAngle = 0;
	private boolean mIsBackwards = false;

	// ===========================================================
	// Constructors
	// ===========================================================


	public ZombleBaseCharacterSprite(float pX, float pY,
			ITiledTextureRegion pTiledTextureRegion,
			int pFrameSpeed) {

		super(pX, pY, pTiledTextureRegion, ZombleSceneManager.sharedInstance().getVBOmanager());

		// Set the Z index to be the Character(middle) 
		this.setZIndex(CharacterLayer.CHARACTER.index());

		// Setup the animation duration array
		for(int i = 0; i < mDuration.length; i++)  mDuration[i] = pFrameSpeed;
	}

	// ===========================================================
	// Methods
	// ===========================================================


	/**
	 * Moves the character in the appropriate direction and animates the sprite accordingly
	 * 
	 * @param pAngle
	 *            angle in radians
	 *           
	 */
	public void walk(double pAngle){

		// Convert angle from degrees to 8-way directional coordinates
		int walkAngle = (int) ((pAngle*HERO_SPRITE_SHEET_ROW)/(Math.PI*2));

		// True if the current angle the sprite is moving is within 90 degrees of the current angle the sprite is facing
		if(HERO_SPRITE_SHEET_ROW - Math.abs(walkAngle-mCharacterAngle)%HERO_SPRITE_SHEET_ROW <= 2 
				|| Math.abs(walkAngle-mCharacterAngle)%HERO_SPRITE_SHEET_ROW <= 2){

			// Walking forwards
			if(mIsBackwards || !this.isAnimationRunning()){
				mIsBackwards = false;
				final int frame = mCharacterAngle*(HERO_SPRITE_SHEET_ROW-1);
				final int[] mframe = {frame+1,frame+2,frame+3,frame+4,frame+5,frame+6};
				this.animate(mDuration, mframe);
			}
		}
		else{

			// Walking backwards
			if(!mIsBackwards || !this.isAnimationRunning()){
				mIsBackwards = true;
				final int frame = mCharacterAngle*(HERO_SPRITE_SHEET_ROW-1);
				final int[] mframe = {frame+6,frame+5,frame+4,frame+3,frame+2,frame+1};
				this.animate(mDuration, mframe);
			}
		}
	}

	public int getCurrentAngle(){
		return mCharacterAngle;
	}

	/**
	 * Angles the character in the appropriate direction
	 * 
	 * @param pAngle
	 *            angle in radians
	 *            
	 */
	public void look(double pAngle){

		// Check to see if character angle needs updating
		if(mCharacterAngle != (int)((pAngle*HERO_SPRITE_SHEET_ROW)/(Math.PI*2))){

			// Update the gun angle
			mCharacterAngle =  (int) ((pAngle*HERO_SPRITE_SHEET_ROW)/(Math.PI*2));

			// Get the row of the new frame
			int frame = mCharacterAngle*(HERO_SPRITE_SHEET_ROW-1);

			// Set the new frame on the (n+1) column of the row
			this.stopAnimation(frame+((this.getCurrentTileIndex()+1)%HERO_SPRITE_SHEET_COL));

		}
	}

	/**
	 * Stops all animation of the character sprite 
	 */
	public void stop() {
		// Stop animation of the character
		this.stopAnimation(mCharacterAngle*(HERO_SPRITE_SHEET_ROW-1));
	}
}
