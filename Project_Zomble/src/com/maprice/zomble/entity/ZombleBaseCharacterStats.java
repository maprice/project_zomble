package com.maprice.zomble.entity;

import java.util.Random;

import com.maprice.zomble.util.contants.ZombleConstants;


/**
 * All statistical information pertaining to the main character entity
 * 
 * @author mprice
 */

public abstract class ZombleBaseCharacterStats implements ZombleConstants {


	// ===========================================================
	// Constants
	// ===========================================================

	static final String LOG_TAG = PREFIX + ZombleBaseCharacterStats.class.getSimpleName();

	// ===========================================================
	// Fields
	// ===========================================================

	final protected float mSpeed;
	final protected int mTotalHealth;
	final protected int mDamage;

	protected int mCurrentHealth;

	// ===========================================================
	// Constructors
	// ===========================================================

	public ZombleBaseCharacterStats(float pSpeed, int pTotalHealth, int pDamage){
		mSpeed = pSpeed;
		mTotalHealth = pTotalHealth;
		mDamage = pDamage;
		mCurrentHealth = pTotalHealth;
	}

	// ===========================================================
	// Methods
	// ===========================================================


	/**
	 * Returns speed
	 */
	public float getSpeed(){
		return mSpeed;
	}

	/**
	 * Returns TotalHealth
	 */
	public int getTotalHealth(){
		return mTotalHealth;
	}

	/**
	 * Returns damage
	 */
	public int getDamage(){
		//Ghetto for now
		Random rand = new Random();
		return rand.nextInt(3)+1;
	}

	/**
	 * Returns current health
	 */
	public int getCurrentHealth(){
		return mCurrentHealth;
	}

	/**
	 * Subtracts pAmount from current health, returns true is currentHealth less than or equal to zero
	 * 
	 * @param pAmount
	 *            amount of health to subtract
	 *            
	 */
	public boolean subtractHealth(int pAmount){
		mCurrentHealth -= pAmount;

		if(mCurrentHealth <= 0){
			mCurrentHealth = 0;
			return true;
		}

		return false;
	}


	public void resetInteractions() {
		mCurrentHealth = mTotalHealth;
	}

}
