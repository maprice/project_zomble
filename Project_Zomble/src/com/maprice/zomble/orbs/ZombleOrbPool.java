package com.maprice.zomble.orbs;

import org.andengine.opengl.texture.region.TiledTextureRegion;
import org.andengine.util.adt.pool.GenericPool;

import com.maprice.zomble.scene.ZombleSceneManager;
import com.maprice.zomble.util.contants.ZombleCallback;
import com.maprice.zomble.util.contants.ZombleConstants;

/**
 * Pool system that controls the allocation and deletion of orbs
 * 
 * @author maprice
 */


public class ZombleOrbPool extends GenericPool<ZombleOrbs> implements ZombleConstants{

	// ===========================================================
	// Fields
	// ===========================================================

	private final TiledTextureRegion mOrbTextureRegion;
	private final ZombleCallback<ZombleOrbs> mZombleCallback;

	// ===========================================================
	// Constructors
	// ===========================================================

	public ZombleOrbPool(TiledTextureRegion pOrbTextureRegion, ZombleCallback<ZombleOrbs> pZombleCallback) {
		mOrbTextureRegion = pOrbTextureRegion;
		mZombleCallback = pZombleCallback;
	}

	// ===========================================================
	// Methods
	// ===========================================================

	@Override
	protected ZombleOrbs onAllocatePoolItem() {
		return new ZombleOrbs(mOrbTextureRegion, mZombleCallback);
	}

	@Override
	protected void onHandleRecycleItem(final ZombleOrbs pOrb) {
		ZombleSceneManager.sharedInstance().getEngine().runOnUpdateThread(new Runnable(){
			@Override
			public void run() {
				pOrb.setIgnoreUpdate(true);
				pOrb.clearUpdateHandlers();
				pOrb.clearEntityModifiers();
				pOrb.setVisible(false);
			}
		});
	}

	@Override
	protected void onHandleObtainItem(final ZombleOrbs pOrb) {
		pOrb.setIgnoreUpdate(false);
		pOrb.resetInteractions();
	}
}