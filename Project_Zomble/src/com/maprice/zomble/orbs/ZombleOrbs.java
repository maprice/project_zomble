package com.maprice.zomble.orbs;

import java.util.Random;

import org.andengine.engine.handler.physics.PhysicsHandler;
import org.andengine.entity.Entity;
import org.andengine.entity.IEntity;
import org.andengine.entity.sprite.AnimatedSprite;
import org.andengine.opengl.texture.region.TiledTextureRegion;

import com.maprice.zomble.hero.ZombleHero;
import com.maprice.zomble.hero.ZombleHeroEntity;
import com.maprice.zomble.scene.ZombleSceneManager;
import com.maprice.zomble.util.contants.ZombleCallback;
import com.maprice.zomble.util.contants.ZombleConstants;


/**
 * Single number (discharge from gun)
 * 
 * @author maprice
 */

public class ZombleOrbs extends AnimatedSprite implements ZombleConstants{
	// ===========================================================
	// Constants
	// ===========================================================

	static final String LOG_TAG = PREFIX + ZombleOrbs.class.getSimpleName();

	// ===========================================================
	// Fields
	// ===========================================================

	private ZombleHero mChaseEntity;
	private boolean mActive;
	
	final ZombleCallback<ZombleOrbs> mZombleCallback;
	//TODO: needs work
	final private PhysicsHandler mPhysicsHandler = new PhysicsHandler(this){
		@Override
		protected void onUpdate(final float pSecondsElapsed, final IEntity pEntity) {
			super.onUpdate(pSecondsElapsed,pEntity);

			if(mActive){
				float deltaX =  mChaseEntity.getEntity().getX() - ZombleOrbs.this.getX();
				float deltaY =  mChaseEntity.getEntity().getY() - ZombleOrbs.this.getY();
				
				double angleInDegrees = Math.atan2(deltaY, deltaX);

				// Sets the velocity in the correct direction
				float gox = (float) Math.cos(angleInDegrees);
				float goy = (float) Math.sin(angleInDegrees);

				mPhysicsHandler.setVelocity(gox*100, goy*100);

				if(ZombleOrbs.this.collidesWith(mChaseEntity.getEntity().getSpriteHitbox())){
					mPhysicsHandler.setAcceleration(0, 0);
					mPhysicsHandler.setVelocity(0, 0);
					mZombleCallback.onComplete(ZombleOrbs.this);
					mActive = false;
					mChaseEntity.onOrbCollect(1);
				}
			}
		}
	};


	// ===========================================================
	// Constructors
	// ===========================================================

	public ZombleOrbs(TiledTextureRegion mOrbTextureRegion, ZombleCallback<ZombleOrbs> pZombleCallback) {
		super(0, 0, mOrbTextureRegion, ZombleSceneManager.sharedInstance().getVBOmanager());

		mZombleCallback = pZombleCallback;
		
		this.setZIndex(Layer.HUD.ordinal());
	}

	// ===========================================================
	// Methods
	// ===========================================================

	public void resetInteractions(){
		this.registerUpdateHandler(mPhysicsHandler);
	}


	public void start(Entity pFromEntity, ZombleHero pTo) {
		mChaseEntity = pTo;

		Random rand = new Random();
		this.setScale(rand.nextFloat()*(SCALE-1) + 1);

		mActive = true;

		if(!this.isVisible()){
			this.setVisible(true);	
		}

		this.animate(ORB_ANIMATION_FRAME_RATE);
		this.setPosition(pFromEntity);
	}
}