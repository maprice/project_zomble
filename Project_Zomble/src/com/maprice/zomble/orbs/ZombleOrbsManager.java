package com.maprice.zomble.orbs;

import org.andengine.entity.Entity;
import org.andengine.opengl.texture.region.TiledTextureRegion;

import com.maprice.zomble.hero.ZombleHero;
import com.maprice.zomble.scene.ZombleSceneManager;
import com.maprice.zomble.util.contants.ZombleCallback;
import com.maprice.zomble.util.contants.ZombleConstants;


/**
 * Manages the all activity with respect to damage numbers
 * 
 * @author maprice
 */


public class ZombleOrbsManager implements ZombleConstants{
	// ===========================================================
	// Constants
	// ===========================================================

	static final String LOG_TAG = PREFIX + ZombleOrbsManager.class.getSimpleName();

	// ===========================================================
	// Fields
	// ===========================================================

	final ZombleOrbPool mNumberPool;
	
	final ZombleCallback<ZombleOrbs> mZombleCallback = new ZombleCallback<ZombleOrbs>(){

		@Override
		public void onComplete(ZombleOrbs pResult) {
			mNumberPool.recyclePoolItem(pResult);
		}

		@Override
		public void onError(String pErrorMessage) {
			throw new RuntimeException(pErrorMessage);
		}
	};
	
	// ===========================================================
	// Constructors
	// ===========================================================

	public ZombleOrbsManager(TiledTextureRegion pOrbTextureRegion){
		mNumberPool = new ZombleOrbPool(pOrbTextureRegion, mZombleCallback);
	}

	// ===========================================================
	// Methods
	// ===========================================================

	public void dropOrbs(Entity pFromEntity, ZombleHero pTo) {

		// Get new casing
		
		ZombleOrbs casing = mNumberPool.obtainPoolItem();


		// Attach casing to scene
		if(!casing.hasParent()){
			ZombleSceneManager.sharedInstance().getCurrentScene().attachChild(casing);
		}

		// Discharge casing
		casing.start(pFromEntity, pTo);
	}	
}