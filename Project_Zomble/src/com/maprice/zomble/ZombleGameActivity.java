package com.maprice.zomble;


import org.andengine.engine.camera.BoundCamera;
import org.andengine.engine.handler.timer.ITimerCallback;
import org.andengine.engine.handler.timer.TimerHandler;
import org.andengine.engine.options.EngineOptions;
import org.andengine.engine.options.ScreenOrientation;
import org.andengine.engine.options.resolutionpolicy.RatioResolutionPolicy;
import org.andengine.entity.scene.Scene;
import org.andengine.entity.util.FPSLogger;
import org.andengine.ui.activity.BaseGameActivity;

import com.maprice.zomble.scene.ZombleSceneManager;
import com.maprice.zomble.scene.ZombleSceneManager.ZombleScenes;
import com.maprice.zomble.util.contants.ZombleConstants;


/**
 * Main activity
 * 
 * @author maprice
 */

public class ZombleGameActivity extends BaseGameActivity implements ZombleConstants{
	// ===========================================================
	// Constants
	// ===========================================================

	static final String LOG_TAG = PREFIX + ZombleGameActivity.class.getSimpleName();

	// ===========================================================
	// Fields
	// ===========================================================

	private BoundCamera mBoundChaseCamera;

	// ===========================================================
	// Methods
	// ===========================================================

	@Override
	public EngineOptions onCreateEngineOptions() {
		this.mBoundChaseCamera = new BoundCamera(0, 0, CAMERA_WIDTH, CAMERA_HEIGHT);
		final EngineOptions lEngineOptions = new EngineOptions(true, ScreenOrientation.LANDSCAPE_FIXED, new RatioResolutionPolicy(CAMERA_WIDTH, CAMERA_HEIGHT), this.mBoundChaseCamera);
		lEngineOptions.getTouchOptions().setNeedsMultiTouch(true);
		lEngineOptions.getAudioOptions().setNeedsSound(true);
		lEngineOptions.getAudioOptions().setNeedsMusic(true);
		lEngineOptions.getRenderOptions().setDithering(true);
		lEngineOptions.getRenderOptions().setMultiSampling(true);
		return lEngineOptions;
	}


	@Override
	public void onCreateResources(OnCreateResourcesCallback pOnCreateResourcesCallback)
	throws Exception {

		ZombleSceneManager.sharedInstance().initSceneManager(this, mEngine, mBoundChaseCamera);
		ZombleSceneManager.sharedInstance().loadResources(ZombleScenes.SPLASH);

		pOnCreateResourcesCallback.onCreateResourcesFinished();
	}

	@Override
	public void onCreateScene(OnCreateSceneCallback pOnCreateSceneCallback)
	throws Exception {

		this.mEngine.registerUpdateHandler(new FPSLogger());

		pOnCreateSceneCallback.onCreateSceneFinished(ZombleSceneManager.sharedInstance().createScene(ZombleScenes.SPLASH));
	}

	@Override
	public void onPopulateScene(Scene pScene,
			OnPopulateSceneCallback pOnPopulateSceneCallback) throws Exception {
		mEngine.registerUpdateHandler(new TimerHandler(2f, new ITimerCallback()
		{
			public void onTimePassed(final TimerHandler pTimerHandler)
			{
				mEngine.unregisterUpdateHandler(pTimerHandler);

				ZombleSceneManager.sharedInstance().loadResources(ZombleScenes.MENU);
				ZombleSceneManager.sharedInstance().createScene(ZombleScenes.MENU);
				ZombleSceneManager.sharedInstance().setCurrentScene(ZombleScenes.MENU);

				ZombleSceneManager.sharedInstance().unloadResources(ZombleScenes.SPLASH);
				ZombleSceneManager.sharedInstance().loadResources(ZombleScenes.GAME);

			}
		}));
		pOnPopulateSceneCallback.onPopulateSceneFinished();
	}
}