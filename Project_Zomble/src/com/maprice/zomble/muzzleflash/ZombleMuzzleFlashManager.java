package com.maprice.zomble.muzzleflash;

import org.andengine.opengl.texture.region.TiledTextureRegion;

import com.maprice.zomble.scene.ZombleSceneManager;
import com.maprice.zomble.util.contants.ZombleConstants;



/**
 * Manages the all activity with respect to muzzle flashes
 * 
 * @author maprice
 */


public class ZombleMuzzleFlashManager implements ZombleConstants{
	// ===========================================================
	// Constants
	// ===========================================================

	static final String LOG_TAG = PREFIX + ZombleMuzzleFlashManager.class.getSimpleName();

	// ===========================================================
	// Fields
	// ===========================================================
	
	private ZombleMuzzleFlash mMuzzleFlash;
	final private TiledTextureRegion mMuzzleFlashTextureRegion;
	
	// ===========================================================
	// Constructors
	// ===========================================================

	public ZombleMuzzleFlashManager(TiledTextureRegion pMuzzleFlashTextureRegion){
		mMuzzleFlashTextureRegion = pMuzzleFlashTextureRegion;
		mMuzzleFlash = new ZombleMuzzleFlash(0, 0, mMuzzleFlashTextureRegion);

	}

	// ===========================================================
	// Methods
	// ===========================================================

	public void muzzleFlash(float pX, float pY, double pAngle, int index) {

		float offsetX1 =  pX+(float)(ITEM_X_OFFSET_MULTIPLIER * 9 * Math.cos(-pAngle));
		float offsetY1 =  pY+(float)(ITEM_Y_OFFSET_MULTIPLIER * 9 * Math.sin(-pAngle));
		
		// Attach casing to scene
		if(!mMuzzleFlash.hasParent()){
			ZombleSceneManager.sharedInstance().getCurrentScene().attachChild(mMuzzleFlash);
		}

		// Start casing animation
		mMuzzleFlash.start(offsetX1-mMuzzleFlashTextureRegion.getWidth()/2,  offsetY1-mMuzzleFlashTextureRegion.getHeight()/2);
	}

	
}
