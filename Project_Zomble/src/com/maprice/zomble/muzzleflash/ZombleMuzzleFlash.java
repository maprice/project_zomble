package com.maprice.zomble.muzzleflash;

import org.andengine.entity.sprite.AnimatedSprite;
import org.andengine.opengl.texture.region.ITiledTextureRegion;

import com.maprice.zomble.scene.ZombleSceneManager;
import com.maprice.zomble.util.contants.ZombleConstants;



/**
 * Muzzle flash animation (appears after every shot)
 * 
 * @author maprice
 */


public class ZombleMuzzleFlash extends AnimatedSprite implements ZombleConstants{
	// ===========================================================
	// Constants
	// ===========================================================

	static final String LOG_TAG = PREFIX + ZombleMuzzleFlash.class.getSimpleName();

	// ===========================================================
	// Fields
	// ===========================================================

	// ===========================================================
	// Constructors
	// ===========================================================

	public ZombleMuzzleFlash(float pX, float pY, ITiledTextureRegion pTiledTextureRegion) {
		super(pX, pY, pTiledTextureRegion,ZombleSceneManager.sharedInstance().getVBOmanager());

		this.setZIndex(Layer.FRONT_EFFECTS.ordinal());
	}

	// ===========================================================
	// Methods
	// ===========================================================

	/**
	 * Starts the motion of the casing
	 * 
	 * @param pX
	 *            starting x position of flash
	 *            
	 * @param pY
	 *            starting y position of flash            
	 *            
	 */
	public void start(float pX, float pY) {

		this.setPosition(pX, pY);
		
		this.animate(10, false, new IAnimationListener(){

			@Override
			public void onAnimationStarted(AnimatedSprite pAnimatedSprite,
					int pInitialLoopCount) {
				// TODO Auto-generated method stub
				ZombleMuzzleFlash.this.setVisible(true);

			}

			@Override
			public void onAnimationFrameChanged(AnimatedSprite pAnimatedSprite,
					int pOldFrameIndex, int pNewFrameIndex) {


			}

			@Override
			public void onAnimationLoopFinished(AnimatedSprite pAnimatedSprite,
					int pRemainingLoopCount, int pInitialLoopCount) {


			}

			@Override
			public void onAnimationFinished(AnimatedSprite pAnimatedSprite) {
				ZombleMuzzleFlash.this.setVisible(false);

			}});
	}
}
