package com.maprice.zomble.projectile;

import com.maprice.zomble.bullet.ZombleBulletManager;
import com.maprice.zomble.casing.ZombleCasingManager;
import com.maprice.zomble.hero.ZombleHero;
import com.maprice.zomble.hero.ZombleHeroStats.GunType;
import com.maprice.zomble.muzzleflash.ZombleMuzzleFlashManager;
import com.maprice.zomble.util.contants.ZombleConstants;

/**
 * Handles firing projectiles
 * 
 * @author mprice
 */

public class ZombleProjectileManager implements ZombleConstants {

	// ===========================================================
	// Constants
	// ===========================================================

	static final String LOG_TAG = PREFIX + ZombleProjectileManager.class.getSimpleName();

	// ===========================================================
	// Fields
	// ===========================================================

	final ZombleMuzzleFlashManager mMuzzleFlashManager;
	final ZombleBulletManager mBulletManager;
	final ZombleCasingManager mCasingManager;

	// ===========================================================
	// Constructors
	// ===========================================================

	public ZombleProjectileManager(ZombleCasingManager pCasingManager, ZombleBulletManager pBulletManager, ZombleMuzzleFlashManager pMuzzleFlashManager) 
	{
		mCasingManager = pCasingManager;
		mBulletManager = pBulletManager;
		mMuzzleFlashManager = pMuzzleFlashManager;
	}

	// ===========================================================
	// Methods
	// ===========================================================

	public void shootProjectile(float pX, float pY, double pAngle, GunType pGunType, ZombleHero pSource){
		mBulletManager.shootProjectile(pX, pY, pAngle, pGunType, pSource);
		mCasingManager.dischargeCasing(pX, pY, pAngle, 1);
		mMuzzleFlashManager.muzzleFlash(pX, pY, pAngle, 0);
	}

	public void shootSpecialProjectile(float pX, float pY, double pAngle, GunType pGunType, ZombleHero pSource) {
		mBulletManager.shootProjectile(pX, pY, pAngle, pGunType, pSource);
		mBulletManager.shootProjectile(pX, pY, pAngle+.1, pGunType, pSource);
		mBulletManager.shootProjectile(pX, pY, pAngle-.1, pGunType, pSource);
		mCasingManager.dischargeCasing(pX, pY, pAngle, 1);
		mCasingManager.dischargeCasing(pX, pY, pAngle, 1);
		mCasingManager.dischargeCasing(pX, pY, pAngle, 1);
		
		mMuzzleFlashManager.muzzleFlash(pX, pY, pAngle, 0);
		
	}

}
