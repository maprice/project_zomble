package com.maprice.zomble.projectile;

import java.util.Timer;
import java.util.TimerTask;

import org.andengine.extension.tmx.TMXTile;
import org.andengine.extension.tmx.TMXTiledMap;
import org.andengine.util.Constants;

import android.util.Log;

import com.maprice.zomble.blood.ZombleBloodManager;
import com.maprice.zomble.bullet.ZombleBullet;
import com.maprice.zomble.bullet.ZombleBulletManager;
import com.maprice.zomble.enemy.ZombleEnemy;
import com.maprice.zomble.enemy.ZombleEnemyManager;
import com.maprice.zomble.hero.ZombleHero;
import com.maprice.zomble.hero.ZombleHeroManager;
import com.maprice.zomble.items.ZombleItem;
import com.maprice.zomble.items.ZombleItemManager;
import com.maprice.zomble.map.ZombleMapManager;
import com.maprice.zomble.numbers.ZombleNumberManager;
import com.maprice.zomble.scene.ZombleSceneManager;
import com.maprice.zomble.util.contants.ZombleCallback;
import com.maprice.zomble.util.contants.ZombleConstants;

/**
 * Handles ALL entity-bullet collisions and entity-entity collisions
 * 
 * @author mprice
 */

public class ZombleCollisionManager implements ZombleConstants {

	// ===========================================================
	// Constants
	// ===========================================================

	static final String LOG_TAG = PREFIX + ZombleCollisionManager.class.getSimpleName();

	public enum Direction{
		NORTH,
		EAST,
		SOUTH,
		WEST
	}

	// ===========================================================
	// Fields
	// ===========================================================

	private final ZombleEnemyManager mEnemyManager;
	private final ZombleBloodManager mBloodManager;
	private final ZombleBulletManager mBulletManager;
	private final ZombleNumberManager mNumberManager;
	private final ZombleHeroManager mCharacterManager;
	private final ZombleItemManager mItemManager;

	private boolean mMapLoaded = true;
	private Timer mMapLoadCoolDownTimer = new Timer();

	// ===========================================================
	// Constructors
	// ===========================================================

	public ZombleCollisionManager(ZombleEnemyManager pEnemyManager, ZombleBloodManager pBloodManager, ZombleBulletManager pBulletManager, ZombleHeroManager pCharacterManager, ZombleNumberManager pNumberManager, ZombleItemManager pItemManager) 
	{
		mEnemyManager = pEnemyManager;
		mBloodManager = pBloodManager;
		mBulletManager = pBulletManager;
		mCharacterManager = pCharacterManager;
		mNumberManager = pNumberManager;
		mItemManager = pItemManager;
	}


	// ===========================================================
	// Methods
	// ===========================================================

	public void onUpdate() {

		if(mMapLoaded){
			checkExits();
		}

		for(ZombleHero mCharacter : mCharacterManager.getCharacters()){
			for(int j = 0; j < mEnemyManager.getActiveEnemys().size(); j++){
				ZombleEnemy mZomble = mEnemyManager.getActiveEnemys().get(j);
				if(mCharacter.getEntity().getFeetHitbox().collidesWith(mZomble.getEntity().getFeetHitbox())){

					if(mZomble.shouldAttack()){
						mBloodManager.splatter(mCharacter.getEntity().getX(), mCharacter.getEntity().getY(), mZomble.getEntity().getCurrentAngle(), mZomble.getEntity().getGround());
						mNumberManager.dropNumber(mCharacter.getEntity().getX(), mCharacter.getEntity().getY(), mZomble.getStats().getDamage());
						mCharacter.takeDamage(mZomble.getStats().getDamage());
					}
					//mCharacter.getEntity().knockBack(((ZombleEnemy)mZomble).getEntity().getCurrentAngle());
				}
			}
			
			for(int j = 0; j < mItemManager.getItems().size(); j++){
				ZombleItem mZomble = mItemManager.getItems().get(j);
				if(mCharacter.getEntity().getFeetHitbox().collidesWith(mZomble)){
					mCharacter.obtainItem(mZomble);
					mItemManager.removeItem(mZomble);
				}
			}
		}

		for(int j = 0; j < mEnemyManager.getActiveEnemys().size(); j++){
			ZombleEnemy mZomble = mEnemyManager.getActiveEnemys().get(j);

			// Cant do a for each for now
			for(int i = 0; i < mBulletManager.getActiveBullets().size(); i++){
				ZombleBullet b =  mBulletManager.getActiveBullets().get(i);

				if(b.collidesWith( mZomble.getEntity().getSpriteHitbox())){

					mBloodManager.splatter(b.getX(), b.getY(), b.getBulletDirection(), mZomble.getEntity().getGround());

					mNumberManager.dropNumber(mZomble.getEntity().getX(), mZomble.getEntity().getY(), mCharacterManager.getCharacters().get(0).getStats().getDamage());
					mZomble.takeDamage(b.getBulletDirection(), mCharacterManager.getCharacters().get(0).getStats().getDamage(), b.getSource());

					mBulletManager.bulletDidFinish(b);
				}
			}
		}

	}


	public void startMapCooldown() {
		mMapLoaded = false;
		mMapLoadCoolDownTimer.schedule(new LoadMapCoolDownTask(), MAP_LOAD_COOLDOWN);
	}

	final class LoadMapCoolDownTask extends TimerTask {
		public void run() {
			mMapLoaded = true;
		}
	}

	// Needs to be modularised
	private void checkExits() {
		for(ZombleHero mCharacter : mCharacterManager.getCharacters()){

			final float[] lPlayerCordinates = mCharacter.getEntity().getCoordinates();


			/* Get the tile the feet of the player are currently waking on. */
			final TMXTile lFromTmxTile = ZombleMapManager.sharedInstance().getCurrentLayer().getTMXTileAt(
					lPlayerCordinates[Constants.VERTEX_INDEX_X], 
					lPlayerCordinates[Constants.VERTEX_INDEX_Y]
			);

			if(lFromTmxTile == null){
				Log.e(LOG_TAG, "Null Tile!");
				return;
			}

			int lTileCol = lFromTmxTile.getTileColumn();
			int lTileRow = lFromTmxTile.getTileRow();

			Direction lDirection = null;

			if(ZombleMapManager.sharedInstance().getMapColumns() - 1 == lTileCol){
				Log.v(LOG_TAG, "Exit east");
				lDirection = Direction.EAST;
			}
			else if(lTileCol == 0){
				Log.v(LOG_TAG, "Exit west");
				lDirection = Direction.WEST;
			}
			else if(ZombleMapManager.sharedInstance().getMapRows() - 1 == lTileRow){
				Log.v(LOG_TAG, "Exit south");
				lDirection = Direction.SOUTH;
			}
			else if(lTileRow == 0){
				Log.v(LOG_TAG, "Exit north");
				lDirection = Direction.NORTH;
			}
			if(lDirection != null){
				this.startMapCooldown();
				final Direction fDirection = lDirection;

				mEnemyManager.clearEnemys();
				mItemManager.clearItems();
				ZombleMapManager.sharedInstance().loadNextMap(fDirection, new ZombleCallback<TMXTiledMap>(){

					@Override
					public void onComplete(final TMXTiledMap pResult) {
						ZombleMapManager.sharedInstance().switchMaps(pResult);


						ZombleSceneManager.sharedInstance().getEngine().runOnUpdateThread(new Runnable(){

							@Override
							public void run() {
								float lNewX = lPlayerCordinates[Constants.VERTEX_INDEX_X];
								float lNewY = lPlayerCordinates[Constants.VERTEX_INDEX_Y];

								float lNewWaypointX = lNewX;
								float lNewWaypointY = lNewY;

								switch(fDirection){
								case NORTH:
									lNewY = pResult.getTileHeight() * pResult.getTileRows() * SCALE - (pResult.getTileHeight() * SCALE)/2;
									lNewWaypointY = lNewY - pResult.getTileHeight() * SCALE;
									break;
								case EAST:
									lNewX = (pResult.getTileWidth() * SCALE) / 2;
									lNewWaypointX = lNewX + pResult.getTileWidth() * SCALE;
									break;
								case SOUTH:
									lNewY = (pResult.getTileHeight() * SCALE) / 2;
									lNewWaypointY = lNewY + pResult.getTileHeight() * SCALE;
									break;
								case WEST:
									lNewX = pResult.getTileWidth() * pResult.getTileColumns() * SCALE - (pResult.getTileWidth() * SCALE)/2;
									lNewWaypointX = lNewX - pResult.getTileWidth() * SCALE;
									break;
								}
								mCharacterManager.onSwitchMap(lNewX, lNewY, lNewWaypointX, lNewWaypointY);

								mEnemyManager.onSwitchMap();
								mItemManager.onSwitchMap();
								ZombleSceneManager.sharedInstance().getCurrentScene().sortChildren();
							}
						});
					}

					@Override
					public void onError(String pErrorMessage) {
						Log.e(LOG_TAG, pErrorMessage);
					}
				});

				ZombleSceneManager.sharedInstance().getCurrentScene().sortChildren();

				// Dont check for other characters
				return;
			}
		}
	}
}