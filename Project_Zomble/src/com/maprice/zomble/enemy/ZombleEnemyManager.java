package com.maprice.zomble.enemy;

import java.util.LinkedList;
import java.util.List;

import org.andengine.entity.Entity;
import org.andengine.opengl.texture.region.TiledTextureRegion;
import org.andengine.util.Constants;

import android.graphics.Point;
import android.util.Log;

import com.maprice.zomble.hero.ZombleHero;
import com.maprice.zomble.hero.ZombleHeroManager;
import com.maprice.zomble.map.ZombleMapManager;
import com.maprice.zomble.orbs.ZombleOrbsManager;
import com.maprice.zomble.scene.ZombleSceneManager;
import com.maprice.zomble.util.contants.ZombleConstants;


/**
 * Manages the all activity with respect to zombles
 * 
 * @author maprice
 */


public class ZombleEnemyManager implements ZombleConstants{
	// ===========================================================
	// Constants
	// ===========================================================

	static final String LOG_TAG = PREFIX + ZombleEnemyManager.class.getSimpleName();

	// ===========================================================
	// Fields
	// ===========================================================

	final private Entity mScene;
	final ZombleEnemyPool mEnemyPool;
	private List<ZombleEnemy> mActiveEnemyList = new LinkedList<ZombleEnemy>();
	private final ZombleOrbsManager mOrbManager;

	private final EnemyListener mEnemyListener = new EnemyListener(){

		@Override
		public void onZombleRemove(ZombleEnemy pZomble, ZombleHero pSource) {
			mOrbManager.dropOrbs(pZomble.getEntity(), pSource);
			mEnemyPool.recyclePoolItem(pZomble);
			
		}

		@Override
		public void onZombleInactive(ZombleEnemy pZomble) {
			mActiveEnemyList.remove(pZomble);
			
		}
	};
	
	// ===========================================================
	// Constructors
	// ===========================================================

	public ZombleEnemyManager(ZombleOrbsManager pOrbManager, TiledTextureRegion pZombleTextureRegion,
			TiledTextureRegion pZombleArmTextureRegion,
			TiledTextureRegion pZombleDeadTextureRegion,
			Entity mSpriteLayer) {

		mOrbManager = pOrbManager;
		mEnemyPool = new ZombleEnemyPool(pZombleTextureRegion, pZombleArmTextureRegion, pZombleDeadTextureRegion, mEnemyListener);
		mScene = mSpriteLayer;	
	}

	// ===========================================================
	// Methods
	// ===========================================================

	public void spawnZomble(final float pX, final float pY){
		
		ZombleSceneManager.sharedInstance().getEngine().runOnUpdateThread(new Runnable(){

			@Override
			public void run() {
				ZombleEnemy zomble = mEnemyPool.obtainPoolItem();
				zomble.getEntity().setPosition(pX, pY);
				
				mActiveEnemyList.add(zomble);
				
				if(!zomble.getEntity().hasParent()){
				mScene.attachChild(zomble.getEntity());
				}
				
			}
			
			
			
			
		});
		

	}

	public List<ZombleEnemy> getActiveEnemys(){
		return mActiveEnemyList;
	}

	/**
	 * Interface to handle call-backs
	 */
	public interface EnemyListener{
		public void onZombleInactive (ZombleEnemy pZomble);
		public void onZombleRemove (ZombleEnemy pZomble, ZombleHero pSource);
	}

	public void onUpdate(ZombleHeroManager mCharacterManager) {
		for(ZombleEnemy zomble : this.getActiveEnemys()){
			float x = 0;
			float y = 0;
			float minDelta = -1;
			for(ZombleHero character : mCharacterManager.getCharacters()){
				float delta = Math.abs(character.getEntity().getX() - zomble.getEntity().getX()) +
				Math.abs(character.getEntity().getY() - zomble.getEntity().getY());

				if(delta < minDelta || minDelta == -1){
					minDelta = delta;
					//character.getEntity().getColor()[Constants.VERTEX_INDEX_X]
					x = character.getEntity().getCoordinates()[Constants.VERTEX_INDEX_X];
					y = character.getEntity().getCoordinates()[Constants.VERTEX_INDEX_Y];
				}
				
			}
			
			zomble.onUpdate(x, y);
			
			//Log.e(LOG_TAG, "ACR%IVE");
			
			//mScene.sortChildren();
			//ZombleSceneManager.sharedInstance().getCurrentScene().sortChildren();
		}
	}

	int zomblenum = 10;
	
	public void clearEnemys(){
		Log.e(LOG_TAG, "Nuked enemeys");
		
		for(ZombleEnemy lEnemy : mActiveEnemyList){
			mEnemyPool.recyclePoolItem(lEnemy);
		}
		
		mActiveEnemyList.clear();
		
		
	}
	
	public void onSwitchMap() {
		Log.e(LOG_TAG, "Added enemys");
		for(int i = 0; i < zomblenum; i++){
		
			Point lSpawnLocation = ZombleMapManager.sharedInstance().getValidSpawnPoint();
			this.spawnZomble(lSpawnLocation.x, lSpawnLocation.y);
		}
		mScene.sortChildren();

	}
}
