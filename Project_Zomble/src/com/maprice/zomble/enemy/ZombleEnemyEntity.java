package com.maprice.zomble.enemy;

import org.andengine.engine.handler.physics.PhysicsHandler;
import org.andengine.entity.primitive.Rectangle;
import org.andengine.entity.sprite.AnimatedSprite;
import org.andengine.entity.sprite.AnimatedSprite.IAnimationListener;
import org.andengine.opengl.texture.region.TiledTextureRegion;

import com.maprice.zomble.entity.ZombleBaseCharacterEntity;
import com.maprice.zomble.entity.ZombleBaseCharacterSprite;
import com.maprice.zomble.scene.ZombleSceneManager;
import com.maprice.zomble.util.contants.ZombleCallback;
import com.maprice.zomble.util.contants.ZombleConstants;
import com.maprice.zomble.util.contants.ZombleUtil;


/**
 * Enemy entity
 * 
 * @author mprice
 */

public class ZombleEnemyEntity extends ZombleBaseCharacterEntity implements ZombleConstants {
	// ===========================================================
	// Constants
	// ===========================================================

	static final String LOG_TAG = PREFIX + ZombleEnemyEntity.class.getSimpleName();

	public enum CharacterLayer {
		BEHIND (2),
		CHARACTER (3),
		FRONT (4);

		final int value;   

		CharacterLayer(int value) {
			this.value = value;
		}

		public int index() { 
			return value; 
		}
	}


	// ===========================================================
	// Fields
	// ===========================================================

	final private ZombleBaseCharacterSprite mCharacterSprite;
	final private ZombleEnemyArmSprite mGunSprite;
	final private ZombleEnemyHealthBar mHealthBar;
	final private AnimatedSprite mDeadZomble;
	final private PhysicsHandler mPhysicsHandler;

	
	// ===========================================================
	// Constructors
	// ===========================================================

	public ZombleEnemyEntity(TiledTextureRegion characterTextureRegion,
			TiledTextureRegion itemTextureRegion,
			TiledTextureRegion mZombleDeadTextureRegion) 
	{
		super(0,0);
		
		this.setCullingEnabled(true);
		
		this.setZIndex(Layer.SPRITES.ordinal());
		
		this.setTouchHitbox(new Rectangle(
				-characterTextureRegion.getWidth()*TOUCH_HITBOX_SCALE_X/2,  
				-characterTextureRegion.getHeight()*TOUCH_HITBOX_SCALE_Y/2,
				characterTextureRegion.getWidth()*TOUCH_HITBOX_SCALE_X,  
				characterTextureRegion.getHeight()*TOUCH_HITBOX_SCALE_Y,
				ZombleSceneManager.sharedInstance().getVBOmanager()));
		
		this.setSpriteHitbox(new Rectangle(
				-characterTextureRegion.getWidth()/2,  
				-characterTextureRegion.getHeight()/2,
				characterTextureRegion.getWidth(),  
				characterTextureRegion.getHeight(),
				ZombleSceneManager.sharedInstance().getVBOmanager()));
		
		this.setFeetHitbox(new Rectangle(
				-characterTextureRegion.getWidth()/2,  
				characterTextureRegion.getHeight()/4,
				characterTextureRegion.getWidth(),  
				characterTextureRegion.getHeight()/4,
				ZombleSceneManager.sharedInstance().getVBOmanager()));

		
		mCharacterSprite = new ZombleBaseCharacterSprite(-characterTextureRegion.getWidth()/2, -characterTextureRegion.getHeight()/2, characterTextureRegion, ENEMY_FRAME_SPEED);
		mGunSprite = new ZombleEnemyArmSprite(-itemTextureRegion.getWidth()/2, -itemTextureRegion.getHeight()/2, itemTextureRegion);
		mHealthBar = new ZombleEnemyHealthBar();
		mDeadZomble = new AnimatedSprite(-mZombleDeadTextureRegion.getWidth()/2, -mZombleDeadTextureRegion.getHeight()/2, mZombleDeadTextureRegion, ZombleSceneManager.sharedInstance().getVBOmanager());

		mPhysicsHandler = new PhysicsHandler(this);

		// Set scale
		this.setScale(SCALE);


		this.attachChild(mCharacterSprite);
		this.attachChild(mGunSprite);
		this.attachChild(mHealthBar);
		this.registerUpdateHandler(mPhysicsHandler);

	}

	// ===========================================================
	// Methods
	// ===========================================================



	/**
	 * Moves the character in the appropriate direction and animates the sprite accordingly
	 * 
	 * @param pValueX
	 *            x value of the analog control
	 *            
	 * @param pValueY
	 *            y value of the analog control
	 */
	public void walk(float pValueX, float pValueY){
		double angle = ZombleUtil.getAngle(pValueX, pValueY);

		// Animates the character sprite to walk in the correct direction
		mCharacterSprite.walk(angle);

		// Update the current velocity
		mPhysicsHandler.setVelocity(pValueX * ENEMY_WALK_SPEED_MULTIPLIER, pValueY * ENEMY_WALK_SPEED_MULTIPLIER);
	}



	/**
	 * Angles the character in the appropriate direction
	 * 
	 * @param pValueX
	 *            x value of the analog control
	 *            
	 * @param pValueY
	 *            y value of the analog control
	 */
	public void look(float pValueX, float pValueY){

		// Get the angle in radians from [0, 2PI]
		double angle = ZombleUtil.getAngle(pValueX, pValueY);
		mCurrentAngle = angle;
		// Angles the gun in the correct orientation
		mGunSprite.look(angle);

		// Angles the character in the correct orientation
		mCharacterSprite.look(angle);
	}


	/**
	 * Stops all animation and movement of the character sprite 
	 */
	public void stop() {
		// Stop motion of the character
		mPhysicsHandler.setVelocity(0, 0);

		// Stop animation of the character
		mCharacterSprite.stop();
	}

	public float getGround() {
		return (mCharacterSprite.getHeightScaled())+this.getY();
	}

	public void knockBack(double bulletDirection) {

		float gox = (float) Math.cos(bulletDirection);
		float goy = (float) Math.sin(-bulletDirection);

		// Update the current velocity
		mPhysicsHandler.setVelocity(gox * ENEMY_WALK_SPEED_MULTIPLIER * 2, goy * ENEMY_WALK_SPEED_MULTIPLIER * 2);

	}
	
	public void updateHealthBar(int pCurrentHealth, int pTotalHealth) {
		mHealthBar.updateHealth(pCurrentHealth, pTotalHealth);
	}

	public void playDeathAnimation(final ZombleCallback<ZombleEnemyEntity> zombleCallback) {

		stop();
		long[] mDuration = new long[ENEMY_DEAD_SPRITE_SHEET_COL];

		int frame = mCharacterSprite.getCurrentAngle()*(ENEMY_DEAD_SPRITE_SHEET_COL);
		int[] mframe = {frame+0,frame+1,frame+2,frame+3,frame+4};
		
		// Setup the animation duration array
		for(int i = 0; i < mDuration.length; i++)  mDuration[i] = 70;

		if(!mDeadZomble.hasParent()){
			this.attachChild(mDeadZomble);
		}

		mCharacterSprite.setVisible(false);
		mGunSprite.setVisible(false);

		mDeadZomble.animate(mDuration, mframe, false, new IAnimationListener(){

			@Override
			public void onAnimationStarted(AnimatedSprite pAnimatedSprite,
					int pInitialLoopCount) {
				mDeadZomble.setVisible(true);
				// TODO Auto-generated method stub

			}

			@Override
			public void onAnimationFrameChanged(AnimatedSprite pAnimatedSprite,
					int pOldFrameIndex, int pNewFrameIndex) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onAnimationLoopFinished(AnimatedSprite pAnimatedSprite,
					int pRemainingLoopCount, int pInitialLoopCount) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onAnimationFinished(AnimatedSprite pAnimatedSprite) {
				mDeadZomble.setVisible(false);
				mCharacterSprite.setVisible(false);
				mGunSprite.setVisible(false);
				mHealthBar.setVisible(false);
				
				zombleCallback.onComplete(ZombleEnemyEntity.this);

			}
		});

	}

	public void resetInteractions() {
		this.setVisible(true);
		mCharacterSprite.setVisible(true);
		mGunSprite.setVisible(true);
		mHealthBar.setVisible(true);
		
		if(this.getUpdateHandlerCount() == 0){
			registerUpdateHandler(mPhysicsHandler);
		}
		
		this.setZIndex(0);
	}

}
