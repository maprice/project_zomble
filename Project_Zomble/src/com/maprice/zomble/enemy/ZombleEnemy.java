package com.maprice.zomble.enemy;


import java.util.Timer;
import java.util.TimerTask;

import org.andengine.opengl.texture.region.TiledTextureRegion;
import org.andengine.util.Constants;
import org.andengine.util.algorithm.path.Path;
import org.andengine.util.color.Color;

import com.maprice.zomble.enemy.ZombleEnemyManager.EnemyListener;
import com.maprice.zomble.hero.ZombleHero;
import com.maprice.zomble.map.ZombleMapManager;
import com.maprice.zomble.util.contants.ZombleCallback;
import com.maprice.zomble.util.contants.ZombleConstants;
import com.maprice.zomble.util.contants.ZomblePathMapper;
import com.maprice.zomble.util.contants.ZombleUtil;

/**
 * Wraps the statistics of a zomble with its entity
 * 
 * @author maprice
 */


public class ZombleEnemy implements ZombleConstants {

	// ===========================================================
	// Constants
	// ===========================================================

	public enum State {
		DOCILE,
		DEATH,
		FLEEING,
		AGGROED;
	}

	static final String LOG_TAG = PREFIX + ZombleEnemy.class.getSimpleName();

	// ===========================================================
	// Fields
	// ===========================================================

	final private EnemyListener mEnemyListener;

	final private ZombleEnemyEntity mZombleEnemyCharacterEntity;
	final private ZombleEnemyStats mZombleEnemyStats;
	private State mState = State.DOCILE;

	private boolean mCanAttack = true;
	private boolean mShouldUpdate = true;
	private Timer mCoolDownTimer = new Timer();
	private ZomblePathMapper mPathMapper;

	private Path mCurrentPath;
	private int mCurrentPathIndex = 1;


	// ===========================================================
	// Constructors
	// ===========================================================

	public ZombleEnemy(TiledTextureRegion pCharacterTextureRegion,
			TiledTextureRegion pItemTextureRegion,
			TiledTextureRegion pZombleDeadTextureRegion, EnemyListener pEnemyListener) {

		mEnemyListener = pEnemyListener;

		mZombleEnemyCharacterEntity = new ZombleEnemyEntity(
				pCharacterTextureRegion,
				pItemTextureRegion,
				pZombleDeadTextureRegion);

		mZombleEnemyStats = new ZombleEnemyStats(1.0f, 10, 1);

		if(DEBUG){
			mPathMapper = new ZomblePathMapper(Color.RED);
		}
	}


	// ===========================================================
	// Methods
	// ===========================================================


	public ZombleEnemyEntity getEntity(){
		return mZombleEnemyCharacterEntity;
	}

	public ZombleEnemyStats getStats(){
		return mZombleEnemyStats;
	}


	/**
	 * Called when zomble takes damage
	 * 
	 * @param pBulletDirection
	 *            the angle the bullet is travelling (Radians [0 , 2PI])
	 *            
	 * @param pAmount
	 *            the amount of damage done
	 * @param pSourceEntity 
	 *          
	 *            
	 */
	public void takeDamage(double pBulletDirection, int pAmount, final ZombleHero pSource) {
		mState = State.AGGROED;

		//Cause knockback with respect to bulletDirection
		// TODO need to rework how knockback functions
		//mZombleEnemyCharacterEntity.knockBack(pBulletDirection);

		//Subtract health and check if it is less than or equal to zero
		if(mZombleEnemyStats.subtractHealth(pAmount)){
			startDeathSequence(pSource);
		}

		//Update health bar
		mZombleEnemyCharacterEntity.updateHealthBar(mZombleEnemyStats.getCurrentHealth(), mZombleEnemyStats.getTotalHealth());
	}



	/**
	 * Called when zomble health reaches 0  
	 * @param pSourceEntity 
	 * 			The Id of the character who killed the zomble
	 */
	private void startDeathSequence(final ZombleHero pSource) {
		mState = State.DEATH;

		// Set zomble to inactive
		mEnemyListener.onZombleInactive(ZombleEnemy.this);
		
		// Play death animation
		mZombleEnemyCharacterEntity.playDeathAnimation(new ZombleCallback<ZombleEnemyEntity>(){

			@Override
			public void onComplete(ZombleEnemyEntity pResult) {
				mEnemyListener.onZombleRemove(ZombleEnemy.this, pSource);
			}

			@Override
			public void onError(String pErrorMessage) {
				throw new RuntimeException(pErrorMessage);
			}
		});
		
		if(DEBUG){
			this.mPathMapper.clearPath();
		}
	}


	public void resetInteractions() {
		this.getEntity().stop();
		mState = State.DOCILE;
		mZombleEnemyCharacterEntity.resetInteractions();
		mZombleEnemyStats.resetInteractions();
		
	}


	public void updatePath(float pX, float pY){
		// Find new path
		Path lNewPath = ZombleMapManager.sharedInstance().findPath(
				this.getEntity().getCoordinates()[Constants.VERTEX_INDEX_X],
				this.getEntity().getCoordinates()[Constants.VERTEX_INDEX_Y], 
				pX,
				pY
		);

		// If new path exists
		if(lNewPath != null){
			mCurrentPath = lNewPath;
			mCurrentPathIndex = 1;
			//	mZombleWaypoint.setWaypoint(pX, pY);

			if(DEBUG){
				mPathMapper.drawPath(mCurrentPath);
			}
		}
	}


	public void onUpdate(float pX, float pY) {
		switch(mState){
		case DOCILE:
			return;
		case FLEEING:
			return;
		case DEATH:
			return;
		case AGGROED:

			float[] lCoordinates = this.getEntity().getCoordinates();

			if(shouldUpdatePath()){
				this.updatePath(pX, pY);
			}

			if(ZombleUtil.collidesWith(lCoordinates[Constants.VERTEX_INDEX_X], lCoordinates[Constants.VERTEX_INDEX_Y] , pX, pY, 20)) {
				// The player has reached their final destination
				if(mCurrentPathIndex == mCurrentPath.getLength()-1){
					this.getEntity().stop();
					if(DEBUG){
						this.mPathMapper.clearPath();
					}
				}
				else{
					mCurrentPathIndex++;
				}

			}
			else if(mCurrentPath != null) {

				float deltaX =  mCurrentPath.getX(mCurrentPathIndex) - lCoordinates[Constants.VERTEX_INDEX_X];
				float deltaY =  mCurrentPath.getY(mCurrentPathIndex) - lCoordinates[Constants.VERTEX_INDEX_Y];

				double angleInDegrees = Math.atan2(deltaY, deltaX);


				// Sets the velocity in the correct direction
				float gox = (float) Math.cos(angleInDegrees);
				float goy = (float) Math.sin(angleInDegrees);


				this.getEntity().walk(gox, goy);
				this.getEntity().look(gox, goy);

			}
		}
	}

	private void updateZIndex(float pY) {
		this.getEntity().setZIndex((int)this.getEntity().getY());
	}


	public boolean shouldAttack() {
		if (mCanAttack) {
			mCanAttack = false;
			mCoolDownTimer.schedule(new AttackCoolDownTask(), ENEMY_ATTACK_COOLDOWN);
			return true;
		}
		return false;
	}


	public boolean shouldUpdatePath() {
		if (mShouldUpdate) {
			mShouldUpdate = false;
			mCoolDownTimer.schedule(new PathCoolDownTask(), ENEMY_PATH_POLL_COOLDOWN);
			return true;
		}
		return false;
	}

	final class AttackCoolDownTask extends TimerTask {
		public void run() {
			mCanAttack = true;
		}
	}

	final class PathCoolDownTask extends TimerTask {
		public void run() {
			mShouldUpdate = true;
		}
	}


}