package com.maprice.zomble.enemy;

import org.andengine.opengl.texture.region.TiledTextureRegion;
import org.andengine.util.adt.pool.GenericPool;

import com.maprice.zomble.enemy.ZombleEnemyManager.EnemyListener;
import com.maprice.zomble.scene.ZombleSceneManager;
import com.maprice.zomble.util.contants.ZombleConstants;

/**
 * Pool system that controls the allocation and deletion of zombles
 * 
 * @author maprice
 */


public class ZombleEnemyPool extends GenericPool<ZombleEnemy> implements ZombleConstants{

	// ===========================================================
	// Fields
	// ===========================================================

	private TiledTextureRegion mZombleTextureRegion; 
	private TiledTextureRegion mZombleArmTextureRegion;
	private TiledTextureRegion mZombleDeadTextureRegion;
	private EnemyListener mEnemyListener;
	
	// ===========================================================
	// Constructors
	// ===========================================================

	public ZombleEnemyPool(TiledTextureRegion pZombleTextureRegion,
			TiledTextureRegion pZombleArmTextureRegion,
			TiledTextureRegion pZombleDeadTextureRegion, EnemyListener pEnemyListener) {
		
		mZombleTextureRegion = pZombleTextureRegion;
		mZombleArmTextureRegion = pZombleArmTextureRegion;
		mZombleDeadTextureRegion = pZombleDeadTextureRegion;
		mEnemyListener = pEnemyListener;
	}

	// ===========================================================
	// Methods
	// ===========================================================


	@Override
	protected ZombleEnemy onAllocatePoolItem() {
		return new ZombleEnemy(
				mZombleTextureRegion,
				mZombleArmTextureRegion,
				mZombleDeadTextureRegion,
				mEnemyListener);
	}

	@Override
	protected void onHandleRecycleItem(final ZombleEnemy pZombleEnemy) {

		ZombleSceneManager.sharedInstance().getEngine().runOnUpdateThread(new Runnable(){
			@Override
			public void run() {
			
		
				//TODO double running on update thread fucking up
				pZombleEnemy.getEntity().setIgnoreUpdate(true);
				pZombleEnemy.getEntity().clearUpdateHandlers();
				pZombleEnemy.getEntity().clearEntityModifiers();
				pZombleEnemy.getEntity().setVisible(false);
				
				if(pZombleEnemy.getEntity().hasParent())
				pZombleEnemy.getEntity().detachSelf();
			}
		});
	}

	/**
	 * Called just before a particle is returned to the caller, this is where you write your initialize code
	 * i.e. set location, rotation, etc.
	 */
	@Override
	protected void onHandleObtainItem(final ZombleEnemy pZombleEnemy) {
		pZombleEnemy.getEntity().setIgnoreUpdate(false);
		pZombleEnemy.resetInteractions();
	}
}

