package com.maprice.zomble.enemy;

import java.util.Timer;
import java.util.TimerTask;

import org.andengine.entity.Entity;
import org.andengine.entity.modifier.AlphaModifier;
import org.andengine.entity.primitive.Rectangle;

import com.maprice.zomble.scene.ZombleSceneManager;
import com.maprice.zomble.util.contants.ZombleConstants;



/**
 * Controls the healthBar above zombles
 * 
 * @author maprice
 */


public class ZombleEnemyHealthBar extends Entity implements ZombleConstants{
	// ===========================================================
	// Constants
	// ===========================================================

	static final String LOG_TAG = PREFIX + ZombleEnemyHealthBar.class.getSimpleName();

	// ===========================================================
	// Fields
	// ===========================================================

	private final Rectangle mRedBar;
	private final Rectangle mGreenBar;
	private Timer mTimer;
	private Task mHideHealth;

	// ===========================================================
	// Constructors
	// ===========================================================

	public ZombleEnemyHealthBar(){
		mRedBar = new Rectangle(-HUD_ENEMY_HEALTH_BAR_WIDTH/2, -15, HUD_ENEMY_HEALTH_BAR_WIDTH, HUD_ENEMY_HEALTH_BAR_HEIGHT, ZombleSceneManager.sharedInstance().getVBOmanager());
		mGreenBar = new Rectangle(-HUD_ENEMY_HEALTH_BAR_WIDTH/2, -15, HUD_ENEMY_HEALTH_BAR_WIDTH, HUD_ENEMY_HEALTH_BAR_HEIGHT, ZombleSceneManager.sharedInstance().getVBOmanager());

		mRedBar.setColor(1.0f, 0.0f, 0.0f);
		mGreenBar.setColor(0.0f, 1.0f, 0.0f);

		this.attachChild(mRedBar);
		this.attachChild(mGreenBar);

		mRedBar.setAlpha(0.0f);
		mGreenBar.setAlpha(0.0f);
	}

	// ===========================================================
	// Methods
	// ===========================================================


	public void updateHealth(int pCurrentHealth, int pTotalHealth){
		// Update health bar display
		mGreenBar.setWidth(((float)pCurrentHealth/pTotalHealth)*mRedBar.getWidth());
		
		// Removes any old alpha modifiers on health bars
		mRedBar.clearEntityModifiers();
		mGreenBar.clearEntityModifiers();
		
		// Resets alpha on health bars
		mRedBar.setAlpha(HUD_ALPHA);
		mGreenBar.setAlpha(HUD_ALPHA);
		
		// Updates the hideHealth timer
		updateTimer();
	}

	public void updateTimer(){
		// Cancels timers
		if(mTimer != null){
			mTimer.cancel();	
			mHideHealth.cancel();
		}

		// Starts new timers
		mHideHealth = new Task();
		mTimer = new Timer();
		mTimer.schedule(mHideHealth, HUD_ENEMY_HEALTH_BAR_UPTIME);
	}

	class Task extends TimerTask {
		public void run(){
			ZombleEnemyHealthBar.this.mRedBar.registerEntityModifier(new AlphaModifier(1, HUD_ALPHA, 0));
			ZombleEnemyHealthBar.this.mGreenBar.registerEntityModifier(new AlphaModifier(1, HUD_ALPHA, 0));
		}
	}


}
