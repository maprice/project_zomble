package com.maprice.zomble.enemy;

import com.maprice.zomble.entity.ZombleBaseCharacterStats;
import com.maprice.zomble.util.contants.ZombleConstants;



/**
 * All statistical information pertaining to one enemy entity
 * 
 * @author maprice
 */


public class ZombleEnemyStats extends ZombleBaseCharacterStats implements ZombleConstants{
	// ===========================================================
	// Constants
	// ===========================================================

	static final String LOG_TAG = PREFIX + ZombleEnemyStats.class.getSimpleName();

	// ===========================================================
	// Fields
	// ===========================================================


	// ===========================================================
	// Constructors
	// ===========================================================

	public ZombleEnemyStats(float pSpeed, int pTotalHealth, int pDamage){
		super(pSpeed, pTotalHealth, pDamage);
	}

	// ===========================================================
	// Methods
	// ===========================================================

}
