package com.maprice.zomble.enemy;

import org.andengine.opengl.texture.region.ITiledTextureRegion;

import com.maprice.zomble.entity.ZombleBaseCharacterArmSprite;
import com.maprice.zomble.scene.ZombleSceneManager;
import com.maprice.zomble.util.contants.ZombleConstants;


/**
 * Enemy arm sprite
 * 
 * @author maprice
 */


public class ZombleEnemyArmSprite extends ZombleBaseCharacterArmSprite implements ZombleConstants{
	// ===========================================================
	// Constants
	// ===========================================================

	public ZombleEnemyArmSprite(float pX, float pY,
			ITiledTextureRegion pTiledTextureRegion) {
		super(pX, pY, pTiledTextureRegion, ZombleSceneManager.sharedInstance().getVBOmanager());

		mITEM_X_OFFSET_MULTIPLIER = ARM_ITEM_X_OFFSET_MULTIPLIER;
		mITEM_Y_OFFSET_MULTIPLIER = ARM_ITEM_Y_OFFSET_MULTIPLIER;

		mITEM_FRAME_BEHIND = ZITEM_FRAME_BEHIND;
		mITEM_FRAME_END = ZITEM_FRAME_END;

	}


	static final String LOG_TAG = PREFIX + ZombleEnemyArmSprite.class.getSimpleName();

	// ===========================================================
	// Fields
	// ===========================================================



	// ===========================================================
	// Constructors
	// ===========================================================


	// ===========================================================
	// Methods
	// ===========================================================


}
