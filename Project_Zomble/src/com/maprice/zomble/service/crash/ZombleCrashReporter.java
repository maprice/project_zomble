package com.maprice.zomble.service.crash;

import com.maprice.zomble.util.contants.ZombleCallback;

/**
 * Abstract crash reporting interface for logging and sending of crash logs
 * 
 * @author maprice
 */


public interface ZombleCrashReporter {

	public ZombleCallback<?> initialize();

	public ZombleCallback<?> reporterEnable();
	public ZombleCallback<?> reporterDisable();

	public ZombleCallback<?> sendPendingCrashLogs();

}
