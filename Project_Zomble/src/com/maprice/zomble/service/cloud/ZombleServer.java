package com.maprice.zomble.service.cloud;

import com.maprice.zomble.util.contants.ZombleCallback;


/**
 * Abstract server interface for sending and receiving data from a remote server backend
 * 
 * @author maprice
 */

public interface ZombleServer {
	/**
	 * initialises server backend
	 */
	public ZombleCallback<?> initialize();
	public ZombleCallback<?> initializeWithFacebook(String pAppId);

	/**
	 * user management
	 */
	public ZombleCallback<?> userCreate(String pUsername, String pPassword);
	public ZombleCallback<?> userCreate(String pUsername, String pPassword, String pEmail);

	public ZombleCallback<?> userLogin(String pUsername, String pPassword);
	public ZombleCallback<?> userLogout();
	public ZombleCallback<?> userCachedLogin();
	public ZombleCallback<?> userDelete();
	public ZombleCallback<?> userResetPassword(String pEmail);

	/**
	 * analytics
	 */
	public ZombleCallback<?> trackEvent(String pDomain, String pKingdom, String pPhylum);

	/**
	 * data storage
	 */
	public ZombleCallback<?> scorePushUserGameData(int pDays, int pKills, int pBulletsFired, int pDistanceTraveled, int pAmmoCollected);
	public ZombleCallback<?> scorePullUserGameData(int pNumberOfGames);
	public ZombleCallback<?> scorePullGlobalGameData(int pNumberOfGames, String pMetric);

	/**
	 * facebook
	 */
	public ZombleCallback<?> userLoginWithFacebook();
	public ZombleCallback<?> userLinkWithFacebook();
	public ZombleCallback<?> userUnlinkWithFacebook();
}