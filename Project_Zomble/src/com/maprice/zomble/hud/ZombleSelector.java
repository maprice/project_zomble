package com.maprice.zomble.hud;

import org.andengine.entity.sprite.Sprite;
import org.andengine.opengl.texture.region.ITextureRegion;

import com.maprice.zomble.scene.ZombleSceneManager;
import com.maprice.zomble.util.contants.ZombleConstants;



/**
 * <Class Description>
 * 
 * @author maprice
 */


public class ZombleSelector extends Sprite implements ZombleConstants{
	public ZombleSelector(float pX, float pY, ITextureRegion pTextureRegion) {
		super(pX - pTextureRegion.getWidth()/2, pY- pTextureRegion.getHeight()/2, pTextureRegion, ZombleSceneManager.sharedInstance().getVBOmanager());

		this.setAlpha(HUD_ALPHA);
		this.setVisible(false);
	}

	// ===========================================================
	// Constants
	// ===========================================================

	static final String LOG_TAG = PREFIX + ZombleSelector.class.getSimpleName();

	// ===========================================================
	// Fields
	// ===========================================================

	// ===========================================================
	// Constructors
	// ===========================================================


	// ===========================================================
	// Methods
	// ===========================================================


}
