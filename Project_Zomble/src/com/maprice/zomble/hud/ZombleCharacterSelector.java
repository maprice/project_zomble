package com.maprice.zomble.hud;

import org.andengine.entity.sprite.AnimatedSprite;
import org.andengine.opengl.texture.region.ITiledTextureRegion;

import com.maprice.zomble.scene.ZombleSceneManager;
import com.maprice.zomble.util.contants.ZombleConstants;



/**
 * <Class Description>
 * 
 * @author maprice
 */


public class ZombleCharacterSelector extends AnimatedSprite implements ZombleConstants{


	// ===========================================================
	// Constants
	// ===========================================================

	static final String LOG_TAG = PREFIX + ZombleCharacterSelector.class.getSimpleName();

	// ===========================================================
	// Fields
	// ===========================================================

	// ===========================================================
	// Constructors
	// ===========================================================

	public ZombleCharacterSelector(float pX, float pY,
			ITiledTextureRegion pTiledTextureRegion) {
		super(pX - pTiledTextureRegion.getWidth()/2, pY - pTiledTextureRegion.getHeight()/2, pTiledTextureRegion, ZombleSceneManager.sharedInstance().getVBOmanager());


		this.setAlpha(HUD_ALPHA);
		this.setVisible(false);
	}

	// ===========================================================
	// Methods
	// ===========================================================

	public void setSelected() {
		if(!this.isAnimationRunning()){

			this.animate(75, false, new IAnimationListener(){
				@Override
				public void onAnimationStarted(AnimatedSprite pAnimatedSprite,
						int pInitialLoopCount) {
					ZombleCharacterSelector.this.setVisible(true);

				}

				@Override
				public void onAnimationFrameChanged(AnimatedSprite pAnimatedSprite,
						int pOldFrameIndex, int pNewFrameIndex) {
					// TODO Auto-generated method stub

				}

				@Override
				public void onAnimationLoopFinished(AnimatedSprite pAnimatedSprite,
						int pRemainingLoopCount, int pInitialLoopCount) {
					// TODO Auto-generated method stub

				}

				@Override
				public void onAnimationFinished(AnimatedSprite pAnimatedSprite) {

				}
			});
		}
	}

	public void deselect() {
		this.setVisible(false);
	}
}