package com.maprice.zomble.hud;

import org.andengine.engine.camera.hud.HUD;
import org.andengine.entity.sprite.ButtonSprite;
import org.andengine.entity.sprite.ButtonSprite.OnClickListener;
import org.andengine.opengl.font.Font;
import org.andengine.opengl.texture.region.TextureRegion;
import org.andengine.opengl.texture.region.TiledTextureRegion;

import com.maprice.zomble.scene.ZombleSceneManager;
import com.maprice.zomble.touch.ZombleButtonCallback;
import com.maprice.zomble.util.contants.ZombleConstants;



/**
 * Singleton class used to control all the HUD elements
 * 
 * @author maprice
 */

public class ZombleHUDManager implements ZombleConstants{
	// ===========================================================
	// Constants
	// ===========================================================

	static final String LOG_TAG = PREFIX + ZombleHUDManager.class.getSimpleName();

	// ===========================================================
	// Fields
	// ===========================================================

	private HUD mHUD;
	private ZombleHUDHealth mZombleHUDHealth1;
	private ZombleHUDHealth mZombleHUDHealth2;

	private ZombleHUDAbility mZombleHUDAbility;
	private ZombleButtonCallback mButtonCallback;
	private static final ZombleHUDManager sZombleHUDManager = new ZombleHUDManager();
	

	// ===========================================================
	// Constructors
	// ===========================================================

	public static ZombleHUDManager sharedHud() {
		return sZombleHUDManager;
	}

	// ===========================================================
	// Methods
	// ===========================================================

	/**
	 * Sets up HUD sprites
	 * @param pFont 
	 * 
	 * @param pFont
	 *            The font to use
	 */
	public void initZombleHUDHealth(Font pFont, TiledTextureRegion pHUDTextureRegion, TiledTextureRegion pCharacterPortraitTextureRegion, 
			TextureRegion pXPBarTextureRegion,
			TextureRegion pHungerBarTextureRegion,
			TextureRegion pHealthTextureRegion){
		mHUD = new HUD();
		mZombleHUDHealth1 = new ZombleHUDHealth(0, pFont, pHUDTextureRegion, pCharacterPortraitTextureRegion, pXPBarTextureRegion, pHungerBarTextureRegion, pHealthTextureRegion);
		mZombleHUDHealth2 = new ZombleHUDHealth(1, pFont, pHUDTextureRegion, pCharacterPortraitTextureRegion, pXPBarTextureRegion, pHungerBarTextureRegion, pHealthTextureRegion);

		mHUD.attachChild(mZombleHUDHealth1);
		mHUD.attachChild(mZombleHUDHealth2);

		ZombleSceneManager.sharedInstance().getCamera().setHUD(mHUD);
		mHUD.setTouchAreaBindingOnActionDownEnabled(true); 
		mHUD.setTouchAreaBindingOnActionMoveEnabled(true);
	}

	public void initZombleHUDHealthAbility(TextureRegion pHUDTextureRegion){
		mZombleHUDAbility = new ZombleHUDAbility(pHUDTextureRegion, mButtonCallback);
		mHUD.attachChild(mZombleHUDAbility);
		mHUD.registerTouchArea(mZombleHUDAbility.getButton());
	}

	public void initZombleHUD(ZombleButtonCallback pButtonCallback){
		mButtonCallback = pButtonCallback;
	}



	public ZombleHUDHealth getHUD(int pId){
		switch(pId){
		case 0:
			return mZombleHUDHealth1;
		case 1:
			return mZombleHUDHealth2;
		default:
			throw new RuntimeException("Invalid character ID: " + pId);
		}
	}

}