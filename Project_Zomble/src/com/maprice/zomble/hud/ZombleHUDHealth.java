package com.maprice.zomble.hud;

import org.andengine.entity.Entity;
import org.andengine.entity.IEntity;
import org.andengine.entity.modifier.IEntityModifier.IEntityModifierListener;
import org.andengine.entity.modifier.ScaleAtModifier;
import org.andengine.entity.primitive.Rectangle;
import org.andengine.entity.sprite.Sprite;
import org.andengine.entity.sprite.TiledSprite;
import org.andengine.entity.text.Text;
import org.andengine.entity.text.TextOptions;
import org.andengine.opengl.font.Font;
import org.andengine.opengl.texture.region.TextureRegion;
import org.andengine.opengl.texture.region.TiledTextureRegion;
import org.andengine.util.HorizontalAlign;
import org.andengine.util.modifier.IModifier;

import com.maprice.zomble.scene.ZombleSceneManager;
import com.maprice.zomble.util.contants.ZombleConstants;



/**
 * Health HUD element
 * 
 * @author maprice
 */

// TODO: going to change hud layout drastically
public class ZombleHUDHealth extends Entity implements ZombleConstants{
	// ===========================================================
	// Constants
	// ===========================================================

	static final String LOG_TAG = PREFIX + ZombleHUDHealth.class.getSimpleName();

	// ===========================================================
	// Fields
	// ===========================================================

	
	private TiledSprite mHUDSprite;
	private TiledSprite mCharacterPortraitSprite;
	
	private Sprite mXPBarSprite;
	private Sprite mHungerBarSprite;
	private Sprite mHealthBarSprite;
	
	private Rectangle mLeftBackgroundSprite;
	private Rectangle mRightBackgroundSprite;

	private Text mAmmoText;	
	
	// ===========================================================
	// Constructors
	// ===========================================================

	public ZombleHUDHealth(int pId, Font pFont, TiledTextureRegion pHUDTextureRegion, TiledTextureRegion pCharacterPortraitTextureRegion, 
			 TextureRegion pXPBarTextureRegion,
			 TextureRegion pHungerBarTextureRegion,
			 TextureRegion pHealthTextureRegion){
		
		mHUDSprite = new TiledSprite(0, 0, pHUDTextureRegion, ZombleSceneManager.sharedInstance().getVBOmanager());
		
		mAmmoText = new Text(HUD_AMMO_ANCHOR.x, HUD_AMMO_ANCHOR.y, pFont, "01234567890/", new TextOptions(HorizontalAlign.CENTER), ZombleSceneManager.sharedInstance().getVBOmanager());
		
		mLeftBackgroundSprite = new Rectangle(HUD_PORTRAIT_ANCHOR.x, HUD_PORTRAIT_ANCHOR.y, HUD_PORTRAIT_SIZE.x/2, HUD_PORTRAIT_SIZE.y, ZombleSceneManager.sharedInstance().getVBOmanager());
		mRightBackgroundSprite = new Rectangle(HUD_PORTRAIT_ANCHOR.x+HUD_PORTRAIT_SIZE.x/2, HUD_PORTRAIT_ANCHOR.y, HUD_PORTRAIT_SIZE.x/2, HUD_PORTRAIT_SIZE.y, ZombleSceneManager.sharedInstance().getVBOmanager());
		
		mLeftBackgroundSprite.setColor(0.3f, 0.3f, 0.3f);
		mRightBackgroundSprite.setColor(0.5f, 0.5f, 0.5f);
		
		mCharacterPortraitSprite = new TiledSprite(HUD_PORTRAIT_ANCHOR.x, HUD_PORTRAIT_ANCHOR.y, pCharacterPortraitTextureRegion, ZombleSceneManager.sharedInstance().getVBOmanager());
		mCharacterPortraitSprite.setCurrentTileIndex(pId);
		mXPBarSprite = new Sprite(HUD_XP_ANCHOR.x, HUD_XP_ANCHOR.y, pXPBarTextureRegion, ZombleSceneManager.sharedInstance().getVBOmanager());
		mHungerBarSprite = new Sprite(HUD_HUNGER_ANCHOR.x, HUD_HUNGER_ANCHOR.y, pHungerBarTextureRegion, ZombleSceneManager.sharedInstance().getVBOmanager());
		mHealthBarSprite = new Sprite(HUD_HEALTH_ANCHOR.x, HUD_HEALTH_ANCHOR.y, pHealthTextureRegion, ZombleSceneManager.sharedInstance().getVBOmanager());
			
		mHUDSprite.setScale(SCALE);
		mHUDSprite.setScaleCenter(0, 0);
		mHUDSprite.setPosition(5, 5);
		mHUDSprite.setZIndex(Layer.HUD.ordinal());

		
		
		this.attachChild(mHUDSprite);
		this.attachChild(mAmmoText);
		
		mHUDSprite.attachChild(mLeftBackgroundSprite);
		mHUDSprite.attachChild(mRightBackgroundSprite);
		mHUDSprite.attachChild(mCharacterPortraitSprite);
		mHUDSprite.attachChild(mXPBarSprite);
		mHUDSprite.attachChild(mHungerBarSprite);
		mHUDSprite.attachChild(mHealthBarSprite);
		
		mXPBarSprite.setHeight(-HUD_XP_SIZE.y);
		mHungerBarSprite.setWidth(HUD_HUNGER_SIZE.x);
		mHealthBarSprite.setWidth(HUD_HEALTH_SIZE.x);
		mAmmoText.setText("100/100");
		this.setX(pId*250);
	}

	// ===========================================================
	// Methods
	// ===========================================================

	/**
	 * Updates health to new value (visually)
	 * 
	 * @param pTotalHealth
	 *           Total health of entity
	 *            
	 * @param pCurrentHealth
	 *            Current health of the entity
	 */
	public void updateHealth(int pTotalHealth, int pCurrentHealth){
		mHealthBarSprite.setWidth((float)pCurrentHealth/pTotalHealth * HUD_HEALTH_SIZE.x);
	}

	public void updateHunger(int pTotalHealth, int pCurrentHealth){
		mHungerBarSprite.setWidth((float)pCurrentHealth/pTotalHealth * HUD_HUNGER_SIZE.x);
	}

	public void updateXP(int pTotalHealth, int pCurrentHealth){
		mXPBarSprite.setHeight((float)pCurrentHealth/pTotalHealth * -HUD_XP_SIZE.y);
	}
	 
	public void updateAmmo(int pTotalHealth, int pCurrentHealth){
		mAmmoText.setText(pCurrentHealth + "/" + pTotalHealth);
	}
	 
	
	public void startAbilityCooldown(final float pDuration){
		
		IEntityModifierListener lEntityModifierListener = new IEntityModifierListener(){

			@Override
			public void onModifierStarted(IModifier<IEntity> pModifier,
					IEntity pItem) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onModifierFinished(IModifier<IEntity> pModifier,
					IEntity pItem) {
				// TODO Auto-generated method stub
				
			}
			
		};
		
	
		mLeftBackgroundSprite.registerEntityModifier(new ScaleAtModifier(pDuration, 1.0f, 1.0f, 0.0f, 1.0f, 0.0f, mLeftBackgroundSprite.getHeight(), lEntityModifierListener));
	}
	
	public void startGunCooldown(final float pDuration){
		
		IEntityModifierListener lEntityModifierListener = new IEntityModifierListener(){

			@Override
			public void onModifierStarted(IModifier<IEntity> pModifier,
					IEntity pItem) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onModifierFinished(IModifier<IEntity> pModifier,
					IEntity pItem) {
				// TODO Auto-generated method stub
				
			}
			
		};
		
	
		mRightBackgroundSprite.registerEntityModifier(new ScaleAtModifier(pDuration, 1.0f, 1.0f, 0.0f, 1.0f, 0.0f, mLeftBackgroundSprite.getHeight(), lEntityModifierListener));
	}

	public void setSelected(boolean pSelected){
		if(pSelected){
			mHUDSprite.setCurrentTileIndex(0);
			mHUDSprite.setAlpha(0.8f);
			mLeftBackgroundSprite.setAlpha(0.8f);
			mRightBackgroundSprite.setAlpha(0.8f);
			mCharacterPortraitSprite.setAlpha(1.0f);
			mXPBarSprite.setAlpha(0.9f);
			mHungerBarSprite.setAlpha(0.9f);
			mHealthBarSprite.setAlpha(0.9f);
			mAmmoText.setAlpha(0.9f);
			
		}
		else{
			mHUDSprite.setCurrentTileIndex(1);
			mHUDSprite.setAlpha(0.4f);
			mLeftBackgroundSprite.setAlpha(0.4f);
			mRightBackgroundSprite.setAlpha(0.4f);
			mCharacterPortraitSprite.setAlpha(0.4f);
			mXPBarSprite.setAlpha(0.6f);
			mHungerBarSprite.setAlpha(0.6f);
			mHealthBarSprite.setAlpha(0.6f);
			mAmmoText.setAlpha(0.6f);
		}
	}


	public void updateCounter(){
		//mCounter++;
		//mCounterText.setText("Kills: "+mCounter);
	}
}