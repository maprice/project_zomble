package com.maprice.zomble.hud;

import org.andengine.entity.Entity;
import org.andengine.entity.IEntity;
import org.andengine.entity.modifier.IEntityModifier.IEntityModifierListener;
import org.andengine.entity.scene.IOnAreaTouchListener;
import org.andengine.entity.scene.ITouchArea;
import org.andengine.entity.sprite.ButtonSprite;
import org.andengine.entity.sprite.ButtonSprite.OnClickListener;
import org.andengine.entity.sprite.Sprite;
import org.andengine.input.touch.TouchEvent;
import org.andengine.opengl.texture.region.TextureRegion;
import org.andengine.util.modifier.IModifier;

import android.util.Log;

import com.maprice.zomble.scene.ZombleSceneManager;
import com.maprice.zomble.touch.ZombleButtonCallback;
import com.maprice.zomble.util.contants.ZombleConstants;



/**
 * Health HUD element
 * 
 * @author maprice
 */

// TODO: going to change hud layout drastically
public class ZombleHUDAbility extends Entity implements ZombleConstants{
	// ===========================================================
	// Constants
	// ===========================================================

	static final String LOG_TAG = PREFIX + ZombleHUDAbility.class.getSimpleName();

	// ===========================================================
	// Fields
	// ===========================================================

	ZombleButtonCallback mButtonCallback;
	private Sprite mHUDSprite;
	
	// ===========================================================
	// Constructors
	// ===========================================================

	public ZombleHUDAbility(TextureRegion pHUDTextureRegion, final ZombleButtonCallback pButtonCallback){
		mButtonCallback = pButtonCallback;
		mHUDSprite = new Sprite(0, 0, pHUDTextureRegion, ZombleSceneManager.sharedInstance().getVBOmanager())  {
	        public boolean onAreaTouched(TouchEvent touchEvent, float X, float Y)
	        {
	        
	            if (touchEvent.isActionUp())
	            {
	            	pButtonCallback.onButtonPress();
	            }
	            return true;
	        };
	    };;
		mHUDSprite.setScale(SCALE);
		mHUDSprite.setScaleCenter(0, 0);
		mHUDSprite.setPosition(0, ZombleSceneManager.sharedInstance().getCamera().getHeight() - mHUDSprite.getHeightScaled());
		mHUDSprite.setZIndex(Layer.HUD.ordinal());
		
		
		this.attachChild(mHUDSprite);
	}

	// ===========================================================
	// Methods
	// ===========================================================

	
	public void startAbilityCooldown(final float pDuration){
		
		IEntityModifierListener lEntityModifierListener = new IEntityModifierListener(){

			@Override
			public void onModifierStarted(IModifier<IEntity> pModifier,
					IEntity pItem) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onModifierFinished(IModifier<IEntity> pModifier,
					IEntity pItem) {
				// TODO Auto-generated method stub
				
			}
			
		};
		
	
		//mLeftBackgroundSprite.registerEntityModifier(new ScaleAtModifier(pDuration, 1.0f, 1.0f, 0.0f, 1.0f, 0.0f, mLeftBackgroundSprite.getHeight(), lEntityModifierListener));
	}
	
	public Sprite getButton(){
		return mHUDSprite;
	}


}