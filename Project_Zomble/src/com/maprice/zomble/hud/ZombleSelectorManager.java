package com.maprice.zomble.hud;

import org.andengine.opengl.texture.region.TiledTextureRegion;

import com.maprice.zomble.enemy.ZombleEnemy;
import com.maprice.zomble.enemy.ZombleEnemyEntity;
import com.maprice.zomble.util.contants.ZombleConstants;



/**
 * <Class Description>
 * 
 * @author maprice
 */


public class ZombleSelectorManager implements ZombleConstants{
	// ===========================================================
	// Constants
	// ===========================================================

	static final String LOG_TAG = PREFIX + ZombleSelectorManager.class.getSimpleName();

	// ===========================================================
	// Fields
	// ===========================================================

	private final ZombleSelector mZombleSelector;
	private ZombleEnemy mCurrentTarget;

	// ===========================================================
	// Constructors
	// ===========================================================

	public ZombleSelectorManager(TiledTextureRegion pSelectorTextureRegion) {
		mZombleSelector = new ZombleSelector(0, 0, pSelectorTextureRegion);
	}

	// ===========================================================
	// Methods
	// ===========================================================

	public void setTarget(ZombleEnemy pZomble) {
		mCurrentTarget = pZomble;

		if(mZombleSelector.hasParent()){
			mZombleSelector.detachSelf();
		}

		pZomble.getEntity().attachChild(mZombleSelector);
		mZombleSelector.setVisible(true);
	}

	public void clearTarget() {
		if(mZombleSelector.hasParent()){
			mZombleSelector.detachSelf();
		}
		mCurrentTarget = null;
		mZombleSelector.setVisible(false);
	}
	
	// TODO: more sophisticated check to see it zomble is still alive
	public ZombleEnemyEntity getTarget() {
		if(mZombleSelector.isVisible()){
			return mCurrentTarget.getEntity();
		}
		else{
			return null;
		}
	}

	public boolean hasTarget() {
		return(mZombleSelector.isVisible() && mCurrentTarget.getEntity().isVisible() && mCurrentTarget.getStats().getCurrentHealth() > 0);
	}
}