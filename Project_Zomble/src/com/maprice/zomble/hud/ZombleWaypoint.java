package com.maprice.zomble.hud;

import org.andengine.entity.sprite.AnimatedSprite;
import org.andengine.opengl.texture.region.ITiledTextureRegion;

import com.maprice.zomble.hero.ZombleHeroEntity.CharacterLayer;
import com.maprice.zomble.scene.ZombleSceneManager;
import com.maprice.zomble.util.contants.ZombleConstants;



/**
 * <Class Description>
 * 
 * @author maprice
 */


public class ZombleWaypoint extends AnimatedSprite implements ZombleConstants{


	// ===========================================================
	// Constants
	// ===========================================================

	static final String LOG_TAG = PREFIX + ZombleWaypoint.class.getSimpleName();

	// ===========================================================
	// Fields
	// ===========================================================

	// ===========================================================
	// Constructors
	// ===========================================================

	public ZombleWaypoint(float pX, float pY, ITiledTextureRegion pTiledTextureRegion) {
		super(pX - pTiledTextureRegion.getWidth()/2, pY - pTiledTextureRegion.getHeight()/2, pTiledTextureRegion, ZombleSceneManager.sharedInstance().getVBOmanager());
		this.setZIndex(CharacterLayer.FRONT.index());
		this.setScale(SCALE);
		this.setAlpha(HUD_ALPHA);
		this.setVisible(false);

		ZombleSceneManager.sharedInstance().getCurrentScene().attachChild(this);
	}

	// ===========================================================
	// Methods
	// ===========================================================

	public void setWaypoint(float pX, float pY) {
		if(!this.isAnimationRunning()){
			this.setPosition(pX, pY);


			this.animate(50, false, new IAnimationListener(){

				@Override
				public void onAnimationStarted(AnimatedSprite pAnimatedSprite,
						int pInitialLoopCount) {
					ZombleWaypoint.this.setVisible(true);

				}

				@Override
				public void onAnimationFrameChanged(AnimatedSprite pAnimatedSprite,
						int pOldFrameIndex, int pNewFrameIndex) {
					// TODO Auto-generated method stub

				}

				@Override
				public void onAnimationLoopFinished(AnimatedSprite pAnimatedSprite,
						int pRemainingLoopCount, int pInitialLoopCount) {
					// TODO Auto-generated method stub

				}

				@Override
				public void onAnimationFinished(AnimatedSprite pAnimatedSprite) {
					ZombleWaypoint.this.setVisible(false);

				}

			});
		}
	}
}