package com.maprice.zomble.util.contants;

import java.util.Random;

import org.andengine.entity.Entity;


/**
 * General utility functions
 * 
 * @author mprice
 */

public class ZombleUtil {
	
	/**
	 * Converts planar coordinates to an angle in radians
	 * 
	 * @param pValueX
	 *            x value of the analog control
	 *            
	 * @param pValueY
	 *            y value of the analog control
	 *            
	 * @return
	 *           angle in radians [0,2PI]
	 */
	public static double getAngle(float pValueX, float pValueY) {

		// Gets the angle in radians
		double inRads = Math.atan2(pValueY,pValueX);

		// Maps the coordinate system to [0, 2PI]
		inRads = (inRads < 0) ? Math.abs(inRads) : 2*Math.PI - inRads;

		return inRads;
	}
	
	public static boolean collidesWith(Entity pPointA, Entity pPointB, float pPlusMinus) {
		return (pPointA.getX() >= pPointB.getX() - pPlusMinus && 
				pPointA.getX() <= pPointB.getX() + pPlusMinus &&
				pPointA.getY() >= pPointB.getY() - pPlusMinus &&
				pPointA.getY() <= pPointB.getY() + pPlusMinus);
	}

	public static boolean collidesWith(Entity pPointA, float pPointX, float pPointBY, float pBuffer) {
		return (pPointA.getX() >= pPointX - pBuffer && 
				pPointA.getX() <= pPointX + pBuffer &&
				pPointA.getY() >= pPointBY - pBuffer &&
				pPointA.getY() <= pPointBY + pBuffer);
	}
	
	public static boolean collidesWith(float pPointAX, float pPointAY, float pPointBX, float pPointBY, int pBuffer) {
		return (pPointAX >= pPointBX - pBuffer && 
				pPointAX <= pPointBX + pBuffer &&
				pPointAY >= pPointBY - pBuffer &&
				pPointAY <= pPointBY + pBuffer);
	}

	/**
	 * Returns random integer in the range of [start, end]
	 *            
	 * @param pStart
	 *            Lower bound
	 *            
	 * @param pEnd
	 *            Upper bound           
	 */
	public static int getRandom(int pStart, int pEnd){
		Random lRandom = new Random();
		return lRandom.nextInt(pEnd-pStart) + pStart;
	}
}