package com.maprice.zomble.util.contants;

import org.andengine.util.color.Color;

import android.graphics.Point;

/**
 * Constants
 * 
 * @author maprice
 */

public interface ZombleConstants {

	// ===========================================================
	// Global
	// ===========================================================

	public static final String PREFIX = "Zomble";

	public static final boolean DEBUG = false;

	public static final int CAMERA_WIDTH = 800;
	public static final int CAMERA_HEIGHT = 480;

	public static final float SCALE = 3f;
	public static final float GRAVITY = 981;

	public enum Layer {
		BACKGROUND,
		FOREGROUND,
		BACK_EFFECTS,
		SPRITES,
		FRONT_EFFECTS,
		LIGHT,
		HUD;
	}

	// ===========================================================
	// Hero
	// ===========================================================

	public static final int HERO_WALK_SPEED_MULTIPLIER = 100;
	public static final int HERO_FRAME_SPEED = 120;
	
	public static final int HERO_SPRITE_SHEET_COL = 7;
	public static final int HERO_SPRITE_SHEET_ROW = 8;

	// ===========================================================
	// Gun
	// ===========================================================

	public static final int ITEM_FRAME_BEHIND = 2;
	public static final int ITEM_FRAME_END = 9;

	public static final int ZITEM_FRAME_BEHIND = 2;
	public static final int ZITEM_FRAME_END = 7;

	public static final int ITEM_SPRITE_SHEET_COL = 7;
	public static final int ITEM_SPRITE_SHEET_ROW = 8;

	public static final int ITEM_X_OFFSET = 0; 
	public static final int ITEM_Y_OFFSET = 2;

	public static final int ITEM_X_OFFSET_MULTIPLIER = 7;
	public static final int ITEM_Y_OFFSET_MULTIPLIER = 7;


	public static final float GUN_RECOIL_VELOCITY = 100;
	public static final float GUN_RECOIL_ACCEL = 1000;

	public static final float GUN_RECOIL_ANGULAR_VELOCITY = 200;
	public static final float GUN_RECOIL_ANGULAR_ACCEL = 1000;


	// ===========================================================
	// Arm
	// ===========================================================

	public static final int ARM_ITEM_X_OFFSET = 0; 
	public static final int ARM_ITEM_Y_OFFSET = 2;

	public static final int ARM_ITEM_X_OFFSET_MULTIPLIER = 5;
	public static final int ARM_ITEM_Y_OFFSET_MULTIPLIER = 2;

	// ===========================================================
	// Bullets
	// ===========================================================

	public static final float SHOTGUN_BASE_VELOCITY = 700;
	public static final float MACHINEGUN_BASE_VELOCITY = 1000;

	public static final float SHOTGUN_BASE_SPREAD_VELOCITY = 200;

	public static final float SHOTGUN_BASE_SPREAD = 0.1f;
	public static final float MACHINEGUN_BASE_SPREAD = 0.1f;

	public static final float MACHINEGUN_BULLET_WIDTH = 15f;
	public static final float MACHINEGUN_BULLET_HEIGHT = 1f;
	public static final Color MACHINEGUN_BULLET_COLOR = new Color(1f,1f,1f,1);

	// ===========================================================
	// Casings
	// ===========================================================

	public static final float CASING_ELASTICITY = 0.6f;

	public static final int CASING_RANDOM_VELOCITY_X_RANGE = 40;
	public static final int CASING_RANDOM_VELOCITY_Y_RANGE = 50;

	public static final int CASING_BASE_VELOCITY_X = 20;
	public static final int CASING_BASE_VELOCITY_Y = 90;

	public static final int CASING_BASE_GROUND = 30;
	public static final float CASING_RANDOM_GROUND_RANGE = 20;

	// ===========================================================
	// Blood
	// ===========================================================

	public static final int BLOOD_PARTICLE_BASE = 3;
	public static final int BLOOD_PARTICLE_OFFSET = 3;
	public static final float BLOOD_PARTICLE_DURATION = 7;
	public static final float BLOOD_PARTICLE_SIZE = 3;


	// ===========================================================
	// HUD
	// ===========================================================

	public static final int HUD_HEALTH_BAR_SIZE = 39;
	public static final float HUD_ALPHA = 0.7f;

	public static final float HUD_ENEMY_HEALTH_BAR_WIDTH = 13;
	public static final float HUD_ENEMY_HEALTH_BAR_HEIGHT = 3;
	public static final long HUD_ENEMY_HEALTH_BAR_UPTIME = 2000;
	
	public static final Point HUD_PORTRAIT_ANCHOR = new Point(7,2);
	public static final Point HUD_XP_ANCHOR = new Point(2,24);
	public static final Point HUD_HUNGER_ANCHOR = new Point(34,11);
	public static final Point HUD_HEALTH_ANCHOR = new Point(34,2);
	public static final Point HUD_AMMO_ANCHOR = new Point(107,58);
	
	public static final Point HUD_PORTRAIT_SIZE = new Point(22,22);
	public static final Point HUD_XP_SIZE = new Point(4,22);
	public static final Point HUD_HUNGER_SIZE = new Point(37,4);
	public static final Point HUD_HEALTH_SIZE = new Point(37,4);
	

	// ===========================================================
	// Touch
	// ===========================================================

	public static final float TOUCH_HITBOX_SCALE_X = 2.4f;
	public static final float TOUCH_HITBOX_SCALE_Y = 1.2f;
	public static final float TOUCH_SPACING = 50f;

	// ===========================================================
	// Orbs
	// ===========================================================

	public static final long ORB_ANIMATION_FRAME_RATE = 50;

	// ===========================================================
	// Enemy
	// ===========================================================

	public static final long ENEMY_ATTACK_COOLDOWN = 1000;
	public static final long ENEMY_PATH_POLL_COOLDOWN = 1000;

	public static final int ENEMY_WALK_SPEED_MULTIPLIER = 60;
	public static final int ENEMY_FRAME_SPEED = 200;
	
	public static final int ENEMY_DEAD_SPRITE_SHEET_COL = 5;
	public static final int ENEMY_DEAD_SPRITE_SHEET_ROW = 8;

	// ===========================================================
	// Map
	// ===========================================================

	public static final long MAP_LOAD_COOLDOWN = 1000;
	
	// ===========================================================
	// Menu
	// ===========================================================

	public static final float MENU_BUTTON_BUFFER = 5.0f;
	
}