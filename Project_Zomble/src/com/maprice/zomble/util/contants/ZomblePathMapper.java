package com.maprice.zomble.util.contants;

import java.util.ArrayList;

import org.andengine.entity.primitive.Line;
import org.andengine.util.algorithm.path.Path;
import org.andengine.util.color.Color;

import com.maprice.zomble.scene.ZombleSceneManager;
import com.maprice.zomble.util.contants.ZombleConstants.Layer;

/**
 * Used for debugging pathing
 * 
 * @author maprice
 */


public class ZomblePathMapper {

	// ===========================================================
	// Fields
	// ===========================================================

	private final ArrayList<Line> mLineList = new ArrayList<Line>();
	private final Color mColor;

	// ===========================================================
	// Constructors
	// ===========================================================


	public ZomblePathMapper(Color pColor){
		mColor = pColor;
	}

	// ===========================================================
	// Methods
	// ===========================================================

	public void drawPath(final Path pCurrentPath) {
		ZombleSceneManager.sharedInstance().getEngine().runOnUpdateThread(new Runnable(){
			@Override
			public void run() {

				for(Line lLine : mLineList){
					lLine.setVisible(false);
					lLine.detachSelf();
				}

				mLineList.clear();

				for(int i = 0; i < pCurrentPath.getLength() - 1; i++){

					Line lLine = new Line(pCurrentPath.getX(i), 
							pCurrentPath.getY(i), 
							pCurrentPath.getX(i+1), 
							pCurrentPath.getY(i+1), 
							1f,ZombleSceneManager.sharedInstance().getVBOmanager());
					lLine.setColor(mColor);
					lLine.setZIndex(Layer.BACK_EFFECTS.ordinal());
					mLineList.add(lLine);

					ZombleSceneManager.sharedInstance().getCurrentScene().attachChild(lLine);
				}
			}
		});
	}

	public void clearPath() {
		ZombleSceneManager.sharedInstance().getEngine().runOnUpdateThread(new Runnable(){
			@Override
			public void run() {
				for(Line lLine : mLineList){
					lLine.setVisible(false);
					lLine.detachSelf();
				}

				mLineList.clear();
			}
		});
	}
}