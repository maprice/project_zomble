package com.maprice.zomble.util.contants;

/**
 * General call-back interface commonly used to signal completion from Entity to Pool
 * 
 * @author maprice
 */

public interface ZombleCallback <Result>{
	/**
	 * Called after action has successfully completed
	 * 
	 * @param pResult
	 *            The class that calls onComplete
	 */
	public void onComplete(Result pResult);

	/**
	 * Called if something goes wrong
	 *            
	 * @param pErrorMessage
	 *            A message describing the error
	 */
	public void onError(String pErrorMessage);

}