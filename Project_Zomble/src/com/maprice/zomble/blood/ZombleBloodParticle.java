package com.maprice.zomble.blood;

import java.util.Random;

import org.andengine.engine.handler.physics.PhysicsHandler;
import org.andengine.entity.IEntity;
import org.andengine.entity.primitive.Rectangle;

import com.maprice.zomble.scene.ZombleSceneManager;
import com.maprice.zomble.util.contants.ZombleConstants;


/**
 * Single particle of blood
 * 
 * @author maprice
 */

public class ZombleBloodParticle extends Rectangle implements ZombleConstants{
	// ===========================================================
	// Constants
	// ===========================================================

	static final String LOG_TAG = PREFIX + ZombleBloodParticle.class.getSimpleName();

	// ===========================================================
	// Fields
	// ===========================================================

	private float mGround;
	
	final private PhysicsHandler mPhysicsHandler  = new PhysicsHandler(this){
		@Override
		protected void onUpdate(final float pSecondsElapsed, final IEntity pEntity) {
			super.onUpdate(pSecondsElapsed,pEntity);

			// When the particle reaches the ground
			if(pEntity.getY() > mGround && this.getVelocityY() > 0){

				// Stop all motion and remove the handler
				mPhysicsHandler.setVelocity(0, 0);
				mPhysicsHandler.setAcceleration(0, 0);
				pEntity.clearUpdateHandlers();

				// Draw the particle BEHIND the character
				ZombleBloodParticle.this.setZIndex(Layer.BACK_EFFECTS.ordinal());
				ZombleBloodParticle.this.getParent().sortChildren();
			}
		}
	};

	// ===========================================================
	// Constructors
	// ===========================================================


	public ZombleBloodParticle(float pWidth, float pHeight) {
		super(0, 0, pWidth, pHeight, ZombleSceneManager.sharedInstance().getVBOmanager());

		// Set layer
		this.setZIndex(Layer.FRONT_EFFECTS.ordinal());
	}

	// ===========================================================
	// Methods
	// ===========================================================


	/**
	 * Starts the motion of the particle
	 * 
	 * @param pX
	 *            starting x position of the particle
	 *          
	 * @param pY
	 *            starting y position of the particle
	 *          
	 * @param pAngle
	 *            angle the particle will travel initially (Radians [0 , 2PI])
	 *            
	 * @param pGround
	 *            the 'ground' position with respect to the affected sprite
	 *            
	 */
	public void start(float pX, float pY, double pAngle, float pGround) {
		this.setX(pX);
		this.setY(pY);

		Random rand = new Random();
		float groundYOffset = rand.nextInt((int) (CASING_RANDOM_GROUND_RANGE)) + pGround-pY;


		// Set the lowest value for the particle
		mGround = pY + groundYOffset;

		// Create random offsets for both the x and y velocity of the particle
		int xVelocity = rand.nextInt((int) CASING_RANDOM_VELOCITY_X_RANGE) + 100;
		int yVelocity = rand.nextInt((int) CASING_RANDOM_VELOCITY_Y_RANGE) + 100;

		double x = Math.cos(pAngle);
		double y = Math.sin(-pAngle);

		mPhysicsHandler.setVelocity((float)x*xVelocity,(float)y*yVelocity);

		// Constant downward acceleration
		mPhysicsHandler.setAccelerationY(GRAVITY);
	}
	
	public void resetInteractions(){
		this.setAlpha(1.0f);
		this.setVisible(true);
		this.registerUpdateHandler(mPhysicsHandler);
		this.setZIndex(Layer.FRONT_EFFECTS.ordinal());
	}
}