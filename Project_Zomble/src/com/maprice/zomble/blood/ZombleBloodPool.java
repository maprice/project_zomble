package com.maprice.zomble.blood;

import org.andengine.util.adt.pool.GenericPool;

import com.maprice.zomble.scene.ZombleSceneManager;
import com.maprice.zomble.util.contants.ZombleConstants;

/**
 * Pool system that controls the allocation and deletion of particles
 * 
 * @author maprice
 */


public class ZombleBloodPool extends GenericPool<ZombleBloodParticle> implements ZombleConstants{
	// ===========================================================
	// Constants
	// ===========================================================

	static final String LOG_TAG = PREFIX + ZombleBloodPool.class.getSimpleName();
	
	// ===========================================================
	// Fields
	// ===========================================================

	// ===========================================================
	// Constructors
	// ===========================================================

	public ZombleBloodPool(){
		// Nothing to do here
	}

	// ===========================================================
	// Methods
	// ===========================================================


	@Override
	protected ZombleBloodParticle onAllocatePoolItem() {
		return new ZombleBloodParticle(BLOOD_PARTICLE_SIZE, BLOOD_PARTICLE_SIZE);
	}

	/**
	 * Called when a particle is sent to the pool
	 */
	@Override
	protected void onHandleRecycleItem(final ZombleBloodParticle pParticle) {
		ZombleSceneManager.sharedInstance().getEngine().runOnUpdateThread(new Runnable(){
			@Override
			public void run() {
				pParticle.setIgnoreUpdate(true);
				pParticle.clearUpdateHandlers();
				pParticle.clearEntityModifiers();
				pParticle.setVisible(false);
			}
		});
	}

	/**
	 * Called just before a particle is returned to the caller, this is where you write your initialize code
	 * i.e. set location, rotation, etc.
	 */
	@Override
	protected void onHandleObtainItem(final ZombleBloodParticle pParticle) {
		pParticle.setIgnoreUpdate(false);
		pParticle.resetInteractions();
	}
}

