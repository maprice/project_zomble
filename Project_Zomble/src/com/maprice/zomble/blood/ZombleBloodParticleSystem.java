package com.maprice.zomble.blood;

import java.util.Random;

import org.andengine.entity.Entity;
import org.andengine.entity.IEntity;
import org.andengine.entity.modifier.AlphaModifier;
import org.andengine.entity.modifier.ColorModifier;
import org.andengine.entity.modifier.DelayModifier;
import org.andengine.util.modifier.ease.EaseExponentialIn;

import com.maprice.zomble.scene.ZombleSceneManager;
import com.maprice.zomble.util.contants.ZombleConstants;

/**
 * Particle system that controls the movement of particles
 * 
 * @author maprice
 */


public class ZombleBloodParticleSystem extends Entity implements ZombleConstants{

	// ===========================================================
	// Fields
	// ===========================================================

	final ZombleBloodPool mPool;

	// ===========================================================
	// Constructors
	// ===========================================================

	public ZombleBloodParticleSystem() {
		mPool = new ZombleBloodPool();
	}

	// ===========================================================
	// Methods
	// ===========================================================



	/**
	 * Allocates a random number of blood particles and causes them to disperse in a certain direction whilst being effected by gravity
	 * 
	 * @param pX
	 *            starting x position of the particle system
	 *          
	 * @param pY
	 *            starting y position of the particle system
	 *          
	 * @param pAngle
	 *            angle the particle system will travel initially (Radians [0 , 2PI])
	 *            
	 * @param pGround
	 *            the 'ground' position with respect to the affected sprite
	 *            
	 */
	public void splatter(float pX, float pY, double pAngle, float pGround){

		// Create a random number of blood particles
		Random rNum = new Random();
		final ZombleBloodParticle[] bloodParticle = new ZombleBloodParticle[rNum.nextInt(BLOOD_PARTICLE_OFFSET) + BLOOD_PARTICLE_BASE];

		for(int i = 0; i < bloodParticle.length; i++){

			// Obtain a particle from the pool
			bloodParticle[i] = mPool.obtainPoolItem();

			// Fade blood out exponentially
			bloodParticle[i].registerEntityModifier(new AlphaModifier(BLOOD_PARTICLE_DURATION * 0.9f, 1, 0, EaseExponentialIn.getInstance()));


			// Color modifier to turn the blood color from red to dark red/brown
			bloodParticle[i].registerEntityModifier(new ColorModifier(BLOOD_PARTICLE_DURATION * 0.4f, 1f, 0.5f, 0f, 0.1f, 0f, 0.1f));

			// Delay modifier used to call reycle after a set time
			bloodParticle[i].registerEntityModifier(new DelayModifier(BLOOD_PARTICLE_DURATION){

				@Override
				public void onModifierFinished(IEntity pItem) {
					mPool.recyclePoolItem((ZombleBloodParticle) pItem);

				}
			});


			// If already has a parent (has already been created) then attach to scene
			// NOTE: We most likely want to ALWAYS attach to scene and have the particle be detached onRecycle
			if(!bloodParticle[i].hasParent()){
				ZombleSceneManager.sharedInstance().getCurrentScene().attachChild(bloodParticle[i]);
			}

			// Start the motion of the particle
			bloodParticle[i].start(pX, pY, pAngle, pGround);
		}
	}
}
