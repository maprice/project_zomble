package com.maprice.zomble.blood;

import com.maprice.zomble.util.contants.ZombleConstants;


/**
 * Manages the all activity with respect to blood
 * 
 * @author maprice
 */

public class ZombleBloodManager implements ZombleConstants{
	// ===========================================================
	// Constants
	// ===========================================================

	static final String LOG_TAG = PREFIX + ZombleBloodManager.class.getSimpleName();

	// ===========================================================
	// Fields
	// ===========================================================

	final ZombleBloodParticleSystem mBloodParticleSystem;

	// ===========================================================
	// Constructors
	// ===========================================================

	public ZombleBloodManager(){
		mBloodParticleSystem = new ZombleBloodParticleSystem();
	}

	// ===========================================================
	// Methods
	// ===========================================================


	public void splatter(float pX, float pY, double pBloodDirection,
			float pGround) {
		mBloodParticleSystem.splatter(pX, pY, pBloodDirection, pGround);
	}

}
