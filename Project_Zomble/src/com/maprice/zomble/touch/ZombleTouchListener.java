package com.maprice.zomble.touch;

import org.andengine.entity.scene.IOnSceneTouchListener;
import org.andengine.entity.scene.Scene;
import org.andengine.input.touch.TouchEvent;
import org.andengine.input.touch.detector.ScrollDetector;
import org.andengine.input.touch.detector.SurfaceScrollDetector;
import org.andengine.input.touch.detector.ScrollDetector.IScrollDetectorListener;

import android.os.SystemClock;
import android.util.Log;

import com.maprice.zomble.enemy.ZombleEnemy;
import com.maprice.zomble.enemy.ZombleEnemyManager;
import com.maprice.zomble.hero.ZombleHero;
import com.maprice.zomble.hero.ZombleHeroManager;
import com.maprice.zomble.scene.ZombleMenuScene;
import com.maprice.zomble.scene.ZombleSceneManager;
import com.maprice.zomble.util.contants.ZombleConstants;

public class ZombleTouchListener implements ZombleConstants, IOnSceneTouchListener, IScrollDetectorListener{
	// ===========================================================
	// Constants
	// ===========================================================

	static final String LOG_TAG = PREFIX + ZombleMenuScene.class.getSimpleName();

	// ===========================================================
	// Fields
	// ===========================================================

	private ZombleHeroManager mCharacterManager;
	private ZombleEnemyManager mEnemyManager;
	private SurfaceScrollDetector mScrollDetector;
	private Boolean mScrolling = false;

	// ===========================================================
	// Constructors
	// ===========================================================


	public ZombleTouchListener(ZombleHeroManager pCharacterManager, ZombleEnemyManager pEnemyManager){
		mCharacterManager = pCharacterManager;
		mEnemyManager = pEnemyManager;	
		this.mScrollDetector = new SurfaceScrollDetector(this);
		this.mScrollDetector.setTriggerScrollMinimumDistance(80.0f);
		this.mScrollDetector.setEnabled(true);
	}

	// ===========================================================
	// Methods
	// ===========================================================

	boolean firstTap = true;
	long thisTime;
	long prevTime;

	private boolean isDoubleTap(){
		if(firstTap){
			thisTime = SystemClock.uptimeMillis();
			firstTap = false;
		}else{
			prevTime = thisTime;
			thisTime = SystemClock.uptimeMillis();

			//Check that thisTime is greater than prevTime
			//just incase system clock reset to zero
			if(thisTime > prevTime){

				//Check if times are within our max delay
				if((thisTime - prevTime) <= 1000){

					//We have detected a double tap!
					Log.e(LOG_TAG, "DOUBLE!");
					//PUT YOUR LOGIC HERE!!!!
					firstTap = true;
					return true;
				}else{
					//Otherwise Reset firstTap
					firstTap = true;
				}
			}else{
				firstTap = true;
			}
		}
		return false;
	}

	
	@Override
	public boolean onSceneTouchEvent(Scene pScene, final TouchEvent pSceneTouchEvent) {
		this.mScrollDetector.onTouchEvent(pSceneTouchEvent);

		
		
		
		
		
		
		
		
		
		
		
		switch(pSceneTouchEvent.getAction()){
		case TouchEvent.ACTION_UP:
			if(!mScrolling){
				// Touched a character
				for(ZombleHero character : mCharacterManager.getCharacters()){
					if(character.getEntity().getTouchHitbox().contains(pSceneTouchEvent.getX(), pSceneTouchEvent.getY())){
						mCharacterManager.setSelectedCharacter(character);
						return true;
					}
				}

				// Touched a zomble
				for(ZombleEnemy zomble : mEnemyManager.getActiveEnemys()){
					if(zomble.getEntity().getTouchHitbox().contains(pSceneTouchEvent.getX(), pSceneTouchEvent.getY())){
						
						
						if(isDoubleTap()){
							mCharacterManager.useAbility();
						}
						
						mCharacterManager.setTarget(zomble);
						mCharacterManager.removeWaypoint();
						return true;
					}
				}

				// Touched map
				ZombleSceneManager.sharedInstance().getEngine().runOnUpdateThread(new Runnable(){
					@Override
					public void run(){
						mCharacterManager.setWaypoint(pSceneTouchEvent.getX(), pSceneTouchEvent.getY());
					}
				});
				return true;
			}
			else{
				mScrolling = false;
			}

		}
		return false;
	}



	@Override
	public void onScrollStarted(ScrollDetector pScollDetector, int pPointerID,
			float pDistanceX, float pDistanceY) {
		mScrolling = true;
		if(Math.abs(pDistanceX) > Math.abs(pDistanceY)){
			// Scroll right
			if(pDistanceX > 0){
				mCharacterManager.cycleRight();
			}
			// Scroll left
			else{
				mCharacterManager.cycleLeft();
			}
		}
		else{
			// Scroll down
			if(pDistanceY > 0){
				// Nothing to do here
			}
			// Scroll up
			else{
				mCharacterManager.selectAll();
			}
		}
	}

	@Override
	public void onScroll(ScrollDetector pScollDetector, int pPointerID,
			float pDistanceX, float pDistanceY) {
		// Nothing to do here

	}

	@Override
	public void onScrollFinished(ScrollDetector pScollDetector, int pPointerID,
			float pDistanceX, float pDistanceY) {
		// Nothing to do here
	}
}