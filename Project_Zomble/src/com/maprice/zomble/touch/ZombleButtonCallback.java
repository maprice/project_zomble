package com.maprice.zomble.touch;


public interface ZombleButtonCallback{
	/**
	 * Called after action has successfully completed
	 * 
	 * @param pResult
	 *            The class that calls onComplete
	 */
	public void onButtonPress();
}