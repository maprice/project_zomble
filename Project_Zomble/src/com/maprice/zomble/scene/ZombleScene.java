package com.maprice.zomble.scene;

import org.andengine.engine.Engine;
import org.andengine.engine.camera.BoundCamera;
import org.andengine.entity.scene.Scene;
import org.andengine.ui.activity.BaseGameActivity;

/**
 * General scene protocol
 * 
 * @author maprice
 */


public abstract class ZombleScene extends Scene{
	
	protected BaseGameActivity mActivity;
	protected Engine mEngine;
	protected BoundCamera mCamera;
	
	public ZombleScene(BaseGameActivity pActivity, Engine pEngine, BoundCamera pCamera) {
		this.mActivity = pActivity;
		this.mEngine = pEngine;
		this.mCamera = pCamera;
	}
	
	
	public abstract void initScene();
	public abstract void loadResources();
	public abstract void unloadResources();
}
