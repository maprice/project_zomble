package com.maprice.zomble.scene;

import java.io.IOException;

import org.andengine.audio.music.Music;
import org.andengine.audio.music.MusicFactory;
import org.andengine.audio.sound.Sound;
import org.andengine.audio.sound.SoundFactory;
import org.andengine.engine.Engine;
import org.andengine.engine.camera.BoundCamera;
import org.andengine.entity.Entity;
import org.andengine.input.touch.detector.SurfaceScrollDetector;
import org.andengine.opengl.font.Font;
import org.andengine.opengl.font.FontFactory;
import org.andengine.opengl.texture.ITexture;
import org.andengine.opengl.texture.TextureOptions;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlas;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlasTextureRegionFactory;
import org.andengine.opengl.texture.atlas.bitmap.BuildableBitmapTextureAtlas;
import org.andengine.opengl.texture.atlas.bitmap.source.IBitmapTextureAtlasSource;
import org.andengine.opengl.texture.atlas.buildable.builder.BlackPawnTextureAtlasBuilder;
import org.andengine.opengl.texture.atlas.buildable.builder.ITextureAtlasBuilder.TextureAtlasBuilderException;
import org.andengine.opengl.texture.region.TextureRegion;
import org.andengine.opengl.texture.region.TiledTextureRegion;
import org.andengine.ui.activity.BaseGameActivity;
import org.andengine.util.color.Color;
import org.andengine.util.debug.Debug;

import android.util.Log;

import com.maprice.zomble.ZombleSoundManager;
import com.maprice.zomble.blood.ZombleBloodManager;
import com.maprice.zomble.bullet.ZombleBulletManager;
import com.maprice.zomble.casing.ZombleCasingManager;
import com.maprice.zomble.enemy.ZombleEnemyManager;
import com.maprice.zomble.hero.ZombleHero;
import com.maprice.zomble.hero.ZombleHeroManager;
import com.maprice.zomble.hud.ZombleHUDManager;
import com.maprice.zomble.items.ZombleItemManager;
import com.maprice.zomble.map.ZombleMapManager;
import com.maprice.zomble.muzzleflash.ZombleMuzzleFlashManager;
import com.maprice.zomble.numbers.ZombleNumberManager;
import com.maprice.zomble.orbs.ZombleOrbsManager;
import com.maprice.zomble.projectile.ZombleCollisionManager;
import com.maprice.zomble.projectile.ZombleProjectileManager;
import com.maprice.zomble.touch.ZombleButtonCallback;
import com.maprice.zomble.touch.ZombleTouchListener;
import com.maprice.zomble.util.contants.ZombleConstants;

/**
 * Main game scene
 * 
 * @author maprice
 */

public class ZombleGameScene extends ZombleScene implements ZombleConstants{
	// ===========================================================
	// Constants
	// ===========================================================

	static final String LOG_TAG = PREFIX + ZombleGameScene.class.getSimpleName();

	// ===========================================================
	// Fields
	// ===========================================================

	private BuildableBitmapTextureAtlas mBitmapTextureAtlas;

	private TiledTextureRegion mCharacterTextureRegion; 
	private TiledTextureRegion mCharacter2TextureRegion;
	private TiledTextureRegion mZombleTextureRegion; 
	private TiledTextureRegion mZombleArmTextureRegion;
	private TiledTextureRegion mZombleDeadTextureRegion;
	private TiledTextureRegion mItemTextureRegion;
	private TiledTextureRegion mCasingTextureRegion;
	private TiledTextureRegion mMuzzleFlashTextureRegion;
	private TiledTextureRegion mWaypointTextureRegion; 
	private TiledTextureRegion mSelectorTextureRegion; 
	private TiledTextureRegion mCharacterSelectorTextureRegion; 
	private TiledTextureRegion mOrbTextureRegion; 
	private TiledTextureRegion mGroundItemTextureRegion; 

	// Hud
	private TiledTextureRegion mHUDTextureRegion;
	private TiledTextureRegion mCharacterPortraitTextureRegion;
	private TextureRegion mXPBarTextureRegion;
	private TextureRegion mHungerBarTextureRegion;
	private TextureRegion mHealthTextureRegion;
	private ITexture m8BitFontTexture;
	
	private TextureRegion mAbilityTextureRegion;

	private Font m8BitFont;
	private Sound mGunShotSound;
	private Sound mCasingSound;
	private Music mMusic;


	private ZombleHeroManager mCharacterManager;
	private ZombleProjectileManager mProjectileManager;
	private ZombleCollisionManager mCollisionManager;
	private ZombleEnemyManager mEnemyManager;

	// ===========================================================
	// Constructors
	// ===========================================================

	public ZombleGameScene(BaseGameActivity pActivity, Engine pEngine, BoundCamera pCamera) {
		super(pActivity, pEngine, pCamera); 
	}

	// ===========================================================
	// Methods
	// ===========================================================

	@Override
	public void initScene(){
		final Entity lSpriteLayer = new Entity();

		// Init managers
		ZombleSoundManager.sharedInstance().loadSounds(mCasingSound, mGunShotSound, mMusic);


		ZombleHUDManager.sharedHud().initZombleHUD(new ZombleButtonCallback(){

			@Override
			public void onButtonPress() {
				Log.e("sdf", "Use dat shit");
				mCharacterManager.useAbility();
				
			}
			
			
			
			
		});
		ZombleHUDManager.sharedHud().initZombleHUDHealth(m8BitFont, mHUDTextureRegion, mCharacterPortraitTextureRegion, mXPBarTextureRegion, mHungerBarTextureRegion, mHealthTextureRegion);
		ZombleHUDManager.sharedHud().initZombleHUDHealthAbility(mAbilityTextureRegion);
		ZombleMapManager.sharedInstance().initMapManager();

		// Setup local managers
		final ZombleCasingManager lCasingManager = new ZombleCasingManager(mCasingTextureRegion);
		final ZombleBulletManager lBulletManager = new ZombleBulletManager();
		final ZombleMuzzleFlashManager lMuzzleFlashManager = new ZombleMuzzleFlashManager(mMuzzleFlashTextureRegion);
		final ZombleBloodManager lBloodManager = new ZombleBloodManager();
		final ZombleNumberManager lNumberManager = new ZombleNumberManager(m8BitFont);
		final ZombleOrbsManager lOrbManager = new ZombleOrbsManager(mOrbTextureRegion);
		final ZombleItemManager lItemManager = new ZombleItemManager(mGroundItemTextureRegion);
		
		// Setup global managers
		mProjectileManager = new ZombleProjectileManager(
				lCasingManager,
				lBulletManager,
				lMuzzleFlashManager);
		
		mCharacterManager = new ZombleHeroManager(
				ZombleMapManager.sharedInstance().getStartingSpawn().x, 
				ZombleMapManager.sharedInstance().getStartingSpawn().y,
				mCharacterTextureRegion,
				mCharacter2TextureRegion,
				mItemTextureRegion,
				mWaypointTextureRegion,
				mSelectorTextureRegion,
				mCharacterSelectorTextureRegion,
				lSpriteLayer,
				mProjectileManager);

		mEnemyManager = new ZombleEnemyManager(
				lOrbManager,
				mZombleTextureRegion,
				mZombleArmTextureRegion,
				mZombleDeadTextureRegion, 
				lSpriteLayer);

		mCollisionManager = new ZombleCollisionManager(
				mEnemyManager, 
				lBloodManager, 
				lBulletManager,
				mCharacterManager,
				lNumberManager,
				lItemManager);



		this.setupSpriteLayer(lSpriteLayer);
		this.setupCamera();
		
		ZombleTouchListener x = new ZombleTouchListener(mCharacterManager, mEnemyManager);
		
		this.setOnSceneTouchListener(new ZombleTouchListener(mCharacterManager, mEnemyManager));
	}
	

	private void setupCamera(){
		this.mCamera.setBounds(0, 0, ZombleMapManager.sharedInstance().getCurrentLayer().getWidth()*SCALE, ZombleMapManager.sharedInstance().getCurrentLayer().getHeight()*SCALE);
		this.mCamera.setBoundsEnabled(true);
		this.mCamera.setChaseEntity(mCharacterManager.getCameraChaseEntity());
	}

	private void setupSpriteLayer(Entity pSpriteLayer){
		pSpriteLayer.setZIndex(Layer.SPRITES.ordinal());

		for(ZombleHero character : mCharacterManager.getCharacters()){
			pSpriteLayer.attachChild(character.getEntity());
		}

		pSpriteLayer.attachChild(mCharacterManager.getCameraChaseEntity());

		pSpriteLayer.sortChildren(false);
		this.attachChild(pSpriteLayer);
		this.sortChildren();
	}

	@Override
	protected void onManagedUpdate(final float pSecondsElapsed) {
		super.onManagedUpdate(pSecondsElapsed);

		// Update enemies
		mEnemyManager.onUpdate(mCharacterManager);

		// Update characters
		mCharacterManager.onUpdate(mProjectileManager);

		// Check for collisions
		mCollisionManager.onUpdate();
	}

	@Override
	public void loadResources(){
		loadGraphics();
		loadSound();
		loadFonts();
	}

	private void loadGraphics(){
		BitmapTextureAtlasTextureRegionFactory.setAssetBasePath("gfx/");
		this.mBitmapTextureAtlas = new BuildableBitmapTextureAtlas(this.mActivity.getTextureManager(), 512, 512, TextureOptions.NEAREST);

		this.mCharacterTextureRegion = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(this.mBitmapTextureAtlas, mActivity, "main_sprite.png", 7, 8);
		this.mCharacter2TextureRegion = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(this.mBitmapTextureAtlas, mActivity, "main_sprite2.png", 7, 8);
		this.mZombleTextureRegion = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(this.mBitmapTextureAtlas, mActivity, "zomble2.png", 7, 8);
		this.mZombleDeadTextureRegion = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(this.mBitmapTextureAtlas, mActivity, "zombledie.png", 5, 8);
		this.mZombleArmTextureRegion = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(this.mBitmapTextureAtlas, mActivity, "zomblearm.png", 16, 1);
		this.mItemTextureRegion = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(this.mBitmapTextureAtlas, mActivity, "gun.png", 16, 1);
		this.mCasingTextureRegion = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(this.mBitmapTextureAtlas, mActivity, "casing.png", 2, 1);
		this.mMuzzleFlashTextureRegion = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(this.mBitmapTextureAtlas, mActivity, "muzzle_flash.png", 9, 1);
		this.mHUDTextureRegion =  BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(this.mBitmapTextureAtlas, mActivity, "healthhud.png", 1, 1);
		this.mWaypointTextureRegion =  BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(this.mBitmapTextureAtlas, mActivity, "waypoint.png", 4, 1);
		this.mSelectorTextureRegion =  BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(this.mBitmapTextureAtlas, mActivity, "selector.png", 1, 1);
		this.mCharacterSelectorTextureRegion =  BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(this.mBitmapTextureAtlas, mActivity, "feetselecter.png", 4, 1);
		this.mOrbTextureRegion =  BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(this.mBitmapTextureAtlas, mActivity, "orb.png", 6, 1);
		this.mGroundItemTextureRegion =  BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(this.mBitmapTextureAtlas, mActivity, "ground_items.png", 4, 1);
		
		BitmapTextureAtlasTextureRegionFactory.setAssetBasePath("gfx/hud/");

		this.mHUDTextureRegion =  BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(this.mBitmapTextureAtlas, mActivity, "hud.png", 2, 1);
		this.mCharacterPortraitTextureRegion =  BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(this.mBitmapTextureAtlas, mActivity, "character_portraits.png", 2, 1);

		this.mXPBarTextureRegion =  BitmapTextureAtlasTextureRegionFactory.createFromAsset(this.mBitmapTextureAtlas, mActivity, "xp_bar.png");
		this.mHungerBarTextureRegion =  BitmapTextureAtlasTextureRegionFactory.createFromAsset(this.mBitmapTextureAtlas, mActivity, "hunger_bar.png");
		this.mHealthTextureRegion =  BitmapTextureAtlasTextureRegionFactory.createFromAsset(this.mBitmapTextureAtlas, mActivity, "health_bar.png");
		this.mAbilityTextureRegion =  BitmapTextureAtlasTextureRegionFactory.createFromAsset(this.mBitmapTextureAtlas, mActivity, "ability_button.png");


		try {
			this.mBitmapTextureAtlas.build(new BlackPawnTextureAtlasBuilder<IBitmapTextureAtlasSource, BitmapTextureAtlas>(0, 0, 0));
			this.mBitmapTextureAtlas.load();
		} catch (TextureAtlasBuilderException e) {
			Debug.e(e);
		}
	}

	private void loadFonts(){
		m8BitFontTexture = new BitmapTextureAtlas(mEngine.getTextureManager(), 256, 256, TextureOptions.NEAREST);

		FontFactory.setAssetBasePath("gfx/font/");

		m8BitFont = FontFactory.createStrokeFromAsset(mEngine.getFontManager(), m8BitFontTexture, mActivity.getAssets(), "pixelation.ttf", (float)22, false,  new Color(1, 1, 1, 1).getARGBPackedInt(), 2,  new Color(0, 0, 0, 1).getARGBPackedInt());
		this.m8BitFont.load();	
	}

	private void loadSound(){
		SoundFactory.setAssetBasePath("mfx/");
		try {
			this.mGunShotSound = SoundFactory.createSoundFromAsset(this.mEngine.getSoundManager(), mActivity, "gunshot.ogg");
			this.mCasingSound = SoundFactory.createSoundFromAsset(this.mEngine.getSoundManager(), mActivity, "casing.ogg");
		} catch (final IOException e) {
			Debug.e(e);
		}

		MusicFactory.setAssetBasePath("mfx/");
		try {
			this.mMusic = MusicFactory.createMusicFromAsset(this.mEngine.getMusicManager(), mActivity, "bitbasic.mp3");
			this.mMusic.setLooping(true);
		} catch (final IOException e) {
			Debug.e(e);
		}
	}

	@Override
	public void unloadResources() {
		mBitmapTextureAtlas.unload();
	}

}