package com.maprice.zomble.scene;

import org.andengine.engine.Engine;
import org.andengine.engine.camera.BoundCamera;
import org.andengine.engine.camera.Camera;
import org.andengine.entity.scene.Scene;
import org.andengine.opengl.vbo.VertexBufferObjectManager;
import org.andengine.ui.activity.BaseGameActivity;

import android.content.Context;
import android.util.Log;

import com.maprice.zomble.util.contants.ZombleConstants;

/**
 * Singleton that manages the loading of resources and creation/switching of scenes
 * 
 * @author maprice
 */


public class ZombleSceneManager implements ZombleConstants{
	// ===========================================================
	// Constants
	// ===========================================================

	static final String LOG_TAG = PREFIX + ZombleSceneManager.class.getSimpleName();

	public enum ZombleScenes{
		SPLASH,
		MENU,
		GAME
	};

	// ===========================================================
	// Fields
	// ===========================================================

	private ZombleScenes mCurrentScene;
	private BaseGameActivity mActivity;
	private Engine mEngine;
	private BoundCamera mCamera;
	private ZombleScene mSplashScene, mMenuScene, mGameScene;

	private static final ZombleSceneManager sZombleSceneManager = new ZombleSceneManager();

	// ===========================================================
	// Constructors
	// ===========================================================

	/**
	 * Returns shared instance of the scene manager
	 * 
	 * @return
	 *            Shared scene manager
	 */
	public static ZombleSceneManager sharedInstance() {
		return sZombleSceneManager;
	}

	/**
	 * Needs to be called before using any other methods
	 * 
	 * @param pActivity
	 *            activity instance
	 * @param pEngine
	 *            engine instance
	 * @param pCamera
	 *            camera instance
	 */
	public void initSceneManager(BaseGameActivity pActivity, Engine pEngine, BoundCamera pCamera) {
		this.mActivity = pActivity;
		this.mEngine = pEngine;
		this.mCamera = pCamera;
		mGameScene = new ZombleGameScene(mActivity, mEngine, mCamera);
		mSplashScene = new ZombleSplashScene(mActivity, mEngine, mCamera);
		mMenuScene = new ZombleMenuScene(mActivity, mEngine, mCamera);
	}

	// ===========================================================
	// Methods
	// ===========================================================

	public Scene getCurrentScene(){
		switch(mCurrentScene){
		case SPLASH:
			return mSplashScene;
		case MENU:
			return mMenuScene;
		case GAME:
			return mGameScene;
		default:
			return null;
		}
	}

	public void setCurrentScene(ZombleScenes pScene){
		mCurrentScene = pScene;
		switch(pScene){
		case SPLASH:
			mEngine.setScene(mSplashScene);
			break;
		case MENU:
			mEngine.setScene(mMenuScene);
			break;
		case GAME:
			mEngine.setScene(mGameScene);
			break;
		}
	}


	/**
	 * Loads resources for specified scene
	 * 
	 * @param pScene
	 *            which scene to load resources from
	 */
	public void loadResources(ZombleScenes pScene){
		switch(pScene){
		case SPLASH:
			mSplashScene.loadResources();
			break;
		case MENU:
			mMenuScene.loadResources();
			break;

		case GAME:
			mGameScene.loadResources();
			break;
		}
	}

	/**
	 * Loads resources for specified scene
	 * 
	 * @param pScene
	 *            which scene to load resources from
	 */
	public void unloadResources(ZombleScenes pScene){
		switch(pScene){
		case SPLASH:
			mSplashScene.unloadResources();
			break;
		case MENU:
			mMenuScene.unloadResources();
			break;
		case GAME:
			mGameScene.unloadResources();
			break;
		}
	}

	/**
	 * Initializes and returns specified scene
	 * 
	 * @param pScene
	 *            which scene to return
	 */
	public Scene createScene(ZombleScenes pScene){

		mCurrentScene = pScene;

		switch(pScene){
		case SPLASH:
			mSplashScene.initScene();
			return mSplashScene;
		case MENU:
			mMenuScene.initScene();
			return mMenuScene;
		case GAME:
			mGameScene.initScene();
			return mGameScene;
		default:
			Log.e(LOG_TAG, "Tried to create invalid scene");
			return null;
		}
	}

	public Engine getEngine(){
		return mEngine;
	}

	public Camera getCamera(){
		return mCamera;
	}

	public VertexBufferObjectManager getVBOmanager(){
		return mEngine.getVertexBufferObjectManager();
	}

	public Context getContext(){
		return mActivity;
	}
}
