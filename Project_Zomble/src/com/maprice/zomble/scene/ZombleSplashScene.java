package com.maprice.zomble.scene;

import org.andengine.engine.Engine;
import org.andengine.engine.camera.BoundCamera;
import org.andengine.entity.sprite.Sprite;
import org.andengine.opengl.texture.TextureOptions;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlas;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlasTextureRegionFactory;
import org.andengine.opengl.texture.atlas.bitmap.BuildableBitmapTextureAtlas;
import org.andengine.opengl.texture.atlas.bitmap.source.IBitmapTextureAtlasSource;
import org.andengine.opengl.texture.atlas.buildable.builder.BlackPawnTextureAtlasBuilder;
import org.andengine.opengl.texture.atlas.buildable.builder.ITextureAtlasBuilder.TextureAtlasBuilderException;
import org.andengine.opengl.texture.region.TiledTextureRegion;
import org.andengine.ui.activity.BaseGameActivity;
import org.andengine.util.debug.Debug;

import com.maprice.zomble.util.contants.ZombleConstants;

/**
 * Splash screen scene
 * 
 * @author maprice
 */

public class ZombleSplashScene extends ZombleScene implements ZombleConstants{
	// ===========================================================
	// Constants
	// ===========================================================

	static final String LOG_TAG = PREFIX + ZombleSplashScene.class.getSimpleName();

	// ===========================================================
	// Fields
	// ===========================================================
	
	private BuildableBitmapTextureAtlas mBitmapTextureAtlas;
	private TiledTextureRegion mBackgroundTextureRegion; 
	private Sprite mSplashBackground;

	// ===========================================================
	// Constructors
	// ===========================================================

	

	public ZombleSplashScene(BaseGameActivity pActivity, Engine pEngine, BoundCamera pCamera) {
		super(pActivity, pEngine, pCamera); 
	}

	// ===========================================================
	// Methods
	// ===========================================================

	@Override
	public void initScene(){
		mSplashBackground = new Sprite(0f, 0f, mBackgroundTextureRegion, this.mEngine.getVertexBufferObjectManager());
		mSplashBackground.setPosition(mCamera.getWidth()/2 - mSplashBackground.getWidth()/2, mCamera.getHeight()/2 - mSplashBackground.getHeight()/2);
		this.attachChild(mSplashBackground);
	}
	
	@Override
	public void loadResources(){
		BitmapTextureAtlasTextureRegionFactory.setAssetBasePath("gfx/");

		this.mBitmapTextureAtlas = new BuildableBitmapTextureAtlas(this.mActivity.getTextureManager(), 512, 512, TextureOptions.NEAREST);
		this.mBackgroundTextureRegion = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(this.mBitmapTextureAtlas, mActivity, "andengine_logo.png", 1, 1);

		try {
			this.mBitmapTextureAtlas.build(new BlackPawnTextureAtlasBuilder<IBitmapTextureAtlasSource, BitmapTextureAtlas>(0, 0, 1));
			this.mBitmapTextureAtlas.load();
		} catch (TextureAtlasBuilderException e) {
			Debug.e(e);
		}
	}

	@Override
	public void unloadResources() {
		mBitmapTextureAtlas.unload();
	}
}
