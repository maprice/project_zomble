package com.maprice.zomble.scene;

import org.andengine.engine.Engine;
import org.andengine.engine.camera.BoundCamera;
import org.andengine.entity.scene.background.AutoParallaxBackground;
import org.andengine.entity.scene.background.ParallaxBackground.ParallaxEntity;
import org.andengine.entity.scene.menu.MenuScene;
import org.andengine.entity.scene.menu.MenuScene.IOnMenuItemClickListener;
import org.andengine.entity.scene.menu.item.IMenuItem;
import org.andengine.entity.scene.menu.item.SpriteMenuItem;
import org.andengine.entity.scene.menu.item.decorator.ScaleMenuItemDecorator;
import org.andengine.entity.sprite.Sprite;
import org.andengine.opengl.texture.TextureOptions;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlas;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlasTextureRegionFactory;
import org.andengine.opengl.texture.atlas.bitmap.BuildableBitmapTextureAtlas;
import org.andengine.opengl.texture.atlas.bitmap.source.IBitmapTextureAtlasSource;
import org.andengine.opengl.texture.atlas.buildable.builder.BlackPawnTextureAtlasBuilder;
import org.andengine.opengl.texture.atlas.buildable.builder.ITextureAtlasBuilder.TextureAtlasBuilderException;
import org.andengine.opengl.texture.region.TextureRegion;
import org.andengine.ui.activity.BaseGameActivity;
import org.andengine.util.debug.Debug;

import android.util.Log;
import android.widget.Toast;

import com.maprice.zomble.scene.ZombleSceneManager.ZombleScenes;
import com.maprice.zomble.util.contants.ZombleConstants;



/**
 * Splash screen scene
 * 
 * @author maprice
 */

public class ZombleMenuScene extends ZombleScene implements ZombleConstants, IOnMenuItemClickListener{
	// ===========================================================
	// Constants
	// ===========================================================

	static final String LOG_TAG = PREFIX + ZombleMenuScene.class.getSimpleName();

	public enum MenuButton{
		PLAY,
		STATS,
		SHOP
	}
	
	// ===========================================================
	// Fields
	// ===========================================================


	private BuildableBitmapTextureAtlas mMenuTextureAtlas;

	private TextureRegion mParallaxLayer5;
	private TextureRegion mParallaxLayer4;
	private TextureRegion mParallaxLayer3;
	private TextureRegion mParallaxLayer2;
	private TextureRegion mParallaxLayer1;

	private TextureRegion mPlayTextureRegion;
	private TextureRegion mShopTextureRegion;
	private TextureRegion mStatsTextureRegion;
	private TextureRegion mLogoTextureRegion;
	
	private MenuScene mMenuChildScene;

	// ===========================================================
	// Constructors
	// ===========================================================

	public ZombleMenuScene(BaseGameActivity pActivity, Engine pEngine, BoundCamera pCamera) {
		super(pActivity, pEngine, pCamera); 
	}

	// ===========================================================
	// Methods
	// ===========================================================

	public void initScene(){

		mMenuChildScene = new MenuScene(mCamera);
		mMenuChildScene.setPosition(0, 0);
		mMenuChildScene.setScale(SCALE);


		final Sprite lParallaxSprite5 = new Sprite(0, 0, mCamera.getWidth(), mCamera.getHeight(), this.mParallaxLayer5, this.mEngine.getVertexBufferObjectManager());		
		final Sprite lParallaxSprite4 = new Sprite(0, mCamera.getHeight()*0.1f, this.mParallaxLayer4, this.mEngine.getVertexBufferObjectManager());		
		final Sprite lParallaxSprite3 = new Sprite(0, mCamera.getHeight()*0.1f, this.mParallaxLayer3, this.mEngine.getVertexBufferObjectManager());		
		final Sprite lParallaxSprite2 = new Sprite(0, mCamera.getHeight()*0.5f, mCamera.getWidth(), mCamera.getHeight()*0.5f, this.mParallaxLayer2, this.mEngine.getVertexBufferObjectManager());		
		final Sprite lParallaxSprite1 = new Sprite(0, mCamera.getHeight()*0.1f, this.mParallaxLayer1, this.mEngine.getVertexBufferObjectManager());		

		final Sprite lLogoSprite = new Sprite(mCamera.getHeight()*0.05f, (mCamera.getHeight()-mLogoTextureRegion.getHeight()*SCALE) - mCamera.getHeight()*0.05f, this.mLogoTextureRegion, this.mEngine.getVertexBufferObjectManager());		

		lLogoSprite.setScaleCenter(0, 0);
		lLogoSprite.setScale(SCALE); 
		this.attachChild(lLogoSprite);


		lParallaxSprite4.setScaleCenter(0, 0);
		lParallaxSprite3.setScaleCenter(0, 0);
		lParallaxSprite1.setScaleCenter(0, 0);

		lParallaxSprite4.setScale(SCALE);
		lParallaxSprite3.setScale(SCALE);
		lParallaxSprite1.setScale(SCALE);

		lParallaxSprite4.setX(0);
		lParallaxSprite3.setX(0);
		lParallaxSprite1.setX(0);

		final AutoParallaxBackground lAutoParallaxBackground = new AutoParallaxBackground(0, 0, 0, 5);
		lAutoParallaxBackground.attachParallaxEntity(new ParallaxEntity(0.0f, lParallaxSprite5));
		lAutoParallaxBackground.attachParallaxEntity(new ParallaxEntity(-3.0f, lParallaxSprite4));
		lAutoParallaxBackground.attachParallaxEntity(new ParallaxEntity(-5.0f, lParallaxSprite3));
		lAutoParallaxBackground.attachParallaxEntity(new ParallaxEntity(0.0f, lParallaxSprite2));
		lAutoParallaxBackground.attachParallaxEntity(new ParallaxEntity(-6.0f, lParallaxSprite1));

		this.setBackground(lAutoParallaxBackground);

		final IMenuItem playMenuItem = new ScaleMenuItemDecorator(new SpriteMenuItem(MenuButton.PLAY.ordinal(), mPlayTextureRegion, mEngine.getVertexBufferObjectManager()), 1.05f, 1.0f);
		final IMenuItem statsMenuItem = new ScaleMenuItemDecorator(new SpriteMenuItem(MenuButton.STATS.ordinal(), mStatsTextureRegion, mEngine.getVertexBufferObjectManager()), 1.05f, 1.0f);
		final IMenuItem shopMenuItem = new ScaleMenuItemDecorator(new SpriteMenuItem(MenuButton.SHOP.ordinal(), mShopTextureRegion, mEngine.getVertexBufferObjectManager()), 1.05f, 1.0f);

		mMenuChildScene.addMenuItem(playMenuItem);
		mMenuChildScene.addMenuItem(statsMenuItem);
		mMenuChildScene.addMenuItem(shopMenuItem);

		mMenuChildScene.buildAnimations();
		mMenuChildScene.setBackgroundEnabled(false);

		playMenuItem.setPosition(mCamera.getWidth()/SCALE - playMenuItem.getWidth() - MENU_BUTTON_BUFFER, MENU_BUTTON_BUFFER);
		statsMenuItem.setPosition(playMenuItem.getX(), playMenuItem.getY() + playMenuItem.getHeight()+MENU_BUTTON_BUFFER);
		shopMenuItem.setPosition(statsMenuItem.getX(), statsMenuItem.getY() + statsMenuItem.getHeight()+MENU_BUTTON_BUFFER);

		mMenuChildScene.setOnMenuItemClickListener(this);
		setChildScene(mMenuChildScene);
	}
	
	@Override
	public void loadResources(){
		
		BitmapTextureAtlasTextureRegionFactory.setAssetBasePath("gfx/menu/");
		mMenuTextureAtlas = new BuildableBitmapTextureAtlas(mActivity.getTextureManager(), 512, 512, TextureOptions.NEAREST);
		mPlayTextureRegion = BitmapTextureAtlasTextureRegionFactory.createFromAsset(mMenuTextureAtlas, mActivity, "playbutton.png");
		mShopTextureRegion = BitmapTextureAtlasTextureRegionFactory.createFromAsset(mMenuTextureAtlas, mActivity, "shopbutton.png");
		mStatsTextureRegion = BitmapTextureAtlasTextureRegionFactory.createFromAsset(mMenuTextureAtlas, mActivity, "statsbutton.png");

		BitmapTextureAtlasTextureRegionFactory.setAssetBasePath("gfx/parallax/");
		mParallaxLayer5 = BitmapTextureAtlasTextureRegionFactory.createFromAsset(mMenuTextureAtlas, mActivity, "parallax5.png");
		mParallaxLayer4 = BitmapTextureAtlasTextureRegionFactory.createFromAsset(mMenuTextureAtlas, mActivity, "parallax4.png");
		mParallaxLayer3 = BitmapTextureAtlasTextureRegionFactory.createFromAsset(mMenuTextureAtlas, mActivity, "parallax3.png");
		mParallaxLayer2 = BitmapTextureAtlasTextureRegionFactory.createFromAsset(mMenuTextureAtlas, mActivity, "parallax2.png");
		mParallaxLayer1 = BitmapTextureAtlasTextureRegionFactory.createFromAsset(mMenuTextureAtlas, mActivity, "parallax1.png");
		mLogoTextureRegion = BitmapTextureAtlasTextureRegionFactory.createFromAsset(mMenuTextureAtlas, mActivity, "zomble_logo.png");

		try 
		{
			this.mMenuTextureAtlas.build(new BlackPawnTextureAtlasBuilder<IBitmapTextureAtlasSource, BitmapTextureAtlas>(0, 1, 0));
			this.mMenuTextureAtlas.load();
		} 
		catch (final TextureAtlasBuilderException e)
		{
			Debug.e(e);
		}


	}
	
	@Override
	public void unloadResources() {
		mMenuTextureAtlas.unload();
	}


	@Override
	public boolean onMenuItemClicked(MenuScene pMenuScene, IMenuItem pMenuItem,
			float pMenuItemLocalX, float pMenuItemLocalY) {

		Log.i(LOG_TAG, "Menu Item Pressed: " + pMenuItem.getID());

		if(pMenuItem.getID() == MenuButton.PLAY.ordinal()){
			ZombleSceneManager.sharedInstance().createScene(ZombleScenes.GAME);
			ZombleSceneManager.sharedInstance().setCurrentScene(ZombleScenes.GAME);
			ZombleSceneManager.sharedInstance().unloadResources(ZombleScenes.MENU);
		}
		else if(pMenuItem.getID() == MenuButton.STATS.ordinal()){
			mActivity.runOnUiThread(new Runnable(){

				@Override
				public void run() {
					Toast.makeText(mActivity, "Stats not implemented", Toast.LENGTH_SHORT).show();	
				}


			});

		}
		else if(pMenuItem.getID() == MenuButton.SHOP.ordinal()){
			mActivity.runOnUiThread(new Runnable(){

				@Override
				public void run() {
					Toast.makeText(mActivity, "Shop not implemented", Toast.LENGTH_SHORT).show();	
				}


			});
		}

		return false;
	}
}