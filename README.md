# Zomble

## New features
* Map layers
* Radial lighting layer
* Items
* Smarter zomble logic (aggro range)
* More touch controls
**Swipe up -> select all
**Swipe right/left cycle right/left
**Double tap -> use ability

## Improvements
* Replace muzzle flash image with a smaller version
* Improve character animations
* Need smarter algorithm for multiple character movement positions
* Map loading animation (zelda 1 style?)


## Known bugs
* When engine is under stress, it can miss a physics update, this causes the gun to miss its "Recoil limit" and continue spinning
* Shooting animation slightly wonky (and off center)
* Orbiting Orbs
* When moving 2 characters at once i simply use a linear shift so sometimes when moving 2 character only 1 will move because the other is on a wall tile
* Using actual starting position instead of calculated tile can cause heros to move to invalid tiles, then on next press the pathing algorithm will return null
* Dancing zombles (miss update tile and do a little jitter)

## Future Tasks
* Random map generation
* Leaderboard implementation
* Character 'chat'
* Data saving